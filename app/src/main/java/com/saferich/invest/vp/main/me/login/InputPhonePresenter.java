package com.saferich.invest.vp.main.me.login;

import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class InputPhonePresenter extends BasePresenter<IInputPhoneView> {

    @Inject
    public InputPhonePresenter() {
    }

    public void next(EditText editText) {
        params.put("p_phone", editText.getText().toString().trim());
        apiWrapper.existPhone(context,params)
                .doOnSubscribe(() -> showDialogView(R.string.authing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Boolean isExist)->{

                    view.moveActivity(isExist);
                }));
    }
}
