package com.saferich.invest.vp.main.me.phone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.GetCash;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.main.me.result.BalanceResultActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class InputVerCodePresenter extends BasePresenter<IInputVerCodeView> {

    @Inject
    Activity activity;

    @Inject
    public InputVerCodePresenter() {
    }


    public void getRechare(EditText editText, Bundle extras) {
        params.clear();
        params.put("p_rechargeId", extras.getString("id"));
        params.put("p_VerificationCode", editText.getText().toString().trim());


        apiWrapper.confirmPay(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((String object) -> {


                    if (rxBus.hasObservers()) {
                        rxBus.send(new InvestSuccessEvent(userinfo.accessToken));
                    }

                    Intent intent = new Intent(activity, BalanceResultActivity.class);
                    intent.putExtra("blance", StringUtil.repaceE(extras.getDouble("blance")));
                    if (extras.getBoolean("isGetCash")) {
                        intent.putExtra("isGetCash", true);
                    }
                    intent.putExtra("rechargeMoney", StringUtil.repaceE(extras.getDouble("rechargeMoney")));
                    activity.startActivity(intent);

                    activity.finish();
                }));


    }


    public void cashcode() {
        apiWrapper.addExtractCashphoneVCode(context)
                .subscribe(newSubscriber((String data) -> {
                    ShowToast.Short(context, "验证码发送成功");
                }));
    }

    public void getCash(EditText editText, Bundle extras) {
        params.clear();
        params.put("p_money", extras.getString("count"));
        params.put("p_channel", "0");
        params.put("p_vCode", editText.getText().toString().trim());


        apiWrapper.addExtractCash(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((GetCash object) -> {

                    if (rxBus.hasObservers()) {
                        rxBus.send(new InvestSuccessEvent(userinfo.accessToken));
                    }

                    Intent intent = new Intent(activity, BalanceResultActivity.class);
                    intent.putExtra("blance", StringUtil.repaceE(object.getBalance()));
                    intent.putExtra("rechargeMoney", StringUtil.repaceE(object.getExtractMoney()));
                    intent.putExtra("isGetCash", true);
                    activity.startActivity(intent);
                    activity.finish();
                }));

    }

    public void rechareCode(Bundle extras) {
        params.clear();
        params.put("p_rechargeId", extras.getString("id"));
        apiWrapper.rechargesendVerificationCode(context, params)
                .subscribe(newSubscriber((String data) -> {
                    ShowToast.Short(context, "验证码发送成功");
                }));
    }
}
