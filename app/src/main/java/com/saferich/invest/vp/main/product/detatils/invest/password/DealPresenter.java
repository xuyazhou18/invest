package com.saferich.invest.vp.main.product.detatils.invest.password;

import android.app.Activity;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.InvestBody;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class DealPresenter extends BasePresenter<IdealView> {

    @Inject
    Activity activity;

    @Inject
    public DealPresenter() {
        super();
    }

    public void setPayPassWord(String password) {
        userinfo.payPassword = password;
        userinfo.isSetPayWord = true;
        userinfo.update();

    }


    public void addorder(String password) {


        InvestBody body = new InvestBody();
        body.setChannel("0");
        body.setPayWay("余额");
        body.setPayMethod("余额");
        body.setPayPassword(password);
        body.setProductId(activity.getIntent().getStringExtra("id"));
        //   body.setProductId("QDFina000002");
        body.setInvestMoney(activity.getIntent().getStringExtra("count"));
        body.setCert_type("01");
        body.setCurrency("156");
        body.setTerminal_info("2222222");
        body.setTerminal_type("mobile");
        body.setUser_ip("192.168.0.122");


        apiWrapper.addorder(context, body)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((OrderInfo data) -> {
                    sendSub();
                    view.authFinish(data);

                }));


    }


    public void sendSub() {
        if (rxBus.hasObservers()) {
            rxBus.send(new InvestSuccessEvent(userinfo.accessToken));
        }
    }
}
