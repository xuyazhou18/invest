package com.saferich.invest.vp.main.product.Progressive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.common.WebDeatilsActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public class ProgressivePresenter extends BasePresenter<IProgressiveView> {

    @Inject
    public ProgressivePresenter() {
    }

    public void setWebView(WebView webView, String url, MultiStateView multiStateView) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(context,
                        WebDeatilsActivity.class);
                intent.putExtra("url", url);
                // intent.putExtra("title", deatails.getTitle());

                context.startActivity(intent);
                return true;
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
                                                              String url) {
                WebResourceResponse response = null;
                response = super.shouldInterceptRequest(view, url);
                return response;
            }
        });
    }


}
