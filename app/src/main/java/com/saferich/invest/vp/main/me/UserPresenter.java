package com.saferich.invest.vp.main.me;

import android.support.v4.app.Fragment;

import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.event.BankSuccessEvent;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.model.event.loginEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class UserPresenter extends BasePresenter<IUserView> implements IBaseView {

    @Inject
    Fragment fragment;

    @Inject
    public UserPresenter() {
        super();
    }

    public void getdata() {


        if (config.isLogin) {
            apiWrapper.getAccountMessage(context).subscribe(newSubscriber((Account data) -> {
                view.showData(data);
                userinfo.isBindCard = data.isIfBindCard();
                userinfo.balance = data.getBalance();
                userinfo.totalMoney = data.getTotalMoney();
                userinfo.isSetPayWord = data.isIfSetPayPassword();
                userinfo.ifCardUsed = data.isIfCardUsed();
                userinfo.ifNewInvest = data.isIfNewInvest();

                userinfo.mobliePhone = data.getBindMobilePhone();
                userinfo.userLoginPhoneNumber = data.getMobile();
                if (data.isIfBindCard()) {
                    userinfo.bankCard = data.getBankCard();
                    userinfo.bankCode = data.getBankCode();
                }

                userinfo.update();

            }));
        }
    }


    public void refreshData() {
        if (config.isLogin) {
            Account account = new Account();
            account.setIfBindCard(userinfo.isBindCard);
            account.setBalance(userinfo.balance);
            account.setTotalMoney(userinfo.totalMoney);
            if (userinfo.isBindCard) {
                account.setBankCard(userinfo.bankCard);
                account.setBankCode(userinfo.bankCode);
            }
            view.showData(account);
        }

    }

    public void addSub() {
        subscription.add(rxBus.subscribe(event -> {
                    getDataByToken(event.getToken());
                },
                InvestSuccessEvent.class));

        subscription.add(rxBus.subscribe(event -> {
                    getDataByToken(event.getToken());
                },
                BankSuccessEvent.class));

        subscription.add(rxBus.subscribe(event -> {
                    getDataByToken(event.getToken());
                },
                loginEvent.class));
    }

    private void getDataByToken(String token) {
        apiWrapper.getAccountMessage(fragment, token).subscribe(newSubscriber((Account data) -> {
            view.showData(data);
            userinfo.isBindCard = data.isIfBindCard();
            userinfo.balance = data.getBalance();
            userinfo.totalMoney = data.getTotalMoney();
            userinfo.isSetPayWord = data.isIfSetPayPassword();
            userinfo.ifCardUsed = data.isIfCardUsed();
            userinfo.ifNewInvest = data.isIfNewInvest();

            userinfo.mobliePhone = data.getBindMobilePhone();
            userinfo.userLoginPhoneNumber = data.getMobile();
            if (data.isIfBindCard()) {
                userinfo.bankCard = data.getBankCard();
                userinfo.bankCode = data.getBankCode();
            }
            userinfo.update();
        }));
    }

    @Override
    protected void internetError() {
        super.internetError();
        view.error();
    }

    public void Unsubscribe() {
        subscription.unsubscribe();
    }

    public void fresh() {
        getDataByToken(userinfo.accessToken);
    }
}
