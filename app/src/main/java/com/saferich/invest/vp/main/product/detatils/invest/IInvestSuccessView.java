package com.saferich.invest.vp.main.product.detatils.invest;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public interface IInvestSuccessView extends IBaseView {
    void ShowData(ArrayList<AdCount> list);
}
