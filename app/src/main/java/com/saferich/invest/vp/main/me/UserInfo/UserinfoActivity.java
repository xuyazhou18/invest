package com.saferich.invest.vp.main.me.userinfo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.UserMessage;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.bank.BindBankActivity;
import com.saferich.invest.vp.main.me.bank.WatchBankActivity;
import com.saferich.invest.vp.main.me.gesture.GestureActivity;
import com.saferich.invest.vp.main.me.password.ChangePasswordActivity;
import com.saferich.invest.vp.main.me.password.ChangePayPasswordActivity;
import com.saferich.invest.vp.main.me.password.SetPayPassWordActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的用户信息界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public class UserinfoActivity extends BaseActivity<UserinfoPresenter> implements IUserInfoView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.number)
    TextView number;
    @BindView(R.id.bank_layout)
    RelativeLayout bankLayout;
    @BindView(R.id.login_password)
    RelativeLayout loginPassword;
    @BindView(R.id.pay_password)
    RelativeLayout payPassword;
    @BindView(R.id.check_gesture)
    CheckBox checkGesture;
    @BindView(R.id.gesture_layout)
    RelativeLayout gestureLayout;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.bindText)
    TextView bindText;
    @BindView(R.id.paypassword)
    TextView paypassword;
    @BindView(R.id.gesture_text)
    TextView gestureText;
    private boolean isBindCard;
    private boolean isSetPayWord;
    private boolean isPay;
    private boolean isSetGesture;

    @Inject
    UserConfig config;
    @Inject
    Userinfo userinfo;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.anctivity_userinfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("个人信息");
        logoutClick();
        presenter.addSub(bindText, paypassword);

        if (config.isGesture) {
            checkGesture.setChecked(true);
            gestureLayout.setVisibility(View.VISIBLE);

        } else {
            gestureLayout.setVisibility(View.GONE);
            checkGesture.setChecked(false);
        }


        checkGesture.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ShowToast.Short(this, "启用手势");
                presenter.changeGesture(true);
                gestureLayout.setVisibility(View.VISIBLE);
            } else {
                ShowToast.Short(this, "关闭手势");
                presenter.changeGesture(false);
                gestureLayout.setVisibility(View.GONE);
            }
            setGestureText();

        });
    }

    private void setGestureText() {
        if (userinfo.gesturePassword != null) {
            isSetGesture = true;
            gestureText.setText("修改");
        } else {
            gestureText.setText("设置");
        }
    }

    private void logoutClick() {
        RxView.clicks(btnLogout)
                .throttleFirst(500, TimeUnit.MILLISECONDS)

                .subscribe(aVoid ->
                        presenter.logout());
    }

    @OnClick(R.id.bank_layout)
    public void bankLayoutClick() {
        if (isBindCard) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("ispay", isPay);
            ActivityUtil.moveToActivity(this, WatchBankActivity.class, bundle);
        } else {
            ActivityUtil.moveToActivity(this, BindBankActivity.class);
        }

    }

    @OnClick(R.id.login_password)
    public void loginPasswordClick() {
        ActivityUtil.moveToActivity(this, ChangePasswordActivity.class);
    }

    @OnClick(R.id.pay_password)
    public void paypasswordClick() {
        if (isSetPayWord) {
            ActivityUtil.moveToActivity(this, ChangePayPasswordActivity.class);
            this.finish();
        } else {
            ActivityUtil.moveToActivity(this, SetPayPassWordActivity.class);
            this.finish();
        }

    }

    @OnClick(R.id.gesture_layout)
    public void gestureClick() {
        Bundle bundle = new Bundle();
        if (isSetGesture) {
            bundle.putBoolean("isChange", true);
        }

        ActivityUtil.moveToActivity(this, GestureActivity.class, bundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGestureText();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.Unsubscribe();
    }

    @Override
    public void showData(UserMessage message) {
        number.setText(message.getM_phone());
        if (message.isIfBindBankCard()) {

            if (message.isIfCardUsed()) {
                bindText.setText("查看");
                isPay = true;
            } else {
                bindText.setText("修改");
                isPay = false;
            }

            isBindCard = true;
        } else {
            isBindCard = false;
            bindText.setText("绑定");
        }
        if (message.isIfSetPayPassword()) {
            isSetPayWord = true;
            paypassword.setText("修改");
        } else {
            isSetPayWord = false;
            paypassword.setText("设置");
        }


    }
}
