package com.saferich.invest.vp.main.home;

import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public interface IInvestVistorView extends IBaseView {
    void showData(RecommondItem product);

    void showAdData(String[] data, String[] link);
}
