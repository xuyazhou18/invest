package com.saferich.invest.vp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.Utils.SystemUtil;
import com.saferich.invest.common.config.Constant;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.common.WebDeatilsActivity;
import com.saferich.invest.vp.main.home.list.InvestListActivity;
import com.saferich.invest.vp.main.home.myList.MyInvestListActivity;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 登录后首页列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class InvestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<RecommondItem> recomondList;
    private ArrayList<AdCount> AdCountList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOT = 2;
    private ProfitBean profitBean;

    @Inject
    public InvestAdapter() {
        recomondList = new ArrayList<>();
        AdCountList = new ArrayList<>();
    }


    public void SetListData(ArrayList<RecommondItem> productList) {

        this.recomondList = productList;
        if (recomondList != null) {
            notifyDataSetChanged();
        }

    }

    public void setHeaderData(ProfitBean ProfitBean) {
        this.profitBean = ProfitBean;
        notifyItemChanged(0);
    }

    public void setFootData(ArrayList<AdCount> AdCountList) {
        this.AdCountList = AdCountList;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recommend, parent, false);

            return new ItemViewHolder(view);
        }

        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_invest, parent, false);
            return new HeaderViewHolder(view);
        }
        if (viewType == TYPE_FOOT) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.foot_invest, parent, false);
            return new FootViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            if (recomondList.size() == 0) {
                return;
            } else if (recomondList.size() == 1) {
                itemViewHolder.bindTo(recomondList.get(0));
            } else {
                itemViewHolder.bindTo(recomondList.get(position - 1));
            }


        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.setIsRecyclable(true);
            headerViewHolder.bindTo(profitBean);

        } else if (holder instanceof FootViewHolder) {
            FootViewHolder footViewHolder = (FootViewHolder) holder;
            if (recomondList.size() == 0) {
                footViewHolder.bindTo(AdCountList.get(position - 1), AdCountList);
            } else if (recomondList.size() == 1) {
                footViewHolder.bindTo(AdCountList.get(position - 2), AdCountList);
            } else {
                footViewHolder.bindTo(AdCountList.get(position - 3), AdCountList);
            }


        }

    }

    @Override
    public int getItemCount() {

        return recomondList.size() + AdCountList.size() + 1;

    }

    @Override
    public int getItemViewType(int position) {


        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == 1) {
            if (recomondList.size() == 1 || recomondList.size() == 2) {
                return TYPE_ITEM;
            } else {
                return TYPE_FOOT;
            }
        } else if (position == 2) {
            if ( recomondList.size() == 2) {
                return TYPE_ITEM;
            } else {
                return TYPE_FOOT;
            }

        } else {
            return TYPE_FOOT;
        }

    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_icon)
        ImageView productIcon;
        @BindView(R.id.yield_rate)
        TextView yieldRate;
        @BindView(R.id.yield_rate_number)
        TextView yieldRateNumber;
        @BindView(R.id.prsent)
        TextView prsent;
        @BindView(R.id.add_icon)
        TextView addIcon;
        @BindView(R.id.floatText)
        TextView floatText;
        @BindView(R.id.rete_layout)
        LinearLayout reteLayout;
        @BindView(R.id.product_tips)
        TextView productTips;
        @BindView(R.id.product_time)
        TextView productTime;
        @BindView(R.id.item_layout)
        LinearLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(RecommondItem recommondItem) {

            if (recommondItem.getProductProfitTypeCode().equals("FixedIncome")) {
                yieldRateNumber.setText(StringUtil.repaceE(recommondItem.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                floatText.setVisibility(View.GONE);

            }

            if (recommondItem.getProductProfitTypeCode().equals("FloatIncome")) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                prsent.setVisibility(View.GONE);
                floatText.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                yieldRateNumber.setVisibility(View.GONE);
            }
            if (recommondItem.getProductProfitTypeCode().equals("Fixed&FloatIncome")) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                yieldRateNumber.setText(StringUtil.repaceE(recommondItem.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.VISIBLE);
                floatText.setVisibility(View.VISIBLE);
            }


            if (recommondItem.getBuyingGroupsCode().equals("BuyingGroupsCode_NEW")) {
                productIcon.setVisibility(View.VISIBLE);
            } else {
                productIcon.setVisibility(View.GONE);
            }

            productName.setText(recommondItem.getProductName());
            productTips.setText(recommondItem.getMinInvestMoney() + "元起投");
            productTime.setText(recommondItem.getInvestTerm() + "天投资期限");

            RxView.clicks(itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(productName.getContext(),
                                ProductDeatilsActivity.class);
                        intent.putExtra("id", recommondItem.getId());

                        productName.getContext().startActivity(intent);
                    });
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.current_money)
        TextView currentMoney;
        @BindView(R.id.invest_list)
        LinearLayout investList;
        @BindView(R.id.get_count)
        TextView getCount;
        @BindView(R.id.empty_layout)
        TextView emptyLayout;
        @BindView(R.id.my_invest_layout)
        RelativeLayout myInvestLayout;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(ProfitBean profitBean) {
            if (profitBean == null) {
                return;
            }
            currentMoney.setText(profitBean.getCurrentInvestMoney());

            getCount.setText(profitBean.getProfit());

            RxView.clicks(investList).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(investList.getContext(),
                                InvestListActivity.class);
                        intent.putExtra("profit", profitBean.getProfit() + "");
                        intent.putExtra("currentMoney", profitBean.getCurrentInvestMoney());
                        intent.putExtra("totalMoney", profitBean.getTotalInvestMoney());
                        investList.getContext().startActivity(intent);
                    });
            RxView.clicks(myInvestLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(investList.getContext(),
                                MyInvestListActivity.class);

                        investList.getContext().startActivity(intent);
                    });
        }
    }

    static class FootViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.hot_layout)
        RelativeLayout hotLayout;
        @BindView(R.id.image)
        ImageView image;

        public FootViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(AdCount adCount, ArrayList<AdCount> list) {
            if (list.indexOf(adCount) == 0) {
                hotLayout.setVisibility(View.VISIBLE);
            } else {
                hotLayout.setVisibility(View.GONE);
            }

            int realWith = SystemUtil.getScreenWidth(image.getContext());

            if (adCount.getPic() == null) {
                return;
            }

            Glide.with(image.getContext()).load(Constant.imagUrl + adCount.getPic())
                    // .placeholder(R.mipmap.default_read_item)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(realWith, realWith * 200 / 750)
                    .into(image);

            RxView.clicks(image).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(image.getContext(),
                                WebDeatilsActivity.class);
                        intent.putExtra("url", adCount.getForwardUrl());
                        image.getContext().startActivity(intent);
                    });
        }
    }
}
