package com.saferich.invest.vp.main.home.deatils;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.saferich.invest.R;
import com.saferich.invest.vp.adapter.FlowAdapter;
import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.vp.base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * 投资追踪界面
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestTraceFragment extends BaseFragment<InvestTracePresenter> implements ITraceView {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.flow_layout)
    RelativeLayout flowLayout;

    @Inject
    FlowAdapter adapter;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_invest_msg, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //   recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(adapter);
    }

    public void setdata(InvestInfo info) {
        if (info.getInvestTrackBeanList() != null && info.getInvestTrackBeanList().size() > 0) {
            adapter.setDataList((ArrayList<InvestInfo.InvestTrackBeanList>) info.getInvestTrackBeanList());
        }

    }


}
