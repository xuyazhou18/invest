package com.saferich.invest.vp.main.home;

import android.support.v4.app.Fragment;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.model.event.loginEvent;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestPresenter extends BasePresenter<IInvestView> {


    @Inject
    Fragment fragment;


    @Inject
    public InvestPresenter() {
        super();
    }

    public void refresh() {
        if (config.isLogin) {
            Userinfo userinfo = SQLite.select().from(Userinfo.class).querySingle();
            getHeaderdata(userinfo.getAccessToken());


        }
        getdata();
    }

    private void getdata() {




        params.put("p_PageIndex", "1");
        params.put("p_MaxRows", 2);



        apiWrapper.getRecommondList(fragment, params)
                .subscribe(newSubscriber((List<RecommondItem> list) -> {


                   view.showData((ArrayList<RecommondItem>) list);
                }));



    }



    public void addSub() {

        subscription.add(rxBus.subscribe(event -> {
                    apiWrapper.getOrderInfo2(fragment, event.getToken()).subscribe(newSubscriber((ProfitBean data) -> {
                        view.showHeaderData(data);
                    }));
                },
                loginEvent.class));

        subscription.add(rxBus.subscribe(event -> {
                    apiWrapper.getOrderInfo2(fragment, event.getToken()).subscribe(newSubscriber((ProfitBean data) -> {
                        view.showHeaderData(data);
                    }));
                },
                InvestSuccessEvent.class));

    }

    @Override
    protected void internetError() {
        super.internetError();
        view.error();
    }

    public void getHeaderdata() {
        apiWrapper.getOrderInfo(fragment).subscribe(newSubscriber((ProfitBean data) -> {
            view.showHeaderData(data);
        }));
    }

    public void getHeaderdata(String accessToken) {
        apiWrapper.getOrderInfo2(fragment, accessToken).subscribe(newSubscriber((ProfitBean data) -> {
            view.showHeaderData(data);
        }));
    }

    public void Unsubscribe() {
        subscription.unsubscribe();
    }

    public void getAdContent() {
        apiWrapper.getAdContents(context, "2").subscribe(newSubscriber((List<AdCount> list) -> {

            view.showFootData((ArrayList<AdCount>) list);
        }));
    }
}
