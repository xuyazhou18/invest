package com.saferich.invest.vp.main.me.register;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.MD5Utils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.LoginBean;
import com.saferich.invest.model.bean.Response;
import com.saferich.invest.model.event.loginEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class RegsiterPresenter extends BasePresenter<IRegisterView> {

    @Inject
    Activity activity;

    @Inject
    public RegsiterPresenter() {
    }

    public void verifCode(String number) {

        params.put("p_mobile", number);
        Log.d("xyz", "number----->" + number);
        apiWrapper.getRegistCode(context, params).subscribe(newSubscriber((Object ob) -> {
            if (ob == null) {
                ShowToast.Short(context, "验证码发送成功");

            }
        }));
    }

    public void register(EditText editTextCode, EditText password, String number) {
        params.put("p_verCode", editTextCode.getText().toString().trim());
        params.put("p_password", password.getText().toString().trim());
        params.put("p_mobile", number);
        params.put("p_channel", 2);

        apiWrapper.register(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .doOnCompleted(() -> login(password, number))
                .subscribe(newSubscriber((Object object) -> {
                    if (object == null) {
                        ShowToast.Short(context, "注册成功");
                    }
                }));

    }

    public void login(EditText password, String number) {


        apiWrapper.getNonce(context).
                flatMap((Func1<Response<LoginBean>, Observable<?>>) loginBeanResponse -> {
                    params.put("p_loginName", number);
                    params.put("p_deviceType", "1");
                    params.put("p_password", MD5Utils.MD5(number + ":" +
                            MD5Utils.MD5(password.getText().toString().trim()) +
                            ":" + loginBeanResponse.data.getNonce()));
                    return apiWrapper.login(context, params,
                            loginBeanResponse.data.getRandomToken());
                }).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe(newSubscriber((Userinfo user) -> {
                    userinfo.accessToken = user.getAccessToken();
                    userinfo.memberId = user.getMemberId();
                    userinfo.update();

                    config.isLogin = true;
                    config.update();
                    if (rxBus.hasObservers()) {
                        rxBus.send(new loginEvent(user.accessToken));
                    }

                    view.succefull();


                }));

    }
}
