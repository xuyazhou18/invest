package com.saferich.invest.vp.main.discover.more.aboutApp;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.AndroidUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 关于app界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class AboutAppActivity extends BaseActivity<AboutAppPresenter> {
    @BindView(R.id.app_vision)
    TextView appVision;
    @BindView(R.id.eula_layout)
    RelativeLayout eulaLayout;
    @BindView(R.id.safe_layout)
    RelativeLayout safeLayout;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.update_layout)
    RelativeLayout updateLayout;
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_about_app;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("了解APP");
        appVision.setText("APP " + AndroidUtil.getAppVersionName(this));
        updateClick();
    }

    @OnClick(R.id.eula_layout)
    public void eulaClick() {
        presenter.getEulaData();
    }

    @OnClick(R.id.safe_layout)
    public void safeClick() {
        presenter.safeData();
    }


    public void updateClick() {
        RxView.clicks(updateLayout)
                .throttleFirst(500, TimeUnit.MILLISECONDS)

                .subscribe(aVoid -> {

                    presenter.getData();
                });

    }

}
