package com.saferich.invest.vp.main.home.myList;

import android.support.v4.app.Fragment;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class EndPresenter extends BasePresenter<IEndView> {
    private ArrayList<Complete> profitList;
    int page;
    @Inject
    Fragment fragment;

    private boolean isloadMore;


    @Inject
    public EndPresenter() {
        super();
        profitList = new ArrayList<>();
    }

    public void refresh() {
        page = 1;
        isloadMore = false;
        getdata();
    }

    private void getdata() {

       

        apiWrapper.getCompleteOrder(fragment)
                .subscribe(newSubscriber((List<Complete> list) -> {
                    if (isloadMore) {
                        if (list.size() > 0) {
                            profitList.addAll(list);
                        } else {
                            ShowToast.Short(context, "数据已经全部加载完毕");
                        }
                    } else {
                        profitList.clear();
                        profitList.addAll(list);


                    }

                    view.showData(profitList);
                }));
    }

    public void loadMore() {
        isloadMore = true;
        page++;
      //  getdata();
    }

    public boolean isloadMore() {
        return isloadMore;
    }
}
