package com.saferich.invest.vp.main.home.list;

import android.app.Activity;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Invest;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestPresenter extends BasePresenter<IInvestListView> {

    @Inject
    Activity activity;

    @Inject
    public InvestPresenter() {
        investList = new ArrayList<>();

    }


    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<Invest> investList;
    private int currentPage = 1;

    public void setType(String type) {
        this.type = type;
    }

    private String type;


    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
    }

    private void getData() {
        params.clear();


        params.put("p_page", currentPage);
        params.put("p_maxRowNum", 10);
        params.put("p_fundFlowType", type);


        apiWrapper.getFundFlow(activity, params).subscribe(newSubscriber((List<Invest> list) -> {
            if (isloadMore) {
                if (investList.size() > 0) {
                    investList.addAll(list);
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                investList.clear();

                investList.addAll(list);


            }

             view.showData((ArrayList<Invest>) investList);
        }));

    }

    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }
}
