package com.saferich.invest.vp.base;

/**
 * 可用与底部tab或顶部tab
 */
public interface ITabView extends IBaseView{

    /**
     * 获取当前选择的tab索引
     */
    int getCurrentTabIndex();

    /**
     * 获取所有tab数目
     */
    int getTabCount();


    /**
     * 根据索引切换tab
     */
    void switchTab(int tabIndex);

//    /**
//     * 添加Fragment
//     *
//     * @param fragment
//     */
//    public void addCenterFragment(Fragment fragment);
//
//
//    /**
//     * 根据索引获取Fragment
//     */
//    public Fragment getCenterFragmentAtIndex(int index);

}
