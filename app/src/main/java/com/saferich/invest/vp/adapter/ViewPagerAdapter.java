package com.saferich.invest.vp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2015-03-25
 */
public class ViewPagerAdapter extends PagerAdapter {
    private ArrayList<ImageView> imageViews;
    private Context context;
    private itemOnclickListener listener;

    public ViewPagerAdapter(ArrayList<ImageView> images, Context context) {
        super();
        this.imageViews = images;
        this.context = context;

    }

    @Override
    public int getCount() {
        return imageViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // super.destroyItem(container, position, object);
        container.removeView(imageViews.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        container.addView(imageViews.get(position));

        ImageView view = imageViews.get(position);

        view.setScaleType(ImageView.ScaleType.CENTER_CROP);

        view.setOnClickListener(v -> {
            if (position == imageViews.size() - 1) {
                listener.itemLastClick();
            }
        });

        return imageViews.get(position);

    }

    public void setListener(itemOnclickListener listener) {
        this.listener = listener;
    }

    public interface itemOnclickListener {
        void itemLastClick();
    }

}
