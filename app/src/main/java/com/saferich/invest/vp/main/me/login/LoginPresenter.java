package com.saferich.invest.vp.main.me.login;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.MD5Utils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.LoginBean;
import com.saferich.invest.model.bean.Response;
import com.saferich.invest.model.event.loginEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class LoginPresenter extends BasePresenter<ILoginView> {

    @Inject
    Activity activity;

    @Inject
    public LoginPresenter() {
    }

    public void login(EditText password) {
        showDialogView(R.string.authing, false);
        apiWrapper.getNonce(context).

                flatMap((Func1<Response<LoginBean>, Observable<?>>) loginBeanResponse -> {

                    params.put("p_loginName", activity.getIntent().getStringExtra("number"));
                    params.put("p_deviceType", "1");
                    params.put("p_password",
                            MD5Utils.MD5(activity.getIntent().getStringExtra("number")
                                    + ":" +
                                    MD5Utils.MD5(password.getText().toString().trim()) +
                                    ":" + loginBeanResponse.data.getNonce()));
                    return apiWrapper.login(context, params,
                            loginBeanResponse.data.getRandomToken());
                }).
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Userinfo user) -> {

                    userinfo.accessToken = user.getAccessToken();
                    userinfo.memberId = user.getMemberId();
                    userinfo.update();
                    Log.d("xyz", "token------->" + userinfo.getAccessToken());


                    getUserCountMsg(user.accessToken);

                    if (rxBus.hasObservers()) {
                        rxBus.send(new loginEvent(user.accessToken));
                    }


                }));

    }


    public void getUserCountMsg(String token) {
        apiWrapper.getAccountMessage(context, token)
                .subscribe(newSubscriber((Account data) -> {


                    userinfo.isBindCard = data.isIfBindCard();
                    userinfo.balance = data.getBalance();
                    userinfo.totalMoney = data.getTotalMoney();
                    userinfo.isSetPayWord = data.isIfSetPayPassword();
                    userinfo.ifCardUsed = data.isIfCardUsed();
                    userinfo.ifNewInvest = data.isIfNewInvest();

                    userinfo.mobliePhone = data.getBindMobilePhone();
                    userinfo.userLoginPhoneNumber = data.getMobile();
                    if (data.isIfBindCard()) {
                        userinfo.bankCard = data.getBankCard();
                        userinfo.bankCode = data.getBankCode();
                    }

                    userinfo.update();
                    if (activity.getIntent().getBooleanExtra("ispaygesture", false)) {
                        ShowToast.Short(context, "验证密码成功");
                    } else {
                        ShowToast.Short(context, "登录成功");
                    }
                    config.isLogin = true;

                    config.update();

                    activity.setResult(Activity.RESULT_OK);

                    view.suecessful();
                    activity.finish();
                }));
    }

}
