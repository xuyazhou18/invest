package com.saferich.invest.vp.main.discover.more.aboutApp;

import android.content.Intent;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;
import com.saferich.invest.vp.common.WebDeatilsTxtActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class AboutAppPresenter extends BasePresenter<IBaseView> {
    @Inject
    public AboutAppPresenter() {
    }

    public void getEulaData() {
        apiWrapper.getAgrement(context).subscribe(newSubscriber((String data) -> {
            if (data != null) {
                Intent intent = new Intent(context, WebDeatilsTxtActivity.class);
                intent.putExtra("txt", data);
                intent.putExtra("title", "服务协议");
                context.startActivity(intent);
            }
        }));
    }

    public void safeData() {
        apiWrapper.getsafe(context).subscribe(newSubscriber((String data) -> {
            if (data != null) {
                Intent intent = new Intent(context, WebDeatilsTxtActivity.class);
                intent.putExtra("txt", data);
                intent.putExtra("title", "安全保障");
                context.startActivity(intent);
            }
        }));
    }

    public void getData() {
        apiWrapper.getUpdate(context).subscribe(newSubscriber((String data) -> {
            ShowToast.Short(context, "已经是最新版了");
            // ActivityUtil.moveToActivity((BaseActivity) context, UpdateDialogActivity.class);

        }));
    }
}
