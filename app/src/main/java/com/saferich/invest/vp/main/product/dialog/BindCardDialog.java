package com.saferich.invest.vp.main.product.dialog;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.bank.BindBankActivity;

import butterknife.OnClick;

/**
 *
 * 绑定银行卡弹窗
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class BindCardDialog extends BaseActivity<BindCardPresenter> {
    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bindcard_dialog;
    }


    @OnClick(R.id.btn_bind)
    public void bindClick() {
        ActivityUtil.moveToActivity(this, BindBankActivity.class);
        this.finish();
    }

    @OnClick(R.id.cancel)
    public void cancelClick() {
        finish();
    }
}
