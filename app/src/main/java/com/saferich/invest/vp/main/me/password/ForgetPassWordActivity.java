package com.saferich.invest.vp.main.me.password;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.RxUtils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * 找回登陆密码
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-08
 */

public class ForgetPassWordActivity extends BaseActivity<SetPassWordPresenter> implements IPasswordView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.send_code)
    Button sendCode;
    @BindView(R.id.clear_code)
    ImageView delIcon;
    @BindView(R.id.editText_code)
    EditText editTextCode;
    @BindView(R.id.cold_tips)
    TextView codeTips;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.del)
    ImageView del;
    @BindView(R.id.password_tips)
    TextView passwordTips;
    @BindView(R.id.ensure_password)
    EditText ensurePassword;
    @BindView(R.id.clear_name)
    ImageView clearName;
    @BindView(R.id.ensure_tips)
    TextView ensureTips;
    @BindView(R.id.btn_set)
    Button btnSet;

    @Inject
    Userinfo userinfo;

    private String number;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_set_password;
    }

    @Override
    public void succefull() {
        ShowToast.Short(this, "找回密码成功");
        this.finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        centerTitle.setText("找回登录密码");

        if (getIntent().getStringExtra("number") == null) {
            number = userinfo.getUserLoginPhoneNumber();
        } else {
            number = getIntent().getStringExtra("number");
        }

        title.setText("本次操作需要信息确认,验证码已经发送到您的手机:" +
                StringUtil.replaceNumber(number) + ",请注意查收");

        addTextWatch();
        verifCodeClick();
        passwordClick();
        presenter.verifCode(number);
        doCountTime();
    }


    @OnClick(R.id.clear_code)
    public void codeClearClick() {
        editTextCode.setText("");
    }

    @OnClick(R.id.del)
    public void pwClearClick() {
        password.setText("");
    }

    @OnClick(R.id.clear_name)
    public void ensureClearClick() {
        ensurePassword.setText("");
    }


    public void verifCodeClick() {
        RxView.clicks(sendCode)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .doOnCompleted(this::doCountTime)
                .subscribe(aVoid -> {
                    presenter.verifCode(number);
                    doCountTime();
                });

    }

    public void passwordClick() {
        RxView.clicks(btnSet)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> {
                    if (StringUtil.checkIsNotEmpty(editTextCode)) {
                        return true;
                    } else {

                        codeTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .filter(aVoid -> {
                    if (StringUtil.checkIsNotEmpty(password)) {
                        return true;
                    } else {

                        passwordTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .filter(aVoid -> StringUtil.checkBigLenght(password, 6,
                        R.string.string_length))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(ensurePassword,
                        R.string.empty_new_password))
                .filter(aVoid -> StringUtil.checkBigLenght(ensurePassword, 6,
                        R.string.string_length))

                .filter(aVoid -> {
                    if (StringUtil.checkIsEqual(password, ensurePassword)) {
                        return true;
                    } else {
                        ensureTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .filter(aVoid -> StringUtil.isAlphabeticOrNumberic(password,
                        R.string.string_fakut))
                .subscribe(aVoid ->
                        presenter.setpassword(editTextCode, password, number));
    }


    public void doCountTime() {


        RxUtils.Countdown(60).
                subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                        sendCode.setText("获取验证码");
                        sendCode.setBackgroundColor(getResources().getColor(R.color.material_red));
                        sendCode.setEnabled(true);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        sendCode.setEnabled(false);
                        sendCode.setBackgroundColor(getResources().getColor(R.color.code_background));
                        sendCode.setText("重新获取(" + integer + ")秒");
                    }
                });
    }


    private void addTextWatch() {
        editTextCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                codeTips.setVisibility(View.GONE);
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordTips.setVisibility(View.GONE);
                ensureTips.setVisibility(View.GONE);
            }
        });
        ensurePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ensureTips.setVisibility(View.GONE);
            }
        });
    }

}
