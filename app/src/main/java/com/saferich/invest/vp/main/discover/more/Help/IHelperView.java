package com.saferich.invest.vp.main.discover.more.Help;

import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public interface IHelperView extends IBaseView {
    void showListData(ArrayList<Question> questionlist);
}
