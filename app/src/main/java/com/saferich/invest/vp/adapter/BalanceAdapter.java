package com.saferich.invest.vp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.Balance;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 余额列表明细的适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class BalanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Balance> balanceList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private String count;

    @Inject
    public BalanceAdapter() {
        balanceList = new ArrayList<>();
    }


    public void SetListData(ArrayList<Balance> balanceList, String count) {

        this.balanceList = balanceList;
        // this.count = count;
        notifyDataSetChanged();
    }

    public void SetHeadData(String count) {

        this.count = count;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_balance, parent, false);

            return new ItemViewHolder(view);
        }

        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_balance_header, parent, false);
            return new HeaderViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.bindTo(balanceList.get(position - 1));


        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.setIsRecyclable(false);
            headerViewHolder.bindTo(count);
        }

    }

    @Override
    public int getItemCount() {
        return balanceList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.rmb)
        TextView rmb;
        @BindView(R.id.count)
        TextView count;
        @BindView(R.id.txt)
        TextView txt;
        @BindView(R.id.reason)
        TextView reason;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(Balance balance) {

            time.setText(TimeUtils.getFormatTime(balance.getCreateDate(), "yyyy/MM/dd HH:mm:ss"));

            if (balance.getFundsFrom().equals("账户余额")) {
                count.setText("-" + StringUtil.repaceE(balance.getMoney()));
                count.setTextColor(reason.getContext().getResources().getColor(R.color.balance_color_down));
                rmb.setTextColor(reason.getContext().getResources().getColor(R.color.balance_color_down));
            } else {
                count.setText("+" + StringUtil.repaceE(balance.getMoney()));
                count.setTextColor(reason.getContext().getResources().getColor(R.color.balance_color_up));
                rmb.setTextColor(reason.getContext().getResources().getColor(R.color.balance_color_up));
            }
            txt.setText(balance.getType());
            reason.setText(balance.getFundsFrom() + " -----> " + balance.getFundsTo());


            if (balance.getStatusName() == null) {
                status.setText("");
            } else {
                status.setText("(" + balance.getStatusName() + ")");
            }

            RxView.clicks(itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
//                        Intent intent = new Intent(itemLayout.getContext(),
//                                ProductDeatilsActivity.class);
//                        intent.putExtra("showType", Constant.productType.recommend);
//
//                        itemLayout.getContext().startActivity(intent);
                    });
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.money_count)
        TextView moneyCount;
        @BindView(R.id.money_title)
        TextView moneyTitle;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(String count) {
            if (count == null) {
                return;
            }
            moneyCount.setText(count);
        }
    }
}
