package com.saferich.invest.vp.main.discover.more.update;

import android.os.Bundle;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;

/**
 * app更新界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-01
 */

public class UpdateDialogActivity extends BaseActivity<UpatePresenter> {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.vision)
    TextView vision;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.update_txt)
    TextView updateTxt;
    @BindView(R.id.update_txt2)
    TextView updateTxt2;
    @BindView(R.id.update)
    TextView update;
    @BindView(R.id.cancel)
    TextView cancel;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_update;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(true);
    }
}
