package com.saferich.invest.vp.main.discover.more.feedback;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;

/**
 *
 * 用户反馈界面
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class FeedbackActivity extends BaseActivity<FeedbackPresenter> {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.editView)
    EditText editView;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("用户反馈");
        submitClick();
    }

    public void submitClick() {
        RxView.clicks(btnSubmit)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(editView,
                        R.string.feedBack_is_no_empty))

                .subscribe(aVoid -> {

                    presenter.getData(editView.getText().toString().trim());
                });
    }
}
