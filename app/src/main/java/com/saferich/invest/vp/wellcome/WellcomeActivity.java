package com.saferich.invest.vp.wellcome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.MainActivity;

/**
 * app的第一个界面
 * 欢迎界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-30
 */

public class WellcomeActivity extends BaseActivity<WellcomePresenter> implements IWellcomeView {
    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_wellcome;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(WellcomeActivity.this, MainActivity.class));
                WellcomeActivity.this.finish();
            }

        }, 500);//延时500毫秒跳转主界面
    }
}
