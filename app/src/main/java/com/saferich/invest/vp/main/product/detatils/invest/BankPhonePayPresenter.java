package com.saferich.invest.vp.main.product.detatils.invest;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class BankPhonePayPresenter extends BasePresenter<IBankPhoneView> {

    @Inject
    Activity activity;

    @Inject
    public BankPhonePayPresenter() {
    }


    public void bindCard(EditText editText, Bundle extras) {

        params.clear();
        params.put("p_orderId", activity.getIntent().getStringExtra("orderId"));
        params.put("p_VerificationCode", editText.getText().toString().trim());

        apiWrapper.ordConfirmPay(context, params).subscribe(newSubscriber((OrderInfo data) -> {
            sendSub();
            view.moveActivity(data);

        }));


    }


    public void sendSub() {
        if (rxBus.hasObservers()) {
            rxBus.send(new InvestSuccessEvent(userinfo.accessToken));
        }
    }

    public void code() {
        params.clear();
        params.put("p_orderId", activity.getIntent().getStringExtra("orderId"));
        apiWrapper.OrdsendVerificationCode(context, params)
                .subscribe(newSubscriber((String data) -> {
                    ShowToast.Short(context, "验证码发送成功");
                }));
    }
}
