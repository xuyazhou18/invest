package com.saferich.invest.vp.main.discover.noticetrend.notice;

import com.saferich.invest.model.bean.Notice;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public interface INoticeView extends IBaseView{
    void showListData(ArrayList<Notice> data);
}
