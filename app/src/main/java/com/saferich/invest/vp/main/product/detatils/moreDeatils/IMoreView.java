package com.saferich.invest.vp.main.product.detatils.moreDeatils;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-22
 */

public interface IMoreView extends IBaseView{
    void showPdf(String url, String name);
}
