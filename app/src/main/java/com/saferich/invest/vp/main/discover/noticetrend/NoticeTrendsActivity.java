package com.saferich.invest.vp.main.discover.noticetrend;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 公告和新动态主界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class NoticeTrendsActivity extends BaseActivity<NoticeTrendsPresenter> implements INoticeTrendsView, ViewPager.OnPageChangeListener {


    @BindView(R.id.notice)
    Button notice;
    @BindView(R.id.trend)
    Button trend;
    @BindView(R.id.button_layout)
    LinearLayout buttonLayout;


    private Fragment[] fragments;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_notice_trend;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initFragment();
    }

    private void initFragment() {


        fragmentManager = getSupportFragmentManager();
        fragments = new Fragment[2];

        fragments[0] = fragmentManager.findFragmentById(R.id.notcieFragment);
        fragments[1] = fragmentManager.findFragmentById(R.id.trendFragment);
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);

        transaction.show(fragments[0]).commit();


    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                noticeClick();
                break;
            case 1:
                trendClick();
                break;

        }
    }

    @OnClick(R.id.notice)
    public void trendClick() {
        notice.setBackgroundResource(R.mipmap.top_bg01_on);
        notice.setTextColor(getResources().getColor(R.color.colorPrimary));

        trend.setBackgroundResource(R.mipmap.top_bg04);
        trend.setTextColor(getResources().getColor(R.color.white));
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[0]).commit();
    }

    @OnClick(R.id.trend)
    public void noticeClick() {

        notice.setBackgroundResource(R.mipmap.top_bg01);
        notice.setTextColor(getResources().getColor(R.color.white));
        trend.setBackgroundResource(R.mipmap.top_bg04_on);
        trend.setTextColor(getResources().getColor(R.color.colorPrimary));
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[1]).commit();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
