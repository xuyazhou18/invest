package com.saferich.invest.vp.main.product.dialog;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class BindCardPresenter extends BasePresenter<IBaseView> {

    @Inject
    public BindCardPresenter() {
    }
}
