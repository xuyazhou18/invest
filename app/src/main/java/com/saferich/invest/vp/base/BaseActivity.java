package com.saferich.invest.vp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.saferich.invest.MyApplication;
import com.saferich.invest.R;
import com.saferich.invest.model.inject.components.ActivityComponent;
import com.saferich.invest.model.inject.components.AppComponent;
import com.saferich.invest.model.inject.components.DaggerActivityComponent;
import com.saferich.invest.model.inject.models.ActivityModule;
import com.saferich.invest.vp.main.MainActivity;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.umeng.analytics.MobclickAgent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * activity 的基类
 * Author: lampard_xu(xuyazhou18@gmail.com)
 *
 * Date: 2016-03-09
 */
public abstract class BaseActivity<P extends BasePresenter> extends RxAppCompatActivity implements IBaseView {

    @Inject
    protected P presenter;//控制器
    @Inject
    MyApplication app;//全局的appLication
    @Nullable
    @BindView(R.id.toolbar)//全局的toolbar
    protected
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivityComponent();//创建每个activity的dagger component
        setContentView(getLayoutResourceId());//填充当前的布局
        ButterKnife.bind(this);//view的注入绑定
        presenter.setView(this);//控制器的创建

//        pushAgent.onAppStart();
//        pushAgent.enable();

        app.addActivity(this);//记录每一个创建的activity


        configureToolbar();//配置每一个布局的头布局

    }

    private void configureToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (this instanceof MainActivity) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.mipmap.back_icon);

                toolbar.setNavigationOnClickListener(v -> finish());
            }

        }
    }

    @Override
    protected void onDestroy() {

        if (presenter != null) {
            presenter.onDestroy();
        }
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);//友盟的后台统计

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);//友盟的后台统计
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    ActivityComponent activityComponent;

    //得到注入的activityComponent
    protected ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .appComponent(getAppComponent())
                    .activityModule(getActivityModule())
                    .build();
        }
        return activityComponent;
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    protected AppComponent getAppComponent() {
        return ((MyApplication) getApplication()).getAppComponent();
    }

    protected abstract void setupActivityComponent();

    protected abstract int getLayoutResourceId();


}
