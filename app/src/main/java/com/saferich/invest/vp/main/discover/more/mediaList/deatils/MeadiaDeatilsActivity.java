package com.saferich.invest.vp.main.discover.more.mediaList.deatils;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;

/**
 * 媒体详情界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class MeadiaDeatilsActivity extends BaseActivity<MeadiaDeatilsPresenter> implements IMediaDeailsView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.webView)
    WebView webView;


    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_media_deatils;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("媒体详情");

        presenter.getdata();

    }


    @Override
    public void showData(Article article) {
        title.setText(article.getTitle());
        presenter.setWebView(webView, article.getContent());
        author.setText(article.getAuthor());
        time.setText(TimeUtils.getFormatTime(article.getPublishTime(), "yyyy/MM/dd HH:mm:ss"));
    }
}
