package com.saferich.invest.vp.main.me.bank;

import com.saferich.invest.model.bean.BankItem;
import com.saferich.invest.vp.base.IBaseView;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public interface IBankListView extends IBaseView{
    void showData(List<BankItem> bankList);
}