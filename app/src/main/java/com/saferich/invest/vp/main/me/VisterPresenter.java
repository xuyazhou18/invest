package com.saferich.invest.vp.main.me;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-06
 */

public class VisterPresenter extends BasePresenter<MePresenter.IVisterView> {
    @Inject
    public VisterPresenter() {
    }
}
