package com.saferich.invest.vp.main.home.list;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.InvestListAdapter;
import com.saferich.invest.common.recyclerview.EndlessRecyclerOnScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.Invest;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * 全部投资列表
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestListActivity extends BaseActivity<InvestPresenter> implements IInvestListView {
    @BindView(R.id.all)
    Button all;
    @BindView(R.id.invest)
    Button invest;
    @BindView(R.id.profit_click)
    Button profit;
    @BindView(R.id.reback)
    Button reback;
    @BindView(R.id.button_layout)
    LinearLayout buttonLayout;
    @BindView(R.id.invest_count)
    TextView investCount;
    @BindView(R.id.investing_count)
    TextView investingCount;
    @BindView(R.id.title_layout)
    LinearLayout titleLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Inject
    InvestListAdapter adapter;
    @BindView(R.id.profit_count)
    TextView profitCount;
    @BindView(R.id.profit_layout)
    LinearLayout profitLayout;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_invest_list;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    @OnClick(R.id.all)
    public void allClick() {
        all.setBackgroundResource(R.drawable.base_button_home_tab);
        all.setTextColor(getResources().getColor(R.color.colorPrimary));
        invest.setBackgroundColor(Color.TRANSPARENT);
        invest.setTextColor(getResources().getColor(R.color.white));
        profit.setBackgroundColor(Color.TRANSPARENT);
        profit.setTextColor(getResources().getColor(R.color.white));
        reback.setBackgroundColor(Color.TRANSPARENT);
        reback.setTextColor(getResources().getColor(R.color.white));
        presenter.setType("投资,收益,回款");
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);
        titleLayout.setVisibility(View.GONE);
        profitLayout.setVisibility(View.GONE);

    }

    @OnClick(R.id.invest)
    public void investClick() {
        invest.setBackgroundResource(R.drawable.base_button_home_tab);
        invest.setTextColor(getResources().getColor(R.color.colorPrimary));
        all.setBackgroundColor(Color.TRANSPARENT);
        all.setTextColor(getResources().getColor(R.color.white));
        profit.setBackgroundColor(Color.TRANSPARENT);
        profit.setTextColor(getResources().getColor(R.color.white));
        reback.setBackgroundColor(Color.TRANSPARENT);
        reback.setTextColor(getResources().getColor(R.color.white));
        presenter.setType("投资");
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);
        titleLayout.setVisibility(View.VISIBLE);
        profitLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.profit_click)
    public void profitClick() {
        profit.setBackgroundResource(R.drawable.base_button_home_tab);
        profit.setTextColor(getResources().getColor(R.color.colorPrimary));
        all.setBackgroundColor(Color.TRANSPARENT);
        all.setTextColor(getResources().getColor(R.color.white));
        invest.setBackgroundColor(Color.TRANSPARENT);
        invest.setTextColor(getResources().getColor(R.color.white));
        reback.setBackgroundColor(Color.TRANSPARENT);
        reback.setTextColor(getResources().getColor(R.color.white));
        presenter.setType("收益");
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);
        titleLayout.setVisibility(View.GONE);
        profitLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.reback)
    public void rebackClick() {
        reback.setBackgroundResource(R.drawable.base_button_home_tab);
        reback.setTextColor(getResources().getColor(R.color.colorPrimary));
        all.setBackgroundColor(Color.TRANSPARENT);
        all.setTextColor(getResources().getColor(R.color.white));
        profit.setBackgroundColor(Color.TRANSPARENT);
        profit.setTextColor(getResources().getColor(R.color.white));
        invest.setBackgroundColor(Color.TRANSPARENT);
        invest.setTextColor(getResources().getColor(R.color.white));
        presenter.setType("回款");
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);
        titleLayout.setVisibility(View.GONE);
        profitLayout.setVisibility(View.GONE);
    }

    private void initUI() {

        investCount.setText(getIntent().getStringExtra("totalMoney"));
        investingCount.setText(getIntent().getStringExtra("currentMoney"));
        profitCount.setText(getIntent().getStringExtra("profit"));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(this, 2)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);

                presenter.loadMore();
            }
        });

        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });


        presenter.setType("投资,收益,回款");
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);
    }

    @Override
    public void showData(ArrayList<Invest> data) {
        if (presenter.isloadMore()) {

            //  refresh.finishRefreshLoadMore();
        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            ptrFrame.refreshComplete();

        }

        adapter.setDataList(data);
    }
}
