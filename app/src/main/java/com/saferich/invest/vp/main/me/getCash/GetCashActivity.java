package com.saferich.invest.vp.main.me.getCash;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.phone.InputVerCodeActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * 提现界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public class GetCashActivity extends BaseActivity<GetCashPresenter> implements ICashView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.left_image)
    ImageView leftImage;
    @BindView(R.id.left_txt)
    TextView leftTxt;
    @BindView(R.id.card_number)
    TextView cardNumber;
    @BindView(R.id.bank_iamge)
    ImageView bankIamge;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.imageView5)
    ImageView imageView5;
    @BindView(R.id.account)
    TextView account;
    @BindView(R.id.total_icon)
    ImageView totalIcon;
    @BindView(R.id.cash_count)
    EditText cashCount;
    @BindView(R.id.total_layout)
    RelativeLayout totalLayout;
    @BindView(R.id.tips)
    TextView tips;
    @BindView(R.id.cash_tips)
    TextView cashTips;
    @BindView(R.id.btn_cash)
    Button btnCash;
    @BindView(R.id.cash_time)
    TextView cashTime;

    @Inject
    Userinfo userinfo;
    private long limitCount;
    private long minLimitCount;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_get_cash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("提现");
        cardNumber.setText("尾号" + userinfo.bankCard);
        account.setText("账户余额:" + StringUtil.repaceE(userinfo.balance) + "元");
        bankIamge.setImageResource(BankUtils.bankLogo.get(userinfo.bankCode));

        StringUtil.setPricePoint(cashCount);
        presenter.init();
        nextClick();

    }

    @Override
    public void showData(List<CashItem> list) {

        if (list.get(0).getStatus() == 0) {
            minLimitCount = list.get(0).getValue();
            cashCount.setHint("最低提现金额" + list.get(0).getValue() + "元");
        } else {
            minLimitCount = 0;
        }

        if (list.get(1).getStatus() == 0 && list.get(2).getStatus() == 0) {
            cashTips.setText("每笔限额" + list.get(1).getValue() + "元,每日限" + list.get(2).getValue() + "笔");
            limitCount = list.get(1).getValue();
        } else if (list.get(1).getStatus() == 1 && list.get(2).getStatus() == 0) {
            cashTips.setText("每日限" + list.get(2).getValue() + "笔");
            limitCount = 0;
        } else if (list.get(1).getStatus() == 0 && list.get(2).getStatus() == 1) {
            cashTips.setText("每笔限额" + list.get(1).getValue() + "元");
            limitCount = list.get(1).getValue();
        } else if (list.get(1).getStatus() == 1 && list.get(1).getStatus() == 1) {
            limitCount = 0;
        }


    }

    private void nextClick() {
        RxView.clicks(btnCash)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(cashCount,
                        R.string.cash_count_isnot_empty))
                .subscribe(aVoid -> {
                    if (Double.valueOf(cashCount.getText().toString()) > limitCount && limitCount != 0) {
                        ShowToast.Short(this, "提现超过限额");
                        return;
                    }
                    if (Double.valueOf(cashCount.getText().toString()) < minLimitCount && minLimitCount != 0) {
                        ShowToast.Short(this, "最低提现金额" + minLimitCount + "元");
                        return;
                    }

                    if (Double.valueOf(cashCount.getText().toString().trim()) > Double.valueOf(StringUtil.repaceE(userinfo.getBalance()))) {
                        ShowToast.Short(this, "余额不足");
                        return;
                    }

                    Intent intent = new Intent(this, InputVerCodeActivity.class);
                    intent.putExtra("count", cashCount.getText().toString().trim());
                    intent.putExtra("isGetCash", true);
                    intent.putExtra("data", userinfo.getMobliePhone());
                    startActivity(intent);

                    finish();
                });
    }
}
