package com.saferich.invest.vp.main.me.bank;

import com.google.gson.Gson;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.model.bean.BankItem;
import com.saferich.invest.model.cache.CacheManager;
import com.saferich.invest.model.event.BankEvent;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public class BankListPresenter extends BasePresenter<IBankListView> {

    @Inject
    CacheManager cacheManager;

    @Inject
    public BankListPresenter() {
    }

    public void refresh() {


        getData();
    }

    private void getData() {
        apiWrapper.getBankNameList(context).subscribe(newSubscriber((List<BankItem> list) -> {

            view.showData(list);

            cacheManager.insertChache(new Gson().toJson(BankUtils.SavaBank(list)), "bankList");

        }));
    }


    public void sendbSub(String name, String code) {
        if (rxBus.hasObservers()) {
            rxBus.send(new BankEvent(name, code));
        }
    }
}
