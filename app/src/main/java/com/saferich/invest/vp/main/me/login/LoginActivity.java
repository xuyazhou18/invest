package com.saferich.invest.vp.main.me.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.password.ForgetPassWordActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 输入密码登陆界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class LoginActivity extends BaseActivity<LoginPresenter> implements ILoginView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.clear_name)
    ImageView del2;
    @BindView(R.id.passwordTips)
    TextView passwordTips;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.toast_layout)
    LinearLayout toastLayout;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_input_password;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("验证登录密码");
        login();
    }


    public void login() {
        RxView.clicks(btnLogin)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(password,
                        R.string.phone_number_isnot_empty))
                .subscribe(aVoid ->
                        presenter.login(password));
    }

    @OnClick(R.id.clear_name)
    public void clearClick() {
        password.setText("");
    }

    @OnClick(R.id.forget_password)
    public void forgetPassClick() {
        Bundle bundle = new Bundle();
        bundle.putString("number", getIntent().getStringExtra("number"));
        ActivityUtil.moveToActivity(this, ForgetPassWordActivity.class, bundle);
    }

    @Override
    public void suecessful() {
        toastLayout.setVisibility(View.VISIBLE);
    }
}
