package com.saferich.invest.vp.main.product.detatils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ProductUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.widget.CircleProgressBar;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.product.detatils.element.ElementFragment;
import com.saferich.invest.vp.main.product.detatils.moreDeatils.MoreDeatilsFragment;
import com.saferich.invest.vp.main.product.detatils.phone.PhoneDialogActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 产品详情界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-10
 */
public class ProductDeatilsActivity extends BaseActivity<DeatilsPresenter> implements IDetealsView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.progress)
    CircleProgressBar progress;
    @BindView(R.id.rate)
    TextView rate;
    @BindView(R.id.current_progress)
    TextView currentProgress;
    @BindView(R.id.invest_count)
    TextView buyCount;
    @BindView(R.id.profit_time)
    TextView investTime;
    @BindView(R.id.deatils_line)
    View deatilsLine;
    @BindView(R.id.deatils_layout)
    RelativeLayout deatilsLayout;
    @BindView(R.id.element_line)
    View elementLine;
    @BindView(R.id.element_layout)
    RelativeLayout elementLayout;
    @BindView(R.id.phone_icon)
    ImageView phoneIcon;
    @BindView(R.id.btn_invest)
    Button btnInvest;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;
    @BindView(R.id.add_text)
    TextView addText;
    @BindView(R.id.floatText)
    TextView floatText;
    @BindView(R.id.prsent)
    TextView prsent;
    private Fragment[] fragments;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private MoreDeatilsFragment moreDeatilsFragment;
    private ElementFragment elementFragment;
    private ProductInfo productInfo;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_product_deatils;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragments = new Fragment[2];

        fragmentManager = getSupportFragmentManager();


        fragments[1] = fragmentManager.findFragmentById(R.id.fragment_deatils);
        moreDeatilsFragment = (MoreDeatilsFragment) fragments[1];
        fragments[0] = fragmentManager.findFragmentById(R.id.fragment_element);

        elementFragment = (ElementFragment) fragments[0];
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);

        transaction.show(fragments[0]).commit();
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        presenter.getData();


    }

    @OnClick(R.id.btn_invest)
    public void investClick() {

        if (productInfo.getSaleState().equals("Finan_OnSale")) {
            ProductUtils.checkLoginAndBind(productInfo.getId(), productInfo.getProductName(),
                    productInfo.getSubscriptionFee(), productInfo.getRemainMoney(), productInfo.getMinInvestMoney()
                    , productInfo.getPurchaseIncreaseMoney(),
                    this);
        }


    }

    @OnClick(R.id.phone_icon)
    public void phoneClick() {


        startActivity(new Intent(this, PhoneDialogActivity.class));
    }

    @OnClick(R.id.deatils_layout)
    public void deatilsClick() {
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[1]).commit();
        deatilsLine.setVisibility(View.VISIBLE);
        elementLine.setVisibility(View.GONE);

    }

    @OnClick(R.id.element_layout)
    public void elementClick() {
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[0]).commit();
        deatilsLine.setVisibility(View.GONE);
        elementLine.setVisibility(View.VISIBLE);

    }


    @Override
    public void backData(ProductInfo data) {
        this.productInfo = data;

        if (productInfo.getSaleState().equals("Finan_PreSale")) {
            btnInvest.setText("即将开放投资");
            btnInvest.setBackgroundResource(R.drawable.base_button_no);
        } else if (productInfo.getSaleState().equals("Finan_SellOut")) {
            btnInvest.setText("售罄");
            btnInvest.setBackgroundResource(R.drawable.base_button_sell);
        } else if (productInfo.getSaleState().equals("Finan_Disabled")) {
            btnInvest.setText("投资");
            btnInvest.setBackgroundResource(R.drawable.base_button_sell);
        } else {
            btnInvest.setText("投资");
            btnInvest.setBackgroundResource(R.drawable.base_button);
        }

        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        centerTitle.setText(data.getProductName());

        if (data.getProductProfitTypeCode().equals("FixedIncome")) {
            rate.setText(StringUtil.repaceE(data.getFixedProfit()) + "");
            rate.setVisibility(View.VISIBLE);
            prsent.setVisibility(View.VISIBLE);
            addText.setVisibility(View.GONE);
            floatText.setVisibility(View.GONE);


        }

        if (data.getProductProfitTypeCode().equals("FloatIncome")) {
            floatText.setText(data.getFloatProfit() + "");
            prsent.setVisibility(View.GONE);
            floatText.setVisibility(View.VISIBLE);
            addText.setVisibility(View.GONE);
            rate.setVisibility(View.GONE);
        }
        if (data.getProductProfitTypeCode().equals("Fixed&FloatIncome")) {
            floatText.setText(data.getFloatProfit() + "");
            rate.setText(StringUtil.repaceE(data.getFixedProfit()) + "");
            rate.setVisibility(View.VISIBLE);
            prsent.setVisibility(View.VISIBLE);
            addText.setVisibility(View.VISIBLE);
            floatText.setVisibility(View.VISIBLE);
        }


        currentProgress.setText("当前投资进度" + data.getInvestmentProgress() + "%");
        progress.setProgress((float) data.getInvestmentProgress());

        investTime.setText(data.getInvestTerm() + "天");
        moreDeatilsFragment.setData(data);
        elementFragment.setData(data);
        buyCount.setText(StringUtil.repaceEE(data.getMinInvestMoney()) + "元");

    }


}
