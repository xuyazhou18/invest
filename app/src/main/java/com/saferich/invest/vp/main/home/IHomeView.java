package com.saferich.invest.vp.main.home;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IHomeView extends IBaseView {
    void showFragment(boolean isLogin);
}
