package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.vp.main.home.deatils.InvestDeatilsActivity;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 我的投资已结束列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class CommpleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Complete> investList;


    public void setDataList(ArrayList<Complete> investList) {

        this.investList = investList;
        notifyDataSetChanged();

    }

    @Inject
    public CommpleListAdapter() {
        investList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complete, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(investList.get(position));


    }


    @Override
    public int getItemCount() {

        return investList.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.product_layout)
        RelativeLayout productLayout;
        @BindView(R.id.invest_count)
        TextView investCount;
        @BindView(R.id.profit_count)
        TextView profitCount;
        @BindView(R.id.title_layout)
        LinearLayout titleLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(Complete nonProfit) {


            productName.setText(nonProfit.getProductName());
            investCount.setText(StringUtil.repaceE(nonProfit.getInvestMoney()));
            profitCount.setText(StringUtil.repaceE(nonProfit.getTotalProfit()));

            RxView.clicks(titleLayout).subscribe(aVoid -> {
                Intent intent = new Intent(titleLayout.getContext(), InvestDeatilsActivity.class);
                intent.putExtra("id", nonProfit.getOrderId());
                intent.putExtra("productName", nonProfit.getProductName());
                titleLayout.getContext().startActivity(intent);
            });
            RxView.clicks(productLayout).subscribe(aVoid -> {
                Intent intent = new Intent(titleLayout.getContext(), ProductDeatilsActivity.class);
                intent.putExtra("id", nonProfit.getProductId());
                intent.putExtra("productName", nonProfit.getProductName());
                titleLayout.getContext().startActivity(intent);
            });


        }
    }


}
