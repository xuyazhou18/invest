package com.saferich.invest.vp.main.discover.more.mediaList.deatils;

import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public interface IMediaDeailsView extends IBaseView{
    void showData(Article article);
}
