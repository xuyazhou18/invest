package com.saferich.invest.vp.main.me.rechare;

import com.saferich.invest.model.bean.CardRecharge;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.vp.base.IBaseView;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public interface IRechargeView extends IBaseView {
    void showData(RechargeBean recharge);
    void showLimitData(List<CashItem> recharge);
    void moveActivity(CardRecharge data);
}
