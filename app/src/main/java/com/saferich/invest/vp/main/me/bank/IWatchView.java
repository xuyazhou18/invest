package com.saferich.invest.vp.main.me.bank;

import com.saferich.invest.model.bean.Bank;
import com.saferich.invest.vp.base.IBaseView;

import java.util.List;
import java.util.Map;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public interface IWatchView extends IBaseView {
    void showData(Bank data, Map<String,List<Object>> banklist);
}
