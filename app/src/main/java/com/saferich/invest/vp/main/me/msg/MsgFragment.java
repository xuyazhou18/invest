package com.saferich.invest.vp.main.me.msg;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.jayfang.dropdownmenu.DropDownMenu;
import com.jayfang.dropdownmenu.OnMenuSelectedListener;
import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.MsgAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.MsgBean;
import com.saferich.invest.vp.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的消息列表界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MsgFragment extends BaseFragment<MsgPresenter> implements IMsgView, MsgAdapter.MsgListener {


    @Inject
    MsgAdapter adapter;
    @BindView(R.id.menu)
    DropDownMenu mMenu;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh)
    MaterialRefreshLayout refresh;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;
    @BindView(R.id.check)
    CheckBox checkBox;


    private String[] sortItems = {"全部", "注册", "投资", "充值", "提现", "密码变更", "手机验证码"};
    private String headers[] = {"全部消息"};


    @Override
    protected void lazyLoad() {

        if (!isPrepared) {
            return;
        }

        if (!canLoadData(multiStateView, adapter)) {
            return;
        }


        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        refresh.autoRefresh();

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_msg, container,
                false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {


        mMenu.setmMenuCount(1);//Menu的个数
        mMenu.setmShowCount(6);//Menu展开list数量太多是只显示的个数
        mMenu.setShowCheck(true);//是否显示展开list的选中项
        mMenu.setmMenuTitleTextSize(14);//Menu的文字大小
        mMenu.setmMenuTitleTextColor(getResources().getColor(R.color.item_home_text));//Menu的文字颜色
        mMenu.setmMenuListTextSize(14);//Menu展开list的文字大小
        mMenu.setmMenuListTextColor(getResources().getColor(R.color.colorPrimaryDark));//Menu展开list的文字颜色
        mMenu.setmMenuBackColor(Color.WHITE);//Menu的背景颜色
        mMenu.setmMenuPressedBackColor(Color.WHITE);//Menu按下的背景颜色
        mMenu.setmCheckIcon(R.drawable.ico_make);//Menu展开list的勾选图片
        mMenu.setmUpArrow(R.drawable.arrow_up);//Menu默认状态的箭头
        mMenu.setmDownArrow(R.drawable.arrow_down);//Menu按下状态的箭头
        mMenu.setDefaultMenuTitle(headers);//默认未选择任何过滤的Menu title

        mMenu.setShowDivider(true);
        mMenu.setmMenuListBackColor(getResources().getColor(R.color.white));
        mMenu.setmMenuListSelectorRes(R.color.white);
        mMenu.setmArrowMarginTitle(20);
        mMenu.setMenuSelectedListener(new OnMenuSelectedListener() {
            @Override
            //Menu展开的list点击事件  RowIndex：list的索引  ColumnIndex：menu的索引
            public void onSelected(View listview, int RowIndex, int ColumnIndex) {
                if (ColumnIndex == 0) {
                    sortType(RowIndex);
                }

            }
        });

        List<String[]> items = new ArrayList<>();
        items.add(sortItems);
//        items.add(sortItems);
//        items.add(sortItems);
        mMenu.setmMenuItems(items);
        mMenu.setIsDebug(false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setMsgListener(this);

        refresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                presenter.refresh();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                presenter.loadMore();
            }
        });


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                readClick(isChecked);
            }
        });

        isPrepared = true;
        lazyLoad();
    }


    private void sortType(int position) {
        if (position == 0) {
            presenter.setType(null);
        } else if (position == 1) {
            presenter.setType("0");
        } else if (position == 2) {
            presenter.setType("2");
        } else if (position == 3) {
            presenter.setType("3");
        } else if (position == 4) {
            presenter.setType("4");
        } else if (position == 5) {
            presenter.setType("1");
        } else if (position == 6) {
            presenter.setType("5");
        }


        presenter.getData();
    }


    public void readClick(boolean ischeck) {
        if (ischeck) {
            adapter.setAllClear(true);
            presenter.readAll();
        }

    }

    @Override
    public void readClick(MsgBean.list msg) {
        presenter.readItem(msg.getId());
    }

    @Override
    public void showListData(ArrayList<MsgBean.list> data) {
        if (presenter.isloadMore()) {

            refresh.finishRefreshLoadMore();
        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            refresh.finishRefresh();

        }

        adapter.SetListData(data);
    }


    @OnClick(R.id.noread_layout)
    public void noreadClick() {
        if (checkBox.isChecked()) {
            checkBox.setChecked(false);
        } else {
            checkBox.setChecked(true);
        }
    }


}
