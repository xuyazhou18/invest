package com.saferich.invest.vp.main.me.bank;

import android.app.Activity;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.model.event.BankEvent;
import com.saferich.invest.model.event.BankSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class BindBankPresenter extends BasePresenter<IbindBankView> {


    @Inject
    Activity activity;

    @Inject
    public BindBankPresenter() {
    }


    public void code() {


        apiWrapper.sendVCodeForBindCard(context)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((String data) -> {

                    view.successful(data);
                }));
    }

    public void addSub(TextView bankName) {

        subscription.add(rxBus.subscribe(event -> {
                    bankName.setText(event.getName());

                },
                BankEvent.class));
        subscription.add(rxBus.subscribe(event -> {
                    activity.finish();
                },
                BankSuccessEvent.class));


    }

    public void Unsubscribe() {
        subscription.unsubscribe();
    }
}
