package com.saferich.invest.vp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.model.bean.BankItem;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 银行卡列表的适配器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class BankListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<BankItem> bankList;

    public void setListener(BankItemListener listener) {
        this.listener = listener;
    }

    private BankItemListener listener;


    @Inject
    public BankListAdapter() {
        bankList = new ArrayList<>();
    }


    public void SetListData(ArrayList<BankItem> bankList) {

        this.bankList = bankList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(bankList.get(position), listener);


    }

    @Override
    public int getItemCount() {
        return bankList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bank_name)
        TextView bankName;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(BankItem bankItem, BankItemListener listener) {

            bankName.setText(bankItem.getName());
            RxView.clicks(itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        listener.itemClick(bankItem.getName(), bankItem.getBankCode());
                    });
        }
    }

    public interface BankItemListener {
        void itemClick(String name, String code);
    }

}
