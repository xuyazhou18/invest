package com.saferich.invest.vp.main.me.password;

import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.model.event.PayPWSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-08
 */

public class SetPayPassWordPresenter extends BasePresenter<IPasswordView> {

    @Inject
    public SetPayPassWordPresenter() {
    }


    public void setPaypassword(EditText idCard, EditText password) {
        params.put("p_idCard", idCard.getText().toString().trim());
        params.put("p_payPassword", password.getText().toString().trim());

        apiWrapper.setPayPassword(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Boolean issuccfull) -> {

            if (issuccfull) {
                view.succefull();

                userinfo.isSetPayWord = true;
                userinfo.update();

                sendSub();
            }

        }));
    }

    private void sendSub() {
        if (rxBus.hasObservers()) {
            rxBus.send(new PayPWSuccessEvent());
        }
    }
}
