package com.saferich.invest.vp.base;

/**
 * 所有view 接口的基类
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-09
 */
public interface IBaseView {
}
