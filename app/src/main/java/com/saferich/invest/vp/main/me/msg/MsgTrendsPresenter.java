package com.saferich.invest.vp.main.me.msg;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MsgTrendsPresenter extends BasePresenter<IMsgTrendsView> {

    @Inject
    public MsgTrendsPresenter() {
        super();
    }
}
