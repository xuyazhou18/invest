package com.saferich.invest.vp.main.me.bank;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.model.bean.Bank;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 查看绑定银行卡列表
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class WatchBankActivity extends BaseActivity<WathchPresenter> implements IWatchView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.bank_logo)
    ImageView bankLogo;
    @BindView(R.id.bank_name)
    TextView bankName;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.card_number)
    TextView cardNumber;
    @BindView(R.id.btn_change)
    Button btnChange;
    @BindView(R.id.bank_item)
    RelativeLayout bankItem;
    private Bank bank;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_watch_bank;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getIntent().getBooleanExtra("ispay", false)) {
            centerTitle.setText("查看银行卡");
        } else {
            centerTitle.setText("修改银行卡");
            btnChange.setVisibility(View.VISIBLE);
        }

        presenter.getdata();

    }

    @OnClick(R.id.btn_change)
    public void changeClick() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("ischange", true);
        bundle.putString("name", bank.getCardUserName());
        bundle.putString("idCard", bank.getIdCard());
        ActivityUtil.moveToActivity(this, BindBankActivity.class, bundle);
        this.finish();

    }


    @Override
    public void showData(Bank data, Map<String, List<Object>> banklist) {
        bank = data;
        bankName.setText(BankUtils.bankName.get(data.getBankCode()));
        cardNumber.setText(data.getCardNumber());
        name.setText(data.getCardUserName());
        bankItem.setBackgroundColor(BankUtils.bankColor.get(data.getBankCode()));
        bankLogo.setImageResource(BankUtils.bankLogo.get(data.getBankCode()));
    }
}
