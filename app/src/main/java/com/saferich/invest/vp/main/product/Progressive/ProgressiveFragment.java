package com.saferich.invest.vp.main.product.Progressive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseFragment;

import butterknife.BindView;

/**
 * 产品 进取界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public class ProgressiveFragment extends BaseFragment<ProgressivePresenter> implements IProgressiveView {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_progress, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.setWebView(webView, "http://app.saferich.com/licai", multiStateView);

    }


}
