package com.saferich.invest.vp.main.discover.more.aboutCompany;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 了解公司界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class AboutCompanyActivity extends BaseActivity<AboutCompanyPresenter> {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.des_layout)
    RelativeLayout desLayout;
    @BindView(R.id.aptitude_layout)
    RelativeLayout aptitudeLayout;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.join_layout)
    RelativeLayout joinLayout;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_about_company;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("了解公司");
    }

    @OnClick(R.id.des_layout)
    public void desClick() {
        presenter.getdesData();
    }

    @OnClick(R.id.aptitude_layout)
    public void aptitudeClick() {
        presenter.aptidude();
    }

    @OnClick(R.id.join_layout)
    public void joinClick() {
        presenter.joinUs();
    }

}
