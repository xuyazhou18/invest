package com.saferich.invest.vp.main.discover.more;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public interface IMoreView extends IBaseView {
}
