package com.saferich.invest.vp.main.me.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.common.WatchPdfActivity;
import com.saferich.invest.vp.main.me.register.RegisterActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 登陆注册界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class InputPhoneNumberActivity extends BaseActivity<InputPhonePresenter> implements IInputPhoneView {

    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.clear)
    ImageView clear;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.tips)
    TextView tips;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.check_box)
    CheckBox checkBox;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_input_phone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("登录/注册");

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tips.setVisibility(View.GONE);
            }
        });

        nextClick();
    }

    public void nextClick() {

        RxView.clicks(btnNext)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(editText,
                        R.string.phone_number_isnot_empty))
                .filter(aVoid -> {
                    if (editText.length() != 11) {
                        tips.setVisibility(View.VISIBLE);
                        return false;
                    } else {
                        return true;
                    }

                })
                .filter(aVoid1 -> StringUtil.checkIsCheck(checkBox,
                        R.string.eula_is_not))
                .subscribe(aVoid -> {
                    presenter.next(editText);

                });
    }

    @OnClick(R.id.clear)
    public void clearClick() {
        editText.setText("");
    }

    @OnClick(R.id.service_pdf)
    public void serviceClick() {
        Intent intent = new Intent(this, WatchPdfActivity.class);
        intent.putExtra("url", "services.pdf");
        intent.putExtra("title", "JIA理财平台服务协议");
        startActivity(intent);
    }

    @Override
    public void moveActivity(boolean isResigiter) {
        Intent intent;
        if (isResigiter) {
            intent = new Intent(this, LoginActivity.class);

        } else {
            intent = new Intent(this, RegisterActivity.class);
        }
        intent.putExtra("number", editText.getText().toString().trim());
        startActivity(intent);
        finish();
    }
}
