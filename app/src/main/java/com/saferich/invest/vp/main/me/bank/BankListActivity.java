package com.saferich.invest.vp.main.me.bank;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.BankListAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.BankItem;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

/**
 * 所有的可绑定银行卡列表
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public class BankListActivity extends BaseActivity<BankListPresenter> implements IBankListView, BankListAdapter.BankItemListener {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Inject
    BankListAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.common_refresh;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        centerTitle.setText("银行列表");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setListener(this);


        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });

        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
    }


    @Override
    public void showData(List<BankItem> bankList) {
        ptrFrame.refreshComplete();
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        adapter.SetListData((ArrayList<BankItem>) bankList);

    }

    @Override
    public void itemClick(String name, String code) {
        presenter.sendbSub(name, code);
        this.finish();
    }
}
