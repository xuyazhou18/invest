package com.saferich.invest.vp.main.product.detatils.invest;

import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public interface IInvestView extends IBaseView {
    void moveActivity(OrderInfo data);
    void showData(RechargeBean recharge);
}
