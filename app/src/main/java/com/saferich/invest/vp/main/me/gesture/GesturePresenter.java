package com.saferich.invest.vp.main.me.gesture;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-01
 */

public class GesturePresenter extends BasePresenter<IGestureView> {

    @Inject
    public GesturePresenter() {
        super();
    }

    public void savePassword(String password) {
        userinfo.gesturePassword = password;
        userinfo.update();
    }
}
