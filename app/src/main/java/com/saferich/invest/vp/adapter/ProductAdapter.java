package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.ProductInfo;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 产品相关文件列表适配器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<ProductInfo.ProtocalList> txtList;
    private txtClickLister lister;

    public void setLister(txtClickLister lister) {
        this.lister = lister;
    }

    public void setDataList(ArrayList<ProductInfo.ProtocalList> txtList) {

        this.txtList = txtList;
        notifyDataSetChanged();

    }

    @Inject
    public ProductAdapter() {
        txtList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(txtList.get(position), lister);


    }


    @Override
    public int getItemCount() {

        return txtList.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.text)
        TextView text;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(ProductInfo.ProtocalList protocal, txtClickLister lister) {
            time.setText(TimeUtils.getFormatTime(protocal.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
            text.setText(Html.fromHtml("<u>" + protocal.getName() + "</u>"));
            RxView.clicks(text).subscribe(aVoid -> {
                lister.watchPdf(protocal);
            });


        }
    }

    public interface txtClickLister {
        void watchPdf(ProductInfo.ProtocalList protocal);
    }


}
