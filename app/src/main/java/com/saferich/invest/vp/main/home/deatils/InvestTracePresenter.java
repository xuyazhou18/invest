package com.saferich.invest.vp.main.home.deatils;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestTracePresenter extends BasePresenter<IInvestDeatilsView>{

    @Inject
    public InvestTracePresenter() {
    }
}
