package com.saferich.invest.vp.main.me.result;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public interface IResultView extends IBaseView{
    void showData(ArrayList<AdCount> list);
}
