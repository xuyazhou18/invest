package com.saferich.invest.vp.main.home;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public interface IInvestView extends IBaseView {

    void showData(ArrayList<RecommondItem> data);
    void showHeaderData(ProfitBean data);
    void showFootData(ArrayList<AdCount> data);
    void error();

}
