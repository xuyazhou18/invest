package com.saferich.invest.vp.main.me.password;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 找回或者设置交易密码
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-08
 */

public class SetPayPassWordActivity extends BaseActivity<SetPayPassWordPresenter> implements IPasswordView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.id_card)
    EditText idCard;
    @BindView(R.id.clear_code)
    ImageView delIcon;
    @BindView(R.id.id_card_tips)
    TextView idCardTips;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.del_icon2)
    ImageView delIcon2;
    @BindView(R.id.password_tips)
    TextView passwordTips;
    @BindView(R.id.del_icon3)
    ImageView delIcon3;
    @BindView(R.id.ensure_pw)
    EditText ensurePassword;
    @BindView(R.id.ensure_tips)
    TextView ensureTips;
    @BindView(R.id.btn_invest)
    Button btnSet;
    @BindView(R.id.toast_layout)
    LinearLayout toastLayout;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.pay_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getBooleanExtra("findPay", false)) {
            centerTitle.setText("找回交易密码");
            btnSet.setText("找回交易密码");
        } else {
            centerTitle.setText("设置交易密码");
        }


        addTextWatch();

        passwordClick();
    }


    public void passwordClick() {
        RxView.clicks(btnSet)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> {
                    if (StringUtil.checkIsNotEmpty(password)) {
                        return true;
                    } else {

                        passwordTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })

                .filter(aVoid -> StringUtil.checkLenght(password, 6,
                        R.string.string_length2))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(ensurePassword,
                        R.string.empty_new_password))
                .filter(aVoid -> StringUtil.checkLenght(ensurePassword, 6,
                        R.string.string_length2))
                .filter(aVoid -> {
                    if (StringUtil.checkIsEqual(password, ensurePassword)) {
                        return true;
                    } else {

                        ensureTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .subscribe(aVoid ->
                        presenter.setPaypassword(idCard, password));
    }


    private void addTextWatch() {

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordTips.setVisibility(View.GONE);
                ensureTips.setVisibility(View.GONE);
            }
        });
        ensurePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ensureTips.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void succefull() {
        ShowToast.Short(this, "交易密码设置成功");
        toastLayout.setVisibility(View.VISIBLE);
        this.finish();
    }

    @OnClick(R.id.clear_code)
    public void codeClearClick() {
        idCard.setText("");
    }

    @OnClick(R.id.del_icon2)
    public void passwordClearClick() {
        password.setText("");
    }

    @OnClick(R.id.del_icon3)
    public void ensureClearClick() {
        ensurePassword.setText("");
    }
}
