package com.saferich.invest.vp.main.discover.more.feedback;

import android.app.Activity;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class FeedbackPresenter extends BasePresenter<IBaseView> {

    @Inject
    Activity activity;

    @Inject
    public FeedbackPresenter() {
    }

    public void getData(String txt) {

        params.put("p_text", txt);

        apiWrapper.addFeedBack(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Object ob) -> {
            if (ob == null) {
                ShowToast.Short(context, "提交成功");
                activity.finish();
            }
        }));
    }
}
