package com.saferich.invest.vp.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.Window;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.api.ApiWrapper;
import com.saferich.invest.model.api.RetrofitUtil;
import com.saferich.invest.model.api.RetrofitUtil.APIException;
import com.saferich.invest.model.api.RetrofitUtil.TokenException;
import com.saferich.invest.model.event.RxBus;
import com.saferich.invest.vp.main.MainActivity;
import com.saferich.invest.vp.main.me.login.InputPhoneNumberActivity;
import com.saferich.invest.vp.main.product.dialog.RetryEnsurePayPwDialog;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscriber;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;


/**
 * 控制器的基类
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-09
 */
public class BasePresenter<V extends IBaseView> {

    protected V view;
    @Inject
    protected ApiWrapper apiWrapper;//网络请求封装类
    @Inject
    protected Context context;
    protected ProgressDialog spinner;
    @Inject
    protected RxBus rxBus;//事件总线
    @Inject
    protected CompositeSubscription subscription;
    @Inject
    protected UserConfig config;//用户配置
    @Inject
    protected Userinfo userinfo;//用户信息

    protected Map<String, Object> params;//网络请求的params


    public BasePresenter() {

        this.params = new HashMap<>();


    }


    public void setView(V view) {
        this.view = view;
    }

    public void onDestroy() {
        this.view = null;
    }


    /**
     * 创建网络回调数据的观察者
     *
     * @param onNext
     * @param <T>
     * @return
     */
    protected <T> Subscriber newSubscriber(final Action1<? super T> onNext) {
        return new Subscriber<T>() {


            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

//                if (e instanceof TokenException) {
//                    TokenExpired();
//                    ShowToast.Long(context, e.getMessage());
//                }

                try {
                    if (e instanceof RetrofitUtil.APIException) {
                        APIException exception = (APIException) e;
                        ShowToast.Short(context, exception.message);
                    } else if (e instanceof SocketTimeoutException) {
                        ShowToast.Short(context, "网络异常,请稍后再试");
                        internetError();
                    } else if (e instanceof ConnectException || e instanceof UnknownHostException) {
                        ShowToast.Short(context, "网络异常,请稍后再试");
                        internetError();
                    } else if (e instanceof TokenException) {//token异常
                        TokenException exception = (TokenException) e;
                        ShowToast.Short(context, exception.getMessage());
                        TokenExpired();
                    } else if (e instanceof RetrofitUtil.TokenNullException) {//当前用户没有权限

                        ShowToast.Short(context, "账号已在另外一台设备上登陆");
                        TokenExpired();
                    } else if (e instanceof RetrofitUtil.BindCardFail) {//绑卡失败
                        BindCardFail();
                    } else if (e instanceof RetrofitUtil.PayWordFail) {//支付密码失败
                        payWordFail();
                    } else {
                        ShowToast.Long(context, e.getMessage());
                    }

                } catch (Exception ignored) {

                }

            }


            @Override
            public void onNext(T t) {
            //正常的数据回调
                onNext.call(t);

            }
        };
    }

    protected void internetError() {
    }

    public void payWordFail() {
        Intent intent = new Intent(context, RetryEnsurePayPwDialog.class);
        intent.putExtra("count", ((Activity) context).getIntent().getStringExtra("count"));
        intent.putExtra("id", ((Activity) context).getIntent().getStringExtra("id"));
        context.startActivity(intent);
        ((Activity) context).finish();
    }


    protected void BindCardFail() {

    }

    private void TokenExpired() {
        config.isLogin = false;
        config.update();
        userinfo = new Userinfo();
        userinfo.update();
        context.startActivity(new Intent(context, InputPhoneNumberActivity.class));
        if (context instanceof MainActivity) {

        } else {
            ((Activity) context).finish();
        }
    }


    public void showDialogView(int msg, boolean cancelable) {
        spinner = new ProgressDialog(context);

        spinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        spinner.setMessage(context.getString(msg));
        spinner.show();
        spinner.setCancelable(cancelable);
    }

    public void dismissDialog() {
        if (spinner != null) {
            spinner.dismiss();
        }
    }
}
