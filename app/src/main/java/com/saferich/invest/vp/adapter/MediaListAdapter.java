package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.main.discover.more.mediaList.deatils.MeadiaDeatilsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 *
 * 媒体新闻列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class MediaListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Article> articleList;


    public void setDataList(ArrayList<Article> articleList) {

        this.articleList = articleList;
        notifyDataSetChanged();

    }

    @Inject
    public MediaListAdapter() {
        articleList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(articleList.get(position));


    }


    @Override
    public int getItemCount() {

        return articleList.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.from)
        TextView from;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(final Article item) {
            title.setText(item.getTitle());
            from.setText(item.getAuthor());
            content.setText(item.getRemark());
            time.setText(TimeUtils.getFormatTime(item.getPublishTime(), "yyyy/MM/dd HH:mm:ss"));

            Glide.with(image.getContext()).load(item.getPic())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(DensityUtil.px2dp(image.getContext(), 212), DensityUtil.px2dp(image.getContext(), 261))
                    .into(image);

            RxView.clicks(itemLayout).subscribe(aVoid -> {
                Intent intent = new Intent(itemLayout.getContext(), MeadiaDeatilsActivity.class);
                intent.putExtra("id", item.getId());

                itemLayout.getContext().startActivity(intent);
            });


        }
    }


}
