package com.saferich.invest.vp.main.product.detatils.invest.password;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.widget.PasswordInputView;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.product.detatils.invest.InvestSuccefulActivity;
import com.saferich.invest.vp.main.product.dialog.EnsurePayPwDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 支付弹窗界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class DealDialogActivity extends BaseActivity<DealPresenter> implements IdealView, PasswordInputView.intputFinishListener {


    @BindView(R.id.cancel)
    ImageView cancel;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.title_money_count)
    TextView titleMoneyCount;
    @BindView(R.id.password)
    PasswordInputView password;
    @BindView(R.id.pay_tips)
    TextView payTips;
    @BindView(R.id.root_layout)
    RelativeLayout rootLayout;
    private String fristPassWord;
    private int times;

    @Inject
    Userinfo userinfo;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_ensure_password;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setFinishOnTouchOutside(true);
        password.setListener(this);
        if (!userinfo.isSetPayWord) {


            if (getIntent().getBooleanExtra("isRetry", false)) {
                fristPassWord = getIntent().getStringExtra("password");
                title.setText("确定交易密码");
                payTips.setVisibility(View.GONE);
                times++;

            } else {
                title.setText("设置交易密码");
                payTips.setVisibility(View.VISIBLE);
            }


        }

        titleMoneyCount.setText("￥" + getIntent().getStringExtra("count"));


    }

    @OnClick(R.id.cancel)
    public void cancelClick() {

        finish();
    }


    @Override
    public void authFinish(OrderInfo order) {
        Intent intent = new Intent(this, InvestSuccefulActivity.class);
        intent.putExtra("productName", order.getProductName());
        intent.putExtra("CreateTime", order.getCreateDate());
        intent.putExtra("investMoney", StringUtil.repaceE(order.getInvestMoney()) + "");

        startActivity(intent);

        finish();


    }

    @Override
    public void returnBack(String password) {

        if (userinfo.isSetPayWord) {
            presenter.addorder(password);

        } else {
            this.password.setText("");

            if (times == 0) {
                fristPassWord = password;
                title.setText("确定交易密码");
                payTips.setVisibility(View.GONE);
            } else if (times == 1) {

                if (StringUtil.equals(fristPassWord, password)) {
                    presenter.setPayPassWord(password);
                    ShowToast.Short(DealDialogActivity.this, "交易密码设置成功");
                    startActivity(new Intent(this, InvestSuccefulActivity.class));
                    this.finish();
                } else {
                    Intent intent = new Intent(this, EnsurePayPwDialog.class);
                    intent.putExtra("firstPassword", fristPassWord);
                    startActivity(intent);
                    this.finish();
                }
            }
            times++;
        }
    }
}
