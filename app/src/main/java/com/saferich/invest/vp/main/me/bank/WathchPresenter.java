package com.saferich.invest.vp.main.me.bank;

import com.saferich.invest.model.bean.Bank;
import com.saferich.invest.model.cache.CacheManager;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class WathchPresenter extends BasePresenter<IWatchView> {

    @Inject
    CacheManager cacheManager;

    @Inject
    public WathchPresenter() {
    }

    public void getdata() {
        apiWrapper.getBindCardDetail(context).subscribe(newSubscriber((Bank data) -> {

            view.showData(data, cacheManager.getBankCache("bankList"));
        }));
    }
}
