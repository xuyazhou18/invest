package com.saferich.invest.vp.main.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.NetworkUtil;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.Account;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.main.me.balance.BalanceActivity;
import com.saferich.invest.vp.main.me.bank.BindBankActivity;
import com.saferich.invest.vp.main.me.getCash.GetCashActivity;
import com.saferich.invest.vp.main.me.msg.MsgTrendsActivity;
import com.saferich.invest.vp.main.me.rechare.RechargeActivity;
import com.saferich.invest.vp.main.me.userinfo.UserinfoActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 用户登陆后的我界面
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class UserFragment extends BaseFragment<UserPresenter> implements IUserView {

    @Inject
    Fragment context;
    @BindView(R.id.money_count)
    TextView moneyCount;
    @BindView(R.id.total_count)
    TextView totalCount;
    @BindView(R.id.bindTips)
    TextView bindTips;
    @BindView(R.id.refresh)
    MaterialRefreshLayout refresh;
    private Account account;
    private int time;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_user, container,
                false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.addSub();

        refresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                presenter.fresh();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {

                refresh.finishRefreshLoadMore();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if (time == 0) {
            presenter.getdata();
            time++;
        } else {
            presenter.refreshData();
        }
    }

    @OnClick(R.id.money_count)
    public void moneyClick() {

        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("count", moneyCount.getText().toString());
        ActivityUtil.moveToActivity((BaseActivity) getActivity(), BalanceActivity.class, bundle);
    }

    @OnClick(R.id.withdraw_layout)
    public void withdrawClick() {

        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        if (account.isIfBindCard()) {

            ActivityUtil.moveToActivity((BaseActivity) getActivity(), GetCashActivity.class);
        } else {
            ShowToast.Short(getActivity(), "未绑定银行卡!");
        }
    }

    @OnClick(R.id.rechare_layout)
    public void rechareClick() {
        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        if (account.isIfBindCard()) {

            ActivityUtil.moveToActivity((BaseActivity) getActivity(), RechargeActivity.class);
        } else {
            ShowToast.Short(getActivity(), "未绑定银行卡!");
        }
    }

    @OnClick(R.id.bindTips)
    public void bindTips() {
        if (!account.isIfBindCard()) {
            ActivityUtil.moveToActivity((BaseActivity) getActivity(), BindBankActivity.class);
        }

    }

    @OnClick(R.id.msg)
    public void msgClick() {
        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        ActivityUtil.moveToActivity((BaseActivity) getActivity(), MsgTrendsActivity.class);
    }

    @OnClick(R.id.userinfo)
    public void UserClick() {
        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        ActivityUtil.moveToActivity((BaseActivity) getActivity(), UserinfoActivity.class);
    }

    @Override
    public void showData(Account account) {
        refresh.finishRefresh();
        this.account = account;
        if (account.getBalance() == null || account.getTotalMoney() == null) {
            return;
        }

        moneyCount.setText(StringUtil.repaceE(account.getBalance()));
        totalCount.setText(StringUtil.repaceE(account.getTotalMoney()) + "元");
        if (account.isIfBindCard()) {
            bindTips.setVisibility(View.GONE);

        } else {
            bindTips.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void error() {
        refresh.finishRefresh();
    }

}
