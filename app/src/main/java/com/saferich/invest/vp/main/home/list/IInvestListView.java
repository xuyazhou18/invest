package com.saferich.invest.vp.main.home.list;

import com.saferich.invest.model.bean.Invest;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public interface IInvestListView extends IBaseView{
    void showData(ArrayList<Invest> data);

}
