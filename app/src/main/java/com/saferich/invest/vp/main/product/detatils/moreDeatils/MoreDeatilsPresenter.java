package com.saferich.invest.vp.main.product.detatils.moreDeatils;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.api.DownloadPdfService;
import com.saferich.invest.model.api.ServiceGenerator;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.common.WebDeatilsActivity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-22
 */

public class MoreDeatilsPresenter extends BasePresenter<IMoreView> {


    @Inject
    public MoreDeatilsPresenter() {
    }


    public void setWebView(WebView webView, String content) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        webView.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(context,
                        WebDeatilsActivity.class);
                intent.putExtra("url", url);
                // intent.putExtra("title", deatails.getTitle());

                context.startActivity(intent);
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
                                                              String url) {
                WebResourceResponse response = null;
                response = super.shouldInterceptRequest(view, url);
                return response;
            }
        });
    }

    public void downloadPdf(String url, String name) {


        DownloadPdfService downloadPdfService = ServiceGenerator.createService(DownloadPdfService.class);

        String savePath = null;
        try {
            //    savePath = Constant.DownLoadPath + URLEncoder.encode(name, "utf-8") + "pdf.pdf";
            savePath = context.getExternalFilesDir(null) + File.separator + URLEncoder.encode(name, "utf-8") + "pdf.pdf";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Call<File> call = downloadPdfService.download(StringUtil.replaceUrl(url), savePath);

        showDialogView(R.string.downing, true);

        call.enqueue(new Callback<File>() {
            @Override
            public void onResponse(Call<File> call, Response<File> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e("onResponse", "file path:" + response.body().getPath());
                    view.showPdf(response.body().getPath(), name);
                } else {
                    ShowToast.Short(context, "pdf文件找不到");
                }
                dismissDialog();


            }

            @Override
            public void onFailure(Call<File> call, Throwable t) {
                dismissDialog();
            }
        });
    }
}
