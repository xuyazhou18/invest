package com.saferich.invest.vp.main.me.password;

import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-08
 */

public class SetPassWordPresenter extends BasePresenter<IPasswordView> {

    @Inject
    public SetPassWordPresenter() {
    }

    public void setpassword(EditText editTextCode, EditText password,String number) {



        params.put("p_verCode", editTextCode.getText().toString().trim());
        params.put("p_password", password.getText().toString().trim());
        params.put("p_phone", number);

        apiWrapper.setPassword(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Boolean issuccfull) -> {

            if (issuccfull) {
                view.succefull();
            }

        }));


    }

    public void verifCode(String number) {
        params.put("p_phone", number);
        apiWrapper.phoneVCode(context, params).subscribe(newSubscriber((Object ob) -> {
            if (ob == null) {
                ShowToast.Short(context, "验证码发送成功");

            }
        }));
    }
}
