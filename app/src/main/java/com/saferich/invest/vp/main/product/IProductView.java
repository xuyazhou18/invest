package com.saferich.invest.vp.main.product;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IProductView extends IBaseView {
    void refreshData();
}
