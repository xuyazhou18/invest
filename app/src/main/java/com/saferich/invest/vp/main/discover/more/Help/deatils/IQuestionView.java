package com.saferich.invest.vp.main.discover.more.Help.deatils;

import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public interface IQuestionView extends IBaseView{
    void showData(Question question);
}
