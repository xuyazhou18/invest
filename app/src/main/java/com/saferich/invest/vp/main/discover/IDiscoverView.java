package com.saferich.invest.vp.main.discover;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IDiscoverView extends IBaseView {
    void showAdData(String[] data, String[] link);
}
