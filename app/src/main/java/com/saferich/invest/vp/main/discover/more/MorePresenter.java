package com.saferich.invest.vp.main.discover.more;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MorePresenter extends BasePresenter<IMoreView> {

    @Inject
    public MorePresenter() {
        super();
    }
}
