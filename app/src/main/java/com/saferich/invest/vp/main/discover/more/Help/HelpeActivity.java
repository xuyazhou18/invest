package com.saferich.invest.vp.main.discover.more.Help;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.QuestionAdapter;
import com.saferich.invest.common.recyclerview.EndlessRecyclerOnScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.discover.more.Help.questList.QuestionListActivity;
import com.saferich.invest.vp.main.product.detatils.phone.PhoneDialogActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * 帮助中心界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class HelpeActivity extends BaseActivity<HelpPresenter> implements IHelperView, QuestionAdapter.HeaderItemListener {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;
    @BindView(R.id.btn_phone)
    Button btnPhone;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @Inject
    QuestionAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_help;
    }

    @Override
    public void headItemClick(int type) {
        Intent intent = new Intent(this, QuestionListActivity.class);
        intent.putExtra("id", type);
        startActivity(intent);
    }

    @OnClick(R.id.btn_phone)
    public void phoneClick() {
        ActivityUtil.moveToActivity(this, PhoneDialogActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI() {

        centerTitle.setText("帮助中心");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(this, 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListener(this);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);

                presenter.loadMore();
            }
        });

        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });

        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
    }

    @Override
    public void showListData(ArrayList<Question> questionlist) {
        if (presenter.isloadMore()) {

            //   refresh.finishRefreshLoadMore();
        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            ptrFrame.refreshComplete();


        }

        adapter.setDataList(questionlist);
    }
}
