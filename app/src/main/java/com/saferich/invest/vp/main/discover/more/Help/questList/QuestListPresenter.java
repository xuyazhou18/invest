package com.saferich.invest.vp.main.discover.more.Help.questList;

import android.app.Activity;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.main.discover.more.Help.IHelperView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class QuestListPresenter extends BasePresenter<IHelperView> {

    @Inject
    public QuestListPresenter() {
        super();
        questionList = new ArrayList<>();
    }

    @Inject
    Activity activity;


    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<Question> questionList;
    private int currentPage = 1;


    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
    }

    private void getData() {
        params.clear();
        params.put("p_id", activity.getIntent().getIntExtra("id", 0) + 1);

        params.put("p_page", currentPage);


        apiWrapper.getQuestionListById(activity, params).subscribe(newSubscriber((List<Question> list) -> {
            if (isloadMore) {
                if (questionList.size() > 0) {
                    questionList.addAll(list);
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                questionList.clear();

                questionList.addAll(list);


            }

            view.showListData((ArrayList<Question>) questionList);
        }));

    }

    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }
}
