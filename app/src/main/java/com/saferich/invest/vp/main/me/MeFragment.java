package com.saferich.invest.vp.main.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseFragment;

/**
 * 我的主界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class MeFragment extends BaseFragment<MePresenter> implements IMeView {

    private Fragment[] fragments;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_me, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragments = new Fragment[2];
        fragmentManager = getChildFragmentManager();
        fragments[0] = fragmentManager.findFragmentById(R.id.fragment_user);
        fragments[1] = fragmentManager.findFragmentById(R.id.fragment_visitor);


    }


    @Override
    public void onResume() {
        super.onResume();

        presenter.showFragment();

    }

    @Override
    public void showFragment(boolean isLogin) {
        if (isLogin) {
            transaction = fragmentManager.beginTransaction().hide(fragments[0])
                    .hide(fragments[1]);
            transaction.show(fragments[0]).commit();
        } else {
            transaction = fragmentManager.beginTransaction().hide(fragments[0])
                    .hide(fragments[1]);
            transaction.show(fragments[1]).commit();
        }
    }
}
