package com.saferich.invest.vp.main.discover.more.Help.deatils;

import android.os.Bundle;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;

/**
 * 问题详情界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class QuestionDeatilsActivity extends BaseActivity<QuestionDeatilsPresenter> implements IQuestionView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.txt)
    TextView txt;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_question_deatils;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("问题详情");

        presenter.getdata();

    }

    @Override
    public void showData(Question question) {
        title.setText(question.getTitle());
        txt.setText(question.getContent());
    }
}
