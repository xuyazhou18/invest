package com.saferich.invest.vp.main.discover;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.NetworkUtil;
import com.saferich.invest.common.widget.SlideShowView;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.main.discover.more.MoreActivity;
import com.saferich.invest.vp.main.discover.noticetrend.NoticeTrendsActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 发现界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class DiscoverFragment extends BaseFragment<DiscoverPresenter> implements IDiscoverView {

    @Inject
    Context content;
    @BindView(R.id.slideshowView)
    SlideShowView slideshowView;


    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_discover, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       presenter.getAdCount();




    }

    @OnClick(R.id.notice_layout)
    public void noticeClick() {
        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        ActivityUtil.moveToActivity((BaseActivity) content, NoticeTrendsActivity.class);
    }

    @OnClick(R.id.more_layout)
    public void moreClick() {
        if (!NetworkUtil.checkNet(getContext())) {
            return;
        }
        ActivityUtil.moveToActivity((BaseActivity) content, MoreActivity.class);
    }


    @Override
    public void showAdData(String[] data, String[] link) {
        slideshowView.setImageUrls(data, link);
    }
}
