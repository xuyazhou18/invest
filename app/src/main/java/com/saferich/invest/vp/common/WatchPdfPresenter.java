package com.saferich.invest.vp.common;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * 查看pdf的控制器
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-01
 */

public class WatchPdfPresenter extends BasePresenter<IBaseView> {
    @Inject
    public WatchPdfPresenter() {
    }
}
