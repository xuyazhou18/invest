package com.saferich.invest.vp.main.me;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.main.me.login.InputPhoneNumberActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 游客界面
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-06
 */

public class VisterFragment extends BaseFragment<VisterPresenter> implements MePresenter.IVisterView {
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.txt_register)
    TextView txtRegister;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_vister, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick(R.id.btn_login)
    public void loginClick() {


        ActivityUtil.moveToActivity((BaseActivity) getActivity(), InputPhoneNumberActivity.class);
    }

    @OnClick(R.id.btn_register)
    public void registerClick() {

        ActivityUtil.moveToActivity((BaseActivity) getActivity(), InputPhoneNumberActivity.class);
    }

    @OnClick(R.id.txt_login)
    public void txtLogin() {
        btnLogin.setVisibility(View.VISIBLE);
        txtLogin.setVisibility(View.GONE);
        btnRegister.setVisibility(View.GONE);
        txtRegister.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_register)
    public void txtRegister() {
        btnLogin.setVisibility(View.GONE);
        txtLogin.setVisibility(View.VISIBLE);
        btnRegister.setVisibility(View.VISIBLE);
        txtRegister.setVisibility(View.GONE);
    }


}
