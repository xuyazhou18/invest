package com.saferich.invest.vp.main.discover.more.update;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-01
 */

public class UpatePresenter extends BasePresenter<IBaseView> {

    @Inject
    public UpatePresenter() {
    }
}
