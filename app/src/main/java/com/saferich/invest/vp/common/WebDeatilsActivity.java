package com.saferich.invest.vp.common;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.IBaseView;

import butterknife.BindView;

/**
 * 本地网页浏览器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-18
 */
public class WebDeatilsActivity extends BaseActivity<WebDeatilsPresenter> implements IBaseView {

    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    private WebChromeClient chromeClient = null;
    private View myView = null;
    private WebChromeClient.CustomViewCallback myCallBack = null;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_web_deatils;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("");
        init();

    }

    private void init() {

        WebSettings webSettings = webView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(false);

        // 设置可以支持缩放
        webSettings.setSupportZoom(true);
        // 设置出现缩放工具
        // webSettings.setBuiltInZoomControls(true);
        //设置可在大视野范围内上下左右拖动，并且可以任意比例缩放
        webSettings.setUseWideViewPort(true);
//        //设置默认加载的可视范围是大视野范围
//        webSettings.setLoadWithOverviewMode(true);

        webView.setHorizontalScrollBarEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.loadUrl(getIntent().getStringExtra("url"));
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDomStorageEnabled(true);

        chromeClient = new MyChromeClient();

        webView.setWebChromeClient(chromeClient);

        webView.setWebViewClient(mywebViewClient);
        webView.setWebChromeClient(myChromeViewClient);
        webView.setOnKeyListener(myOnKeyListener);
    }


    View.OnKeyListener myOnKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) { // 表示按返回键时的操作

                    webView.goBack(); // 后退

                    // webview.goForward();//前进
                    return true; // 已处理
                }
            }
            return false;
        }
    };

    WebChromeClient myChromeViewClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);

            centerTitle.setText(title);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            super.onShowCustomView(view, callback);

        }
    };

    @Override
    public void onBackPressed() {
        if (myView == null) {
            super.onBackPressed();
        } else {
            chromeClient.onHideCustomView();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        webView.saveState(outState);
    }


    WebViewClient mywebViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            if (view != null && view.getTitle() != null) {
                centerTitle.setText(view.getTitle());
            }
        }

    };

    public class MyChromeClient extends WebChromeClient {

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            if (myView != null) {
                callback.onCustomViewHidden();
                return;
            }
            framelayout.removeView(webView);
            framelayout.addView(view);
            myView = view;
            myCallBack = callback;
        }

        @Override
        public void onHideCustomView() {
            if (myView == null) {
                return;
            }
            framelayout.removeView(myView);
            myView = null;
            framelayout.addView(webView);
            myCallBack.onCustomViewHidden();
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            // TODO Auto-generated method stub
            return super.onConsoleMessage(consoleMessage);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }
}
