package com.saferich.invest.vp.main.me.bank;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.event.BankSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.main.me.login.IInputPhoneView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class BankInputPhonePresenter extends BasePresenter<IInputPhoneView> {

    @Inject
    Activity activity;

    @Inject
    public BankInputPhonePresenter() {
    }


    public void bindCard(EditText editText, Bundle extras) {

        params.put("p_bankCode", extras.getString("bankCode"));
        params.put("p_bankCard", extras.getString("bankcard"));
        params.put("p_verCode", editText.getText().toString().trim());
        params.put("p_mobilePhone", extras.getString("number"));

        if (activity.getIntent().getBooleanExtra("ischange", false)) {
            apiWrapper.changebindCard(context, params)
                    .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                    .doOnTerminate(this::dismissDialog)
                    .subscribe(newSubscriber((Object object) -> {
                        if (object == null) {
                            view.moveActivity(true);
                        }
                    }));
        } else {
            params.put("p_idCard", extras.getString("idcard"));
            params.put("p_realName", extras.getString("realname"));

            apiWrapper.bindCard(context, params)
                    .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                    .doOnTerminate(this::dismissDialog)
                    .subscribe(newSubscriber((Object object) -> {
                        if (object == null) {
                            userinfo.isBindCard = true;
                            String bankCode = extras.getString("bankcard");
                            userinfo.bankCode = extras.getString("bankCode");
                            userinfo.bankCard = bankCode.substring(bankCode.length() - 4, bankCode.length());
                            userinfo.update();
                            view.moveActivity(true);
                        }
                    }));
        }


    }


    public void sendSub() {
        if (rxBus.hasObservers()) {
            rxBus.send(new BankSuccessEvent(userinfo.accessToken));
        }
    }

    public void code() {
        apiWrapper.sendVCodeForBindCard(context)
                .subscribe(newSubscriber((String data) -> {
                    ShowToast.Short(context, "验证码发送成功");
                }));
    }
}
