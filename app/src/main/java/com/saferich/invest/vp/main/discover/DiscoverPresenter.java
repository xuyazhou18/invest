package com.saferich.invest.vp.main.discover;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class DiscoverPresenter extends BasePresenter<IDiscoverView> implements IBaseView {


    @Inject
    public DiscoverPresenter() {
     super();
    }

    public void getAdCount() {
        apiWrapper.getAdContents(context, "12").subscribe(newSubscriber((List<AdCount> list) -> {
            String[] imageList = new String[list.size()];
            String[] linkList = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                imageList[i] = list.get(i).getPic();
                linkList[i] = list.get(i).getForwardUrl();
            }
            view.showAdData(imageList, linkList);
        }));
    }
}
