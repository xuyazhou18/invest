package com.saferich.invest.vp.main.discover.more;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.common.WebDeatilsActivity;
import com.saferich.invest.vp.main.discover.more.Help.HelpeActivity;
import com.saferich.invest.vp.main.discover.more.aboutApp.AboutAppActivity;
import com.saferich.invest.vp.main.discover.more.aboutCompany.AboutCompanyActivity;
import com.saferich.invest.vp.main.discover.more.feedback.FeedbackActivity;
import com.saferich.invest.vp.main.discover.more.mediaList.MediaListActivity;
import com.saferich.invest.vp.main.discover.more.wechat.WeixinActivity;
import com.saferich.invest.vp.main.product.detatils.phone.PhoneDialogActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 发现更多界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MoreActivity extends BaseActivity<MorePresenter> implements IMoreView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.btn_phone)
    Button btnPhone;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_more;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("更多");
    }

    @OnClick(R.id.help_layout)
    public void helpClick() {
        ActivityUtil.moveToActivity(this, HelpeActivity.class);
    }

    @OnClick(R.id.feedback_layout)
    public void feedbackClick() {
        ActivityUtil.moveToActivity(this, FeedbackActivity.class);
    }

    @OnClick(R.id.about_app_layout)
    public void about_appClick() {
        ActivityUtil.moveToActivity(this, AboutAppActivity.class);
    }

    @OnClick(R.id.about_layout)
    public void aboutClick() {
        ActivityUtil.moveToActivity(this, AboutCompanyActivity.class);
    }

    @OnClick(R.id.media_layout)
    public void mediaClick() {
        ActivityUtil.moveToActivity(this, MediaListActivity.class);
    }

    @OnClick(R.id.wechat_layout)
    public void webChatClick() {
        ActivityUtil.moveToActivity(this, WeixinActivity.class);
    }

    @OnClick(R.id.web_layout)
    public void webClick() {
        Intent intent = new Intent(this, WebDeatilsActivity.class);
        intent.putExtra("url", "http://www.saferich.com");
        startActivity(intent);
    }

    @OnClick(R.id.btn_phone)
    public void phoneClick() {
        ActivityUtil.moveToActivity(this, PhoneDialogActivity.class);
    }

}
