package com.saferich.invest.vp.main.product.detatils.invest;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class NoMoneyPresenter extends BasePresenter<IBaseView> {

    @Inject
    public NoMoneyPresenter() {
        super();
    }
}
