package com.saferich.invest.vp.main.me.gesture;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.login.LoginActivity;
import com.takwolf.android.lock9.Lock9View;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 手势密码界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-01
 */

public class GestureActivity extends BaseActivity<GesturePresenter> implements IGestureView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.lock_9_view)
    Lock9View lock9View;
    @BindView(R.id.skip)
    TextView skip;
    @BindView(R.id.des)
    TextView des;
    private String fristPassword;
    private String oldPassword;
    private int times;
    private boolean isChange;
    @Inject
    Userinfo userinfo;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_set_gesture_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("isChange", false)) {
            isChange = true;
        }
        if (isChange) {
            centerTitle.setText("修改手势密码");
            times = -1;
            skip.setText("验证登录密码");
            des.setText("请输入原手势密码");
        } else {
            centerTitle.setText("设置手势密码");
        }

        lock9View.setCallBack(password -> {

            if (times == -1) {
                if (StringUtil.equals(userinfo.gesturePassword, password)) {
                    des.setText("请绘制手势密码");
                    skip.setText("");
                } else {
                    times = -1;
                    ShowToast.Short(GestureActivity.this, "原手势密码错误");
                    return;
                }

            }

            if (times == 0) {
                fristPassword = password;

                des.setText("请再次绘制手势密码");
                skip.setText("重置");
            }

            if (times == 1) {

                if (StringUtil.equals(fristPassword, password)) {
                    ShowToast.Short(GestureActivity.this, "手势密码设置成功");
                    presenter.savePassword(password);
                    finish();
                } else {
                    times = 0;
                    ShowToast.Short(GestureActivity.this, "和上次密码不符,请重新设置");
                }

            }
            times++;

        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @OnClick(R.id.skip)
    public void skipClick() {

        if (isChange) {

            if (times == -1) {

                Bundle bundle = new Bundle();
                bundle.putString("number", userinfo.mobliePhone);
                bundle.putBoolean("ispaygesture", true);
                ActivityUtil.moveToActivityForResult(this, LoginActivity.class, 8, bundle);

            }

            if (times > 0) {
                times = 0;
                des.setText("请绘制手势密码");
                skip.setText("");
            }
        } else {
            if (times > 0) {
                times = 0;
                des.setText("请再次绘制手势密码");
                skip.setText("跳过");
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 8 && resultCode == RESULT_OK) {
            times = 0;
            des.setText("请绘制手势密码");
            skip.setText("");
        }
    }
}
