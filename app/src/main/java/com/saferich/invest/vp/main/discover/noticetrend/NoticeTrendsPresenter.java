package com.saferich.invest.vp.main.discover.noticetrend;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class NoticeTrendsPresenter extends BasePresenter<INoticeTrendsView> {

    @Inject
    public NoticeTrendsPresenter() {
        super();
    }
}
