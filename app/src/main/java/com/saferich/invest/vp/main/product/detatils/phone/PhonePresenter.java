package com.saferich.invest.vp.main.product.detatils.phone;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class PhonePresenter extends BasePresenter<IBaseView> {

    @Inject
    public PhonePresenter() {
        super();
    }
}
