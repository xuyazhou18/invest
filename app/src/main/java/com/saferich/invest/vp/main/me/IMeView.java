package com.saferich.invest.vp.main.me;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IMeView extends IBaseView {

    void showFragment(boolean isLogin);
}
