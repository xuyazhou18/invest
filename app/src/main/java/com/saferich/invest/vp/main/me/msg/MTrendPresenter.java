package com.saferich.invest.vp.main.me.msg;

import android.support.v4.app.Fragment;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MTrendPresenter extends BasePresenter<ITrendView> {

    int page;
    @Inject
    Fragment fragment;

    private boolean isloadMore;
    ArrayList<AdCount> productList;

    @Inject
    public MTrendPresenter() {
        super();
        productList = new ArrayList<>();
    }

    public void refresh() {
        page = 1;
        isloadMore = false;
        getdata();
    }

    private void getdata() {

        apiWrapper.getAdContents(context, "11").subscribe(newSubscriber((List<AdCount> list) -> {

            view.showData((ArrayList<AdCount>) list);
        }));
    }

    public void loadMore() {
        isloadMore = true;
        page++;
        getdata();
    }

    public boolean isloadMore() {
        return isloadMore;
    }
}
