package com.saferich.invest.vp.main.home;

import android.support.v4.app.Fragment;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestVistorPresenter extends BasePresenter<IInvestVistorView> {

    @Inject
    public InvestVistorPresenter() {
    }

    @Inject
    Fragment fragment;

    public void getdata() {

        params.put("p_PageIndex", "1");
        params.put("p_MaxRows", 1);

        apiWrapper.getRecommondList(fragment, params)
                .subscribe(newSubscriber((List<RecommondItem> list) -> {

                    if (list != null && list.get(0) != null) {
                        view.showData((list.get(0)));
                    } else {
                        view.showData(new RecommondItem());
                    }


                }));


    }


    public void geADcount() {
        apiWrapper.getAdContents(context, "1").subscribe(newSubscriber((List<AdCount> list) -> {
            String[] imageList = new String[list.size()];
            String[] linkList = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                imageList[i] = list.get(i).getPic();
                if (list.get(i) != null && list.get(i).getForwardUrl() != null) {
                    linkList[i] = list.get(i).getForwardUrl();
                } else {
                    linkList[i] = "";
                }
            }
            view.showAdData(imageList, linkList);
        }));
    }
}
