package com.saferich.invest.vp.main;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IMainView extends IBaseView {

    int MAIN_TAB_HOME = 0;
    int MAIN_TAB_DISCOVER = 1;
    int MAIN_TAB_PRODUCT = 2;
    int MAIN_TAB_ME = 3;
}
