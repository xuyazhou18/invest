package com.saferich.invest.vp.main.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.NetworkUtil;
import com.saferich.invest.common.Utils.ProductUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.widget.SlideShowView;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.main.me.login.InputPhoneNumberActivity;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 未登录首页fragment界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestVistorFragment extends BaseFragment<InvestVistorPresenter> implements IInvestVistorView {
    @BindView(R.id.slideshowView)
    SlideShowView slideshowView;
    @BindView(R.id.login_layout)
    TextView loginLayout;
    @BindView(R.id.register_layout)
    TextView registerLayout;
    @BindView(R.id.work_layout)
    LinearLayout workLayout;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.center_logo)
    ImageView centerLogo;
    @BindView(R.id.year_title)
    LinearLayout yearTitle;
    @BindView(R.id.rate)
    TextView rate;
    @BindView(R.id.rate_layout)
    RelativeLayout rateLayout;
    @BindView(R.id.invest_date)
    TextView investDate;
    @BindView(R.id.product_deatils)
    TextView productDeatils;
    @BindView(R.id.btn_invest)
    Button btnInvest;
    @BindView(R.id.present)
    TextView present;
    @BindView(R.id.add_text)
    TextView addText;
    @BindView(R.id.floatText)
    TextView floatText;
    @BindView(R.id.empty_layout)
    TextView emptyLayout;
    @BindView(R.id.refresh)
    MaterialRefreshLayout refresh;
    @BindView(R.id.product_layout)
    RelativeLayout productLayout;
    private RecommondItem item;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_invest_vistor, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.getdata();
        presenter.geADcount();


        refresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                presenter.getdata();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {

                refresh.finishRefreshLoadMore();
            }
        });


    }

    @Override
    public void showData(RecommondItem recommondItem) {
        refresh.finishRefresh();
        if (recommondItem.getProductName() != null) {
            emptyLayout.setVisibility(View.GONE);
            productLayout.setVisibility(View.VISIBLE);

            this.item = recommondItem;
            title.setText(recommondItem.getProductName());
            rate.setText(recommondItem.getFixedProfit() + "");
            investDate.setText(recommondItem.getInvestTerm() + "天");

            if (recommondItem.getFixedProfit() != 0 && recommondItem.getFloatProfit() == null) {
                rate.setText(StringUtil.repaceE(recommondItem.getFixedProfit()));
                rate.setVisibility(View.VISIBLE);
                present.setVisibility(View.VISIBLE);
                addText.setVisibility(View.GONE);
                floatText.setVisibility(View.GONE);


            }

            if (recommondItem.getFloatProfit() != null && recommondItem.getFixedProfit() == 0) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                present.setVisibility(View.GONE);
                floatText.setVisibility(View.VISIBLE);
                addText.setVisibility(View.GONE);
                rate.setVisibility(View.GONE);
            }
            if (recommondItem.getFloatProfit() != null && recommondItem.getFixedProfit() != 0) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                rate.setText(StringUtil.repaceE(recommondItem.getFixedProfit()));
                rate.setVisibility(View.VISIBLE);
                present.setVisibility(View.VISIBLE);
                addText.setVisibility(View.VISIBLE);
                floatText.setVisibility(View.VISIBLE);
            }
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            productLayout.setVisibility(View.GONE);

        }
    }

    @Override
    public void showAdData(String[] data, String[] link) {


        slideshowView.setImageUrls(data, link);
    }

    @OnClick(R.id.product_deatils)
    public void deatilsClick() {
        if (NetworkUtil.checkNet(getContext())) {
            Intent intent = new Intent(getContext(),
                    ProductDeatilsActivity.class);
            intent.putExtra("id", item.getId());

            getContext().startActivity(intent);
        }
    }

    @OnClick(R.id.login_layout)
    public void loginClick() {
        if (NetworkUtil.checkNet(getContext())) {

            ActivityUtil.moveToActivity((BaseActivity) getActivity(), InputPhoneNumberActivity.class);
        }
    }

    @OnClick(R.id.register_layout)
    public void registerClick() {
        if (NetworkUtil.checkNet(getContext())) {
            ActivityUtil.moveToActivity((BaseActivity) getActivity(), InputPhoneNumberActivity.class);
        }
    }

    @OnClick(R.id.btn_invest)
    public void investClick() {
        if (NetworkUtil.checkNet(getContext())) {
            ProductUtils.checkLoginAndBind(item.getId(),
                    item.getProductName(), getContext());
        }
    }


}
