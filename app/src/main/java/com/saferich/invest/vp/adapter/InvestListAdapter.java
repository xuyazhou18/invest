package com.saferich.invest.vp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.Invest;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 *
 * 我的投资列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class InvestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Invest> InvestList;


    public void setDataList(ArrayList<Invest> InvestList) {

        this.InvestList = InvestList;
        notifyDataSetChanged();

    }

    @Inject
    public InvestListAdapter() {
        InvestList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invest, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(InvestList.get(position));


    }


    @Override
    public int getItemCount() {

        return InvestList.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.count)
        TextView count;
        @BindView(R.id.money)
        TextView money;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.type_deatils)
        TextView typeDeatils;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据

        public void bindTo(final Invest item) {

            time.setText(TimeUtils.getFormatTime(item.getCreateDate(), "yyyy/MM/dd HH:mm:ss"));
            count.setText(StringUtil.repaceE(item.getMoney()) + "");
            type.setText(item.getType() + "");
            typeDeatils.setText(item.getFundsFrom() + "---->" + item.getFundsTo());

            RxView.clicks(typeDeatils).subscribe(aVoid -> {

            });


        }
    }


}
