package com.saferich.invest.vp.main.me.rechare;

import android.app.Activity;
import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.CardRecharge;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.model.bean.RechargeBody;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public class RecharePresenter extends BasePresenter<IRechargeView> {

    @Inject
    public RecharePresenter() {
    }


    @Inject
    Activity activity;

    public void init() {

        params.put("p_bankCode", userinfo.bankCode);
        params.put("p_channel", "RB");

        apiWrapper.getBankLimitByCodeAndChannel(context, params)
                .subscribe(newSubscriber((RechargeBean data) -> {
                    view.showData(data);
                    getDate();
                }));


    }

    public void getDate() {
        apiWrapper.getExtractCashParameterList(context)
                .subscribe(newSubscriber((List<CashItem> list) -> {
                    view.showLimitData(list);

                }));
    }

    public void getRechare(EditText rechareCount) {

        RechargeBody body = new RechargeBody();

        body.setCert_type("01");
        body.setCurrency("156");
        body.setRechargeMoney(rechareCount.getText().toString().trim());
        body.setTerminal_info("11111");
        body.setTerminal_type("mobile");
        body.setOperateChannel("0");
        body.setUser_ip("192.168.0.12");

        apiWrapper.addRecharge(context, body)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((CardRecharge data) -> {
                    view.moveActivity(data);


                }));

    }
}
