package com.saferich.invest.vp.main.me.getCash;

import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.vp.base.IBaseView;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public interface ICashView extends IBaseView {
    void showData(List<CashItem> list);
}
