package com.saferich.invest.vp.main.me;

import com.saferich.invest.model.bean.Account;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public interface IUserView extends IBaseView {
    void showData(Account account);
    void error();
}
