package com.saferich.invest.vp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.MsgBean;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 用户消息适配器
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class MsgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<MsgBean.list> noticeList;
    private boolean[] showList;
    private boolean isClear;
    private int times;

    public void setMsgListener(MsgListener msgListener) {
        this.msgListener = msgListener;
    }

    private MsgListener msgListener;


    @Inject
    public MsgAdapter() {
        noticeList = new ArrayList<>();
    }


    public void SetListData(ArrayList<MsgBean.list> noticeList) {

        this.noticeList = noticeList;
        showList = new boolean[noticeList.size()];
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notice, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(noticeList.get(position), position, showList, isClear);

        RxView.clicks(itemViewHolder.itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribe(aVoid -> {
                    if (showList[position]) {


                        showList[position] = false;



                    } else {

                        showList[position] = true;

                        noticeList.get(position).setIsRead(true);
                        msgListener.readClick(noticeList.get(position));


                    }

                    notifyItemChanged(position);
                });


    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public void setAllClear(boolean clear) {
        this.isClear = clear;
        notifyDataSetChanged();
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.notice)
        TextView noticeText;
        @BindView(R.id.notice_txt)
        TextView noticeTxt;
        @BindView(R.id.notice_time)
        TextView noticeTime;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;
        @BindView(R.id.notice_fulltxt)
        TextView noticeFulltxt;
        @BindView(R.id.notice_timefull)
        TextView noticeTimefull;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bindTo(MsgBean.list notice, int position, boolean[] showList, boolean isClear) {


            if (showList[position]) {


                if (notice.getContent().length() > 30) {
                    noticeTxt.setVisibility(View.GONE);
                    noticeTime.setVisibility(View.GONE);
                    noticeFulltxt.setVisibility(View.VISIBLE);
                    noticeTimefull.setVisibility(View.VISIBLE);
                } else {
                    noticeTxt.setVisibility(View.VISIBLE);
                    noticeFulltxt.setVisibility(View.GONE);
                    noticeTime.setVisibility(View.VISIBLE);
                    noticeTimefull.setVisibility(View.GONE);
                }


            } else {

                noticeTxt.setVisibility(View.VISIBLE);
                noticeFulltxt.setVisibility(View.GONE);
                noticeTime.setVisibility(View.VISIBLE);
                noticeTimefull.setVisibility(View.GONE);


            }

            if (notice.isIsRead()) {
                noticeText.setBackgroundColor(noticeText.getContext().getResources().getColor(R.color.notic_color));
            } else {
                noticeText.setBackgroundColor(noticeText.getContext().getResources().getColor(R.color.colorPrimary));
            }

            if (isClear) {
                noticeText.setBackgroundColor(noticeText.getContext().getResources().getColor(R.color.notic_color));
            }


            noticeTxt.setText(notice.getContent());
            noticeFulltxt.setText(notice.getContent());

            noticeTime.setText(TimeUtils.getFormatTime(notice.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
            noticeTimefull.setText(TimeUtils.getFormatTime(notice.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));


        }
    }


    public interface MsgListener {
        void readClick(MsgBean.list msg);
    }

}
