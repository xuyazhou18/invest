package com.saferich.invest.vp.main.home.myList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.CommpleListAdapter;
import com.saferich.invest.common.recyclerview.EndlessRecyclerOnScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.vp.base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * 投资已结束fragment
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class EndFragment extends BaseFragment<EndPresenter> implements IEndView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Inject
    CommpleListAdapter adapter;


    @Override
    protected void lazyLoad() {

        if (!isPrepared) {
            return;
        }

        if (!canLoadData(multiStateView, adapter)) {
            return;
        }


        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });

        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_refresh3, container,
                false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);

                presenter.loadMore();
            }
        });


        isPrepared = true;
        lazyLoad();
    }

    @Override
    public void showData(ArrayList<Complete> data) {
        if (presenter.isloadMore()) {

        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            ptrFrame.refreshComplete();

        }

        adapter.setDataList(data);
    }
}
