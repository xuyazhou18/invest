package com.saferich.invest.vp.main.product.detatils;

import android.app.Activity;

import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-10
 */
public class DeatilsPresenter extends BasePresenter<IDetealsView> implements IBaseView {

    @Inject
    Activity activity;

    @Inject
    public DeatilsPresenter() {
        super();
    }

    public void getData() {
        apiWrapper.getProductInfo(context, activity.getIntent().getStringExtra("id"))
                .subscribe(newSubscriber((ProductInfo data) -> {
                    if (data != null) {
                        view.backData(data);
                    }
                }));

    }


}
