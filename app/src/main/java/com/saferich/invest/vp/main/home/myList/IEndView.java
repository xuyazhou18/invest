package com.saferich.invest.vp.main.home.myList;

import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-07
 */

public interface IEndView extends IBaseView {
    void showData(ArrayList<Complete> data);
}
