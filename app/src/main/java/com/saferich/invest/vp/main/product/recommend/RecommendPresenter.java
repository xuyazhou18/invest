package com.saferich.invest.vp.main.product.recommend;

import android.support.v4.app.Fragment;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-03
 */
public class RecommendPresenter extends BasePresenter<IRecommendView> implements IBaseView {

    int page;
    @Inject
    Fragment fragment;

    private boolean isloadMore;
    ArrayList<RecommondItem> recommondList;

    @Inject
    public RecommendPresenter() {
        super();
        recommondList = new ArrayList<>();
    }

    public void refresh() {
        page = 1;
        isloadMore = false;
        getdata();
    }

    private void getdata() {

        params.put("p_PageIndex", page);
        params.put("p_MaxRows", 10);

        apiWrapper.getRecommondList(fragment, params)
                .subscribe(newSubscriber((List<RecommondItem> list) -> {
                    if (isloadMore) {
                        if (list.size() > 0) {
                            recommondList.addAll(list);
                        } else {
                            ShowToast.Short(context, "数据已经全部加载完毕");
                        }
                    } else {
                        recommondList.clear();

                        if (list != null) {
                           recommondList.addAll(list);
                        }else {
                           List<RecommondItem> list1 = new ArrayList<>();

                            recommondList.addAll(list1);
                        }

                    }

                    view.showData(recommondList);
                }));

    }

    public void loadMore() {
        isloadMore = true;
        page++;
        getdata();
    }

    public boolean isloadMore() {
        return isloadMore;
    }

    @Override
    protected void internetError() {
        super.internetError();
        view.error();
    }

    public void getAdCotent() {
        apiWrapper.getAdContents(context, "4").subscribe(newSubscriber((List<AdCount> list) -> {
            String[] imageList = new String[list.size()];
            String[] linkList = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                imageList[i] = list.get(i).getPic();
                linkList[i] = list.get(i).getForwardUrl();
            }
            view.showAdData(imageList, linkList);
        }));
    }
}
