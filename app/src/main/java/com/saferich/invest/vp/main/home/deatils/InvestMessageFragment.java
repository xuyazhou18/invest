package com.saferich.invest.vp.main.home.deatils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.vp.adapter.ProductAdapter;
import com.saferich.invest.vp.adapter.ReportAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.common.WatchPdfActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 投资信息界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestMessageFragment extends BaseFragment<InvestMessagePresenter> implements IMessageView, ProductAdapter.txtClickLister, ReportAdapter.reportClickLister {


    @BindView(R.id.buy_title)
    TextView buyTitle;
    @BindView(R.id.invest_title)
    TextView investTitle;
    @BindView(R.id.profit_time)
    TextView profitTime;
    @BindView(R.id.invest_date)
    TextView investDate;
    @BindView(R.id.invest_date_title)
    TextView investDateTitle;
    @BindView(R.id.buy_time)
    TextView buyTime;
    @BindView(R.id.invest_time)
    TextView investTime;
    @BindView(R.id.invest_count)
    TextView investCount;
    @BindView(R.id.buy_fee)
    TextView buyFee;
    @BindView(R.id.manager_fee)
    TextView managerFee;
    @BindView(R.id.rete_count)
    TextView reteCount;
    @BindView(R.id.get_type)
    TextView getType;
    @BindView(R.id.back_type)
    TextView backType;
    @BindView(R.id.is_reback)
    TextView isReback;
    @BindView(R.id.line_report)
    View lineReport;
    @BindView(R.id.line_report2)
    View lineReport2;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.product_line)
    View productLine;
    @BindView(R.id.product_line2)
    View productLine2;
    @BindView(R.id.recyclerView_product)
    RecyclerView recyclerViewProduct;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.service_layout)
    RelativeLayout serviceLayout;
    @BindView(R.id.report_layout)
    LinearLayout reportLayout;
    @BindView(R.id.product_layout)
    LinearLayout productLayout;
    @BindView(R.id.profit_count)
    TextView profitCount;
    private InvestInfo info;

    @Inject
    ReportAdapter adapter;
    @Inject
    ProductAdapter txtadapter;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_invest_flow, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(adapter);
        adapter.setLister(this);

        LinearLayoutManager txtlinearLayoutManager = new LinearLayoutManager(getContext());
        txtlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewProduct.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerViewProduct.setLayoutManager(txtlinearLayoutManager);

        recyclerViewProduct.setAdapter(txtadapter);
        txtadapter.setLister(this);


    }

    public void setdata(InvestInfo info) {
        this.info = info;
        buyTime.setText(TimeUtils.getFormatTime(info.getCreateTime(), "yyyy/MM/dd"));
        if (info.getBeginprofitTime() == 0) {
            profitTime.setText("待计息");
        } else {
            profitTime.setText(TimeUtils.getFormatTime(info.getBeginprofitTime(), "yyyy/MM/dd"));
        }

        investDate.setText(TimeUtils.getFormatTime(info.getCreateTime(), "yyyy/MM/dd"));
        investTime.setText(info.getInvestTerm() + "天");
        if (info.getEndTime() == 0) {
            investDate.setText("待确定");
        } else {
            investDate.setText(TimeUtils.getFormatTime(info.getEndTime(), "yyyy/MM/dd"));
        }


        investCount.setText(StringUtil.repaceE(info.getInvestMoney()) + "元");
        if (info.getPerYearIncomeRate() != -1) {
            reteCount.setText(StringUtil.repaceE(info.getPerYearIncomeRate()) + "%");
        } else {
            reteCount.setText("待定");
        }

        if (info.getTotalProfit() == -1) {
            profitCount.setText("待定");
        } else {
            profitCount.setText(StringUtil.repaceE(info.getTotalProfit()) + "元");
        }


        buyFee.setText(StringUtil.repaceE(info.getFeeCount()) + "元");
        if (info.getManageFee() == -1) {
            managerFee.setText("待定");
        } else {
            managerFee.setText(StringUtil.repaceE(info.getManageFee()) + "元");
        }
        getType.setText(info.getProfitSettleMannerName());
        backType.setText(info.getCapitalRedeemRuleName());
        if (info.getSupportRedeemStatus() != null && info.getSupportRedeemStatus()) {
            isReback.setText("中途可赎回");
        } else {
            isReback.setText("中途不可赎回");
        }


        if (info.getCustomerReportBeanList() != null && info.getCustomerReportBeanList().size() > 0) {
            adapter.setDataList((ArrayList<InvestInfo.CustomerReportBeanList>) info.getCustomerReportBeanList());
            lineReport.setVisibility(View.VISIBLE);
            reportLayout.setGravity(View.VISIBLE);
            lineReport2.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
            lineReport.setVisibility(View.GONE);
            reportLayout.setGravity(View.GONE);
            lineReport2.setVisibility(View.GONE);
        }
        if (info.getProductProtocalBeanList() != null && info.getProductProtocalBeanList().size() > 0) {
            txtadapter.setDataList((ArrayList<ProductInfo.ProtocalList>) info.getProductProtocalBeanList());
            productLine.setVisibility(View.VISIBLE);
            productLayout.setGravity(View.VISIBLE);
            productLine2.setVisibility(View.VISIBLE);
        } else {
            recyclerViewProduct.setVisibility(View.GONE);
            productLine.setVisibility(View.GONE);
            productLayout.setGravity(View.GONE);
            productLine2.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.service_layout)
    public void serviceClick() {

        Intent intent = new Intent(getContext(), WatchPdfActivity.class);
        intent.putExtra("url", "services.pdf");
        intent.putExtra("title", "JIA理财平台服务协议");
        startActivity(intent);
    }


    @Override
    public void showPdf(String url, String name) {
        Intent intent = new Intent(getContext(), WatchPdfActivity.class);
        intent.putExtra("netUrl", url);
        intent.putExtra("title", name);
        startActivity(intent);
    }


    @Override
    public void watchPdf(ProductInfo.ProtocalList protocal) {
        presenter.downloadPdf(protocal.getProductProtocal(),
                protocal.getForegroundDisplayName());
    }


    @Override
    public void watchPdf(InvestInfo.CustomerReportBeanList protocal) {
        presenter.downloadPdf(protocal.getFilePath(),
                protocal.getFileName());
    }

}
