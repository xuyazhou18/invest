package com.saferich.invest.vp.main.me.rechare;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.CardRecharge;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.phone.InputVerCodeActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;

/**
 *
 * 充值界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public class RechargeActivity extends BaseActivity<RecharePresenter> implements IRechargeView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.bank_iamge)
    ImageView bankIamge;
    @BindView(R.id.card_number)
    TextView cardNumber;
    @BindView(R.id.product_name)
    TextView accoutCount;
    @BindView(R.id.rechare_count)
    EditText rechareCount;
    @BindView(R.id.tips)
    TextView tips;
    @BindView(R.id.rechare_tips)
    TextView rechareTips;
    @BindView(R.id.btn_rechare)
    Button btnRechare;
    @BindView(R.id.rechare_time)
    TextView rechareTime;
    @Inject
    Userinfo userinfo;
    RechargeBean recharge;
    private int minLimitCount;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recharge;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.init();
        centerTitle.setText("充值");
        cardNumber.setText("尾号" + userinfo.bankCard);
        accoutCount.setText("账户余额:" + StringUtil.repaceE(userinfo.balance) + "元");
        bankIamge.setImageResource(BankUtils.bankLogo.get(userinfo.bankCode));
        StringUtil.setPricePoint(rechareCount);
        nextClick();
    }

    @Override
    public void showData(RechargeBean recharge) {
        this.recharge = recharge;
        rechareTips.setText("每日充值限额" + recharge.getMaxMoneyDay() + "元,每笔限额" + recharge.getMaxMoney() + "元");

    }

    @Override
    public void showLimitData(List<CashItem> list) {
        if (list.get(3).getStatus() == 0) {
            rechareCount.setHint("最低充值" + StringUtil.repaceE((double) list.get(3).getValue()) + "元");
            minLimitCount = list.get(3).getValue();
        } else {
            minLimitCount = 0;
            rechareCount.setHint("请输入充值金额");

        }

    }

    @Override
    public void moveActivity(CardRecharge data) {
        Intent intent = new Intent(this, InputVerCodeActivity.class);
        intent.putExtra("id", data.getResult().getRechargeId());
        intent.putExtra("rechargeMoney", data.getResult().getRechargeMoney());
        intent.putExtra("blance", data.getResult().getBalance());
        intent.putExtra("data", data.getMobile());
        startActivity(intent);
        finish();
    }

    private void nextClick() {
        RxView.clicks(btnRechare)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(rechareCount,
                        R.string.recharge_count_isnot_empty))
                .subscribe(aVoid -> {

                    if (minLimitCount != 0 && Double.valueOf(rechareCount.getText().toString()) < recharge.getMinMoney()) {
                        ShowToast.Short(this, "低于最低限额");
                        return;
                    }
                    if (Double.valueOf(rechareCount.getText().toString()) > recharge.getMaxMoney()) {
                        ShowToast.Short(this, "充值超过限额");
                        return;
                    }
                    presenter.getRechare(rechareCount);


                });
    }
}
