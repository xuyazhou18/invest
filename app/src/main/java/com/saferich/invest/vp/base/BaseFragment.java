package com.saferich.invest.vp.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kennyc.view.MultiStateView;
import com.kennyc.view.MultiStateView.ViewState;
import com.saferich.invest.MyApplication;
import com.saferich.invest.model.inject.components.AppComponent;
import com.saferich.invest.model.inject.components.DaggerFragmentComponent;
import com.saferich.invest.model.inject.components.FragmentComponent;
import com.saferich.invest.model.inject.models.FragmentModule;
import com.trello.rxlifecycle.components.support.RxFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;


/**
 * fragment的基类
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-03
 */
public abstract class BaseFragment<P extends BasePresenter> extends RxFragment implements IBaseView {

    @Inject
    protected P presenter;//控制器
    protected boolean isVisible;//当前fragment是否已经显示
    protected boolean isPrepared;//是否准备加载数据


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setupFragmentComponent();//创建每个fragment的dagger component
        presenter.setView(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflateView(inflater, container);//布局的填充
        ButterKnife.bind(this, view);//view的注入绑定
        return view;
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        if (presenter != null) {
            presenter.onDestroy();
        }

    }

    protected FragmentComponent getFragmentComponent() {
        return DaggerFragmentComponent.builder()
                .appComponent(getAppComponent())
                .fragmentModule(getFragmentModule())
                .build();
    }

    protected AppComponent getAppComponent() {
        return ((MyApplication) getActivity().getApplication()).getAppComponent();
    }

    protected FragmentModule getFragmentModule() {
        return new FragmentModule(this);
    }

    /**
     * 在这里实现Fragment数据的缓加载.
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }

    protected boolean canLoadData(@NonNull MultiStateView multiStateView, @NonNull RecyclerView.Adapter adapter) {
        @ViewState int viewState = multiStateView.getViewState();
        return !(viewState == MultiStateView.VIEW_STATE_LOADING ||
                (viewState == MultiStateView.VIEW_STATE_CONTENT && adapter.getItemCount() > 0) ||
                viewState == MultiStateView.VIEW_STATE_EMPTY ||
                viewState == MultiStateView.VIEW_STATE_ERROR);
    }


    protected void onVisible() {
        lazyLoad();
    }

    protected abstract void lazyLoad();

    protected void onInvisible() {
    }

    protected abstract void setupFragmentComponent();

    protected abstract View inflateView(LayoutInflater inflater, ViewGroup container);
}
