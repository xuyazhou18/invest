package com.saferich.invest.vp.wellcome;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-30
 */

public interface IWellcomeView extends IBaseView{
}
