package com.saferich.invest.vp.main.product.detatils.invest;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.common.Utils.ProductUtils;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.common.WatchPdfActivity;
import com.saferich.invest.vp.main.me.password.SetPayPassWordActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 产品投资界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class InvestActivity extends BaseActivity<InvestPresenter> implements IInvestView {


    @Inject
    Userinfo userinfo;
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.accout_count)
    TextView accoutCount;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.clear_code)
    ImageView clearCode;
    @BindView(R.id.invest_count)
    EditText investCount;
    @BindView(R.id.cash_tips)
    TextView cashTips;
    @BindView(R.id.blance_count)
    TextView blanceCount;
    @BindView(R.id.check_money)
    CheckBox checkMoney;
    @BindView(R.id.cash_pay)
    RelativeLayout cashPay;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.check_card)
    CheckBox checkCard;
    @BindView(R.id.bank_number)
    TextView bankNumber;
    @BindView(R.id.card_pay)
    RelativeLayout cardPay;
    @BindView(R.id.card_tips)
    TextView cardTips;
    @BindView(R.id.check_deal)
    CheckBox checkDeal;
    @BindView(R.id.agree_text)
    TextView agreeText;
    @BindView(R.id.and)
    TextView and;
    @BindView(R.id.btn_invest)
    Button btnInvest;
    @BindView(R.id.deal_txt)
    TextView dealTxt;
    @BindView(R.id.bottom_layout)
    RelativeLayout bottomLayout;
    @BindView(R.id.buy_fee)
    TextView buyFee;
    private double singleLimitBuy;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.invest_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        centerTitle.setText("投资");
        presenter.init();
        productName.setText(getIntent().getStringExtra("productName"));

        accoutCount.setText("剩余投资额:" + getIntent().getStringExtra("remainMoney") + "元");
        investCount.setHint("起投金额" + getIntent().getStringExtra("minInvestMoney") + "元,递增金额" +
                getIntent().getStringExtra("purchaseIncreaseMoney") + "元"
        );
        presenter.addSub();

        buyFee.setText("服务费 " + getIntent().getDoubleExtra("subscriptionFee", 0) + "%(" +
                getIntent().getDoubleExtra("subscriptionFee", 0) * 0 + "元)"
        );

        investCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    return;
                }
                buyFee.setText("服务费 " + getIntent().getDoubleExtra("subscriptionFee", 0) + "%(" +
                        StringUtil.repaceE(getIntent().getDoubleExtra("subscriptionFee", 0) * 0.01 * Double.valueOf(s.toString())) + "元)"
                );
            }
        });
    }

    @OnClick(R.id.cash_pay)
    public void cashPay() {
        checkMoney.setChecked(true);
        checkCard.setChecked(false);
        cardTips.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        blanceCount.setText("余额:" + StringUtil.repaceE(userinfo.balance) + "元");
        bankNumber.setText(BankUtils.bankName.get(userinfo.bankCode) + "(" + "尾号" + userinfo.bankCard + ")");
    }

    @OnClick(R.id.btn_invest)
    public void btnInvest() {


        if (!checkDeal.isChecked()) {
            ShowToast.Short(this, "协议未勾选!");
            return;
        }

        if (investCount.getText().toString().equals("")) {
            ShowToast.Short(this, "投资金额不能为空!");
            return;
        }


        if (Double.valueOf(getIntent().getStringExtra("remainMoney")) < Double.valueOf(investCount.getText().toString())) {
            ShowToast.Short(this, "已经超过了可投资额度哦");
            return;
        }
        if (Double.valueOf(getIntent().getStringExtra("minInvestMoney")) > Double.valueOf(investCount.getText().toString())) {
            ShowToast.Short(this, "小于最小投资额哦");
            return;
        }

        if (userinfo.isBindCard) {

            if (checkCard.isChecked()) {
                if (singleLimitBuy < Double.valueOf(investCount.getText().toString())) {
                    ShowToast.Short(this, "超过了单笔限额哦");
                    return;
                }

                presenter.addorder(investCount.getText().toString());
            } else {

                if (Double.valueOf(StringUtil.repaceE(userinfo.getBalance())) < Double.valueOf(investCount.getText().toString())) {
                    ShowToast.Short(this, "余额不足");
                    return;
                }

                if (userinfo.isSetPayWord) {
                    presenter.choicePay(investCount.getText().toString());
                } else {
                    startActivity(new Intent(this, SetPayPassWordActivity.class));
                }
            }
        } else {
            ProductUtils.checkLoginAndBind
                    (getIntent().getStringExtra("id"), getIntent().getStringExtra("productName"), this);
        }

    }

    @OnClick(R.id.clear_code)
    public void clear_code()

    {
        investCount.setText("");
    }

    @OnClick(R.id.card_pay)
    public void cardPay() {
        checkMoney.setChecked(false);
        checkCard.setChecked(true);
        cardTips.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.deal_txt)
    public void serviceClick() {
        Intent intent = new Intent(this, WatchPdfActivity.class);
        intent.putExtra("url", "services.pdf");
        intent.putExtra("title", "JIA理财平台服务协议");
        startActivity(intent);
    }

    @Override
    public void moveActivity(OrderInfo order) {
        Intent intent = new Intent(this, BankPhonePayActivity.class);
        intent.putExtra("orderId", order.getOrderId());

        startActivity(intent);

    }

    @Override
    public void showData(RechargeBean recharge) {
        singleLimitBuy = recharge.getMaxMoney();
        cardTips.setText("单笔限额" + recharge.getMaxMoney() + "元,单日限额" + recharge.getMaxMoneyDay() + "元");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.Unsubscribe();
    }
}
