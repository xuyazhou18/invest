package com.saferich.invest.vp.main.discover.more.aboutCompany;

import android.content.Intent;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;
import com.saferich.invest.vp.common.WebDeatilsTxtActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class AboutCompanyPresenter extends BasePresenter<IBaseView> {
    @Inject
    public AboutCompanyPresenter() {
    }

    public void getdesData() {
        apiWrapper.getdes(context).subscribe(newSubscriber((String data) -> {
            if (data != null) {
                Intent intent = new Intent(context, WebDeatilsTxtActivity.class);
                intent.putExtra("txt", data);
                intent.putExtra("title", "公司简介");
                context.startActivity(intent);
            }
        }));
    }

    public void aptidude() {
        apiWrapper.getQualified(context).subscribe(newSubscriber((String data) -> {
            if (data != null) {
                Intent intent = new Intent(context, WebDeatilsTxtActivity.class);
                intent.putExtra("txt", data);
                intent.putExtra("title", "公司资质");
                context.startActivity(intent);
            }
        }));
    }

    public void joinUs() {
        apiWrapper.getJoinUs(context).subscribe(newSubscriber((String data) -> {
            if (data != null) {
                Intent intent = new Intent(context, WebDeatilsTxtActivity.class);
                intent.putExtra("txt", data);
                intent.putExtra("title", "加入我们");
                context.startActivity(intent);
            }
        }));
    }
}
