package com.saferich.invest.vp.main.me.userinfo;

import com.saferich.invest.model.bean.UserMessage;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public interface IUserInfoView extends IBaseView {
    void showData(UserMessage message);
}
