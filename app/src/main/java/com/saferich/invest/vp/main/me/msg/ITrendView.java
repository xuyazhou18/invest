package com.saferich.invest.vp.main.me.msg;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public interface ITrendView extends IBaseView{
    void showData(ArrayList<AdCount> list);
}
