package com.saferich.invest.vp.main.me.result;

import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public class BalanceResultPresenter extends BasePresenter<IResultView> {

    @Inject
    public BalanceResultPresenter() {
    }

    public void getData(boolean isCash) {
        String id ;
        if(isCash){
            id = "7";
        }else {
            id = "6";
        }

        apiWrapper.getAdContents(context, id).subscribe(newSubscriber((List<AdCount> list) -> {

            view.showData((ArrayList<AdCount>) list);
        }));

    }
}
