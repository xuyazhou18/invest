package com.saferich.invest.vp.main.home.deatils;

import android.util.Log;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.api.DownloadPdfService;
import com.saferich.invest.model.api.ServiceGenerator;
import com.saferich.invest.vp.base.BasePresenter;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestMessagePresenter extends BasePresenter<IMessageView> {

    @Inject
    public InvestMessagePresenter() {
    }

    public void downloadPdf(String url, String name) {


        DownloadPdfService downloadPdfService = ServiceGenerator.createService(DownloadPdfService.class);

        String savePath = null;
        try {
            //    savePath = Constant.DownLoadPath + URLEncoder.encode(name, "utf-8") + "pdf.pdf";
            savePath = context.getExternalFilesDir(null) + File.separator + URLEncoder.encode(name, "utf-8") + "pdf.pdf";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Call<File> call = downloadPdfService.download(url, savePath);

        showDialogView(R.string.downing, true);

        call.enqueue(new Callback<File>() {
            @Override
            public void onResponse(Call<File> call, Response<File> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e("onResponse", "file path:" + response.body().getPath());
                    view.showPdf(response.body().getPath(), name);
                } else {
                    ShowToast.Short(context, "pdf文件地址找不到");
                }
                dismissDialog();


            }

            @Override
            public void onFailure(Call<File> call, Throwable t) {
                dismissDialog();
            }
        });
    }
}
