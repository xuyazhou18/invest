package com.saferich.invest.vp.main.discover.noticetrend.notice;

import android.support.v4.app.Fragment;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Notice;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class NoticePresenter extends BasePresenter<INoticeView> {


    @Inject
    Fragment fragment;

    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<Notice> noticeList;
    private int currentPage = 1;

    @Inject
    public NoticePresenter() {
        super();
        noticeList = new ArrayList<>();
    }

    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
    }

    private void getData() {
        params.clear();


        params.put("p_page", currentPage);

        apiWrapper.getNotice(fragment, params).subscribe(newSubscriber((List<Notice> list) -> {
            if (isloadMore) {
                if (noticeList.size() > 0) {
                    noticeList.addAll(list);
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                noticeList.clear();

                noticeList.addAll(list);


            }
            view.showListData((ArrayList<Notice>) noticeList);

        }));

    }

    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }
}
