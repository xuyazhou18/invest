package com.saferich.invest.vp.main.product.detatils.element;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-22
 */

public interface IElementView extends IBaseView {
}
