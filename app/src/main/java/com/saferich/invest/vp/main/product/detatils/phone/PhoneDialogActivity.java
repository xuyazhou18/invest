package com.saferich.invest.vp.main.product.detatils.phone;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.base.IBaseView;

import butterknife.OnClick;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class PhoneDialogActivity extends BaseActivity<PhonePresenter> implements IBaseView {

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_phone;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(true);
    }


    @OnClick(R.id.btn_phone)
    public void phoneClick() {

        long numbber = 4008031818L;

        Intent intent = new Intent(Intent.ACTION_DIAL,
                Uri.parse("tel:" + numbber));
        startActivity(intent);


    }

    @OnClick(R.id.cancel)
    public void cancelClick() {
        finish();
    }


}
