package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.main.discover.more.Help.deatils.QuestionDeatilsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 帮助问题列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class QuestionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Question> questionlist;


    public void setDataList(ArrayList<Question> questionlist) {

        this.questionlist = questionlist;
        notifyDataSetChanged();

    }

    @Inject
    public QuestionListAdapter() {
        questionlist = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(questionlist.get(position));


    }


    @Override
    public int getItemCount() {

        return questionlist.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(final Question item) {
            title.setText(item.getTitle());

            RxView.clicks(itemLayout).subscribe(aVoid -> {
                Intent intent = new Intent(itemLayout.getContext(), QuestionDeatilsActivity.class);
                intent.putExtra("id", item.getId());

                itemLayout.getContext().startActivity(intent);
            });


        }
    }


}
