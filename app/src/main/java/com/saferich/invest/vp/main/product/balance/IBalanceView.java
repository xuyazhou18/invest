package com.saferich.invest.vp.main.product.balance;

import com.saferich.invest.model.bean.Product;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-03
 */
public interface IBalanceView extends IBaseView {
    void showData(ArrayList<Product> list);
    void error();
}
