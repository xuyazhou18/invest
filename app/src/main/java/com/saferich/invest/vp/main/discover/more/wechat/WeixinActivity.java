package com.saferich.invest.vp.main.discover.more.wechat;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.copyUtils;
import com.saferich.invest.vp.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 微信公众号关注界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-01
 */

public class WeixinActivity extends BaseActivity<WechatPresenter> {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.btn_wechat)
    Button btnWechat;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_wechat;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("微信");
    }

    @OnClick(R.id.btn_wechat)
    public void wechatClick() {

        copyUtils.copy("嘉丰瑞德", this);
        ShowToast.Short(this, "公众号名称已经复制哦,请去微信关注吧");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        ComponentName cn = new ComponentName("com.tencent.mm", "com.tencent.mm.plugin.base.stub.WXCustomSchemeEntryActivity");
        intent.setData(Uri.parse("weixin://dl/officialaccounts"));
        intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        intent.setComponent(cn);
        startActivity(intent);
        finish();
    }
}
