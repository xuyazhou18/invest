package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.InvestInfo.InvestTrackBeanList;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 投资跟踪列表适配器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class FlowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<InvestTrackBeanList> txtList;


    public void setDataList(ArrayList<InvestTrackBeanList> txtList) {

        this.txtList = txtList;
        notifyDataSetChanged();

    }

    @Inject
    public FlowAdapter() {
        txtList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flow, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(txtList.get(position), position, txtList.size());


    }


    @Override
    public int getItemCount() {

        return txtList.size();


    }


    static class

    ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.status_icon)
        ImageView statusIcon;
        @BindView(R.id.create_time)
        TextView createTime;
        @BindView(R.id.line)
        View line;
        @BindView(R.id.status_text)
        TextView statusText;
        @BindView(R.id.line2)
        View line2;
        @BindView(R.id.line3)
        View line3;
        @BindView(R.id.point)
        ImageView point;
        @BindView(R.id.status_secod_text)
        TextView statusSecodText;
        @BindView(R.id.create_secoud_time)
        TextView createSecoudTime;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(InvestTrackBeanList protocal, int position, int size) {


            if (position == 0) {

                statusIcon.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                statusText.setVisibility(View.VISIBLE);
                createTime.setVisibility(View.VISIBLE);

                line2.setVisibility(View.GONE);
                line3.setVisibility(View.GONE);

                point.setVisibility(View.GONE);
                statusSecodText.setVisibility(View.GONE);
                createSecoudTime.setVisibility(View.GONE);

                statusIcon.setImageResource(R.mipmap.suc_tz);
                statusText.setTextColor(line.getContext().getResources().getColor(R.color.colorPrimary));
                createTime.setTextColor(line.getContext().getResources().getColor(R.color.colorPrimary));
                createTime.setText(TimeUtils.getFormatTime(protocal.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
                statusText.setText(protocal.getTypeName());

            } else if (position == size - 1) {


                statusIcon.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
                createTime.setVisibility(View.GONE);
                statusText.setVisibility(View.GONE);

                line2.setVisibility(View.VISIBLE);
                line3.setVisibility(View.GONE);

                point.setVisibility(View.VISIBLE);
                statusSecodText.setVisibility(View.VISIBLE);
                createSecoudTime.setVisibility(View.VISIBLE);

                createSecoudTime.setText(TimeUtils.getFormatTime(protocal.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
                statusSecodText.setText(protocal.getTypeName());
            } else {
                statusIcon.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
                createTime.setVisibility(View.GONE);
                statusText.setVisibility(View.GONE);

                line2.setVisibility(View.VISIBLE);
                line3.setVisibility(View.VISIBLE);

                point.setVisibility(View.VISIBLE);
                statusSecodText.setVisibility(View.VISIBLE);
                createSecoudTime.setVisibility(View.VISIBLE);

                createSecoudTime.setText(TimeUtils.getFormatTime(protocal.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
                statusSecodText.setText(protocal.getTypeName());
            }


        }
    }


}
