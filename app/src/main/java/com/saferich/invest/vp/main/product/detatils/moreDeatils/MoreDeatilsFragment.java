package com.saferich.invest.vp.main.product.detatils.moreDeatils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.DeatilsPhotoapter;
import com.saferich.invest.vp.adapter.TxtListAdapter;
import com.saferich.invest.common.widget.PhotoScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.common.WatchPdfActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 *
 * 产品详情详细界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-21
 */

public class MoreDeatilsFragment extends BaseFragment<MoreDeatilsPresenter> implements IMoreView, TxtListAdapter.txtClickLister {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.webView_des)
    WebView webViewDes;
    @BindView(R.id.webView_special)
    WebView webViewSpecial;
    @BindView(R.id.webView_money)
    WebView webViewMoney;
    @BindView(R.id.webView_safe)
    WebView webViewSafe;
    @BindView(R.id.webView_risk)
    WebView webViewRisk;

    @BindView(R.id.product_photo)
    ImageView productPhoto;
    @BindView(R.id.photo_layout)
    RelativeLayout photoLayout;
    @BindView(R.id.product_des)
    ImageView productDes;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.safe)
    ImageView safe;
    @BindView(R.id.risk_tips)
    ImageView riskTips;
    @BindView(R.id.risk_tipss)
    ImageView riskTipss;
    @BindView(R.id.risk_tipsss)
    ImageView riskTipsss;
    @BindView(R.id.file)
    ImageView file;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.txt)
    TextView txt;
    @BindView(R.id.recyclerView_txt)
    RecyclerView recyclerViewTxt;
    @BindView(R.id.txt_layout)
    RelativeLayout txtLayout;

    @Inject
    DeatilsPhotoapter adapter;

    @Inject
    TxtListAdapter txtadapter;
    @BindView(R.id.item_index)
    TextView itemIndex;

    ProductInfo data;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_more_deatils, container,
                false);
    }

    public void setData(ProductInfo data) {
        this.data = data;
        if (data.isAppIsShowCarousel()) {
            itemIndex.setText(1 + "/" + data.getChannelImageCarousel().size());

            adapter.SetListData((ArrayList<ProductInfo.ChannelImageCarousel>) data.getChannelImageCarousel());

        } else {
            photoLayout.setVisibility(View.GONE);
        }
        if (data.getProtocalList().size() > 0) {
            txtadapter.setLister(this);
            txtadapter.setDataList((ArrayList<ProductInfo.ProtocalList>) data.getProtocalList());

        } else {
            photoLayout.setVisibility(View.GONE);
        }

        presenter.setWebView(webViewDes, data.getTextList().get(0).getRich2());
        presenter.setWebView(webViewSpecial, data.getTextList().get(0).getRich1());
        presenter.setWebView(webViewMoney, data.getTextList().get(0).getRich3());
        presenter.setWebView(webViewSafe, data.getTextList().get(0).getRich5());
        presenter.setWebView(webViewRisk, data.getTextList().get(0).getRich4());


    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnScrollListener(new PhotoScrollListener() {
            @Override
            public void changeTitle(int position) {
                if (data.isAppIsShowCarousel()) {
                    itemIndex.setText(position + "/" + data.getChannelImageCarousel().size());
                }
            }
        });

        recyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext());
        linearLayoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewTxt.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerViewTxt.setLayoutManager(linearLayoutManager2);
        recyclerViewTxt.setAdapter(txtadapter);

    }

    @Override
    public void showPdf(String url, String name) {
        Intent intent = new Intent(getContext(), WatchPdfActivity.class);
        intent.putExtra("netUrl", url);
        intent.putExtra("title", name);
        startActivity(intent);
    }

    @Override
    public void watchPdf(ProductInfo.ProtocalList protocal) {
        presenter.downloadPdf(protocal.getProductProtocal(),
                protocal.getForegroundDisplayName());
    }


}
