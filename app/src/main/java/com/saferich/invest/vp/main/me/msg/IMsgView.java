package com.saferich.invest.vp.main.me.msg;

import com.saferich.invest.model.bean.MsgBean;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public interface IMsgView extends IBaseView {
    void showListData(ArrayList<MsgBean.list> data);
}
