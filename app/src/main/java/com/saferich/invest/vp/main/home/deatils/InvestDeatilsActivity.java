package com.saferich.invest.vp.main.home.deatils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.product.detatils.phone.PhoneDialogActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 投资详情界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestDeatilsActivity extends BaseActivity<InvestDeatilsPresenter> implements IInvestDeatilsView {


    InvestTraceFragment fragmentMsg;
    InvestMessageFragment fragmentFlow;
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    ImageView rightTitle;
    @BindView(R.id.invest_count)
    TextView investCount;
    @BindView(R.id.reback_count)
    TextView reback_count;
    @BindView(R.id.deatils_line)
    View deatilsLine;
    @BindView(R.id.deatils_layout)
    RelativeLayout deatilsLayout;
    @BindView(R.id.element_line)
    View elementLine;
    @BindView(R.id.element_layout)
    RelativeLayout elementLayout;


    private Fragment[] fragments;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_invest_deatils;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragments = new Fragment[2];

        fragmentManager = getSupportFragmentManager();


        fragments[0] = fragmentManager.findFragmentById(R.id.fragment_msg);
        fragmentMsg = (InvestTraceFragment) fragments[0];
        fragments[1] = fragmentManager.findFragmentById(R.id.fragment_flow);

        fragmentFlow = (InvestMessageFragment) fragments[1];

        transaction = fragmentManager.beginTransaction().hide(fragments[1])
                .hide(fragments[0]);

        transaction.show(fragments[1]).commit();

        presenter.getdata();

        centerTitle.setText(getIntent().getStringExtra("productName"));

    }

    @OnClick(R.id.deatils_layout)
    public void deatilsClick() {
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[1]).commit();
        deatilsLine.setVisibility(View.VISIBLE);
        elementLine.setVisibility(View.GONE);

    }

    @OnClick(R.id.element_layout)
    public void elementClick() {
        transaction = fragmentManager.beginTransaction().hide(fragments[0])
                .hide(fragments[1]);
        transaction.show(fragments[0]).commit();
        deatilsLine.setVisibility(View.GONE);
        elementLine.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.right_title)
    public void phoneClick() {
        startActivity(new Intent(this, PhoneDialogActivity.class));

    }

    @Override
    public void ShowData(InvestInfo info) {
        investCount.setText(StringUtil.repaceE(info.getRealInvestMoney()));
        if (info.getRealReturnInvest() == -1) {
            reback_count.setText("到期回款:" + "待定");
        } else {
            reback_count.setText("到期回款:" + StringUtil.repaceE(info.getRealReturnInvest()) + "元");
        }

        fragmentMsg.setdata(info);
        fragmentFlow.setdata(info);
    }
}
