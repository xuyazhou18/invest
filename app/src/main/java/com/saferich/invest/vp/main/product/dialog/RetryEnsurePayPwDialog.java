package com.saferich.invest.vp.main.product.dialog;

import android.content.Intent;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.password.SetPayPassWordActivity;
import com.saferich.invest.vp.main.product.detatils.invest.password.DealDialogActivity;

import butterknife.OnClick;

/**
 *
 * 交易密码重试弹窗
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class RetryEnsurePayPwDialog extends BaseActivity<BindCardPresenter> {
    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_retry_paypassword_dialog;
    }


    @OnClick(R.id.retry)
    public void retryClick() {

        Intent intent = new Intent(this, DealDialogActivity.class);

        intent.putExtra("isSetPayPassword", true);
        intent.putExtra("count", getIntent().getStringExtra("count"));
        intent.putExtra("id", getIntent().getStringExtra("id"));
        startActivity(intent);

        finish();
    }

    @OnClick(R.id.forget_password)
    public void forgetClick() {

        Intent intent = new Intent(this, SetPayPassWordActivity.class);
        intent.putExtra("findPay", true);
        startActivity(intent);
        this.finish();

    }


}
