package com.saferich.invest.vp.main.home.myList;

import android.support.v4.app.Fragment;

import com.saferich.invest.model.bean.MyInvest;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class HavingPresenter extends BasePresenter<IHavingView> {
    private ArrayList<MyInvest.NonProfit> investList;
    private boolean isloadMore;
    int page;
    @Inject
    Fragment fragment;


    @Inject
    public HavingPresenter() {
        super();
        investList = new ArrayList<>();
    }

    public void refresh() {
        page = 1;
        isloadMore = false;
        getdata();
    }

    private void getdata() {


        apiWrapper.getUncompleteOrder(fragment)
                .subscribe(newSubscriber((MyInvest data) -> {

                    changeList(data);


                }));
    }

    private void changeList(MyInvest data) {
        investList.clear();

        if (data.getProfit().size() > 0) {
            for (int i = 0; i < data.getProfit().size(); i++) {
                MyInvest.NonProfit profit;
                profit = data.getProfit().get(i);
                if (i == 0) {
                    profit.setType(1);
                }
                investList.add(profit);
            }
        }
        if (data.getNonProfit().size() > 0) {

            for (int i = 0; i < data.getNonProfit().size(); i++) {
                MyInvest.NonProfit profit;
                profit = data.getNonProfit().get(i);
                if (i == 0) {
                    profit.setType(2);
                }

                investList.add(profit);
            }

        }
        if (data.getReturnX().size() > 0) {

            for (int i = 0; i < data.getReturnX().size(); i++) {
                MyInvest.NonProfit profit;
                profit = data.getReturnX().get(i);
                if (i == 0) {
                    profit.setType(3);
                }
                investList.add(profit);
            }
        }
        view.showData(investList);

    }

    public void loadMore() {
        isloadMore = true;
        page++;
        // getdata();
    }

    public boolean isloadMore() {
        return isloadMore;
    }
}
