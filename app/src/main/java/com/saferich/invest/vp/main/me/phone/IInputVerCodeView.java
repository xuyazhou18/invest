package com.saferich.invest.vp.main.me.phone;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public interface IInputVerCodeView extends IBaseView {
    void moveActivity();
}
