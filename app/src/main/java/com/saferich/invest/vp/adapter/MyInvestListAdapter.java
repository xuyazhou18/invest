package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.MyInvest;
import com.saferich.invest.vp.main.home.deatils.InvestDeatilsActivity;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 我的投资列表适配器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class MyInvestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<MyInvest.NonProfit> investList;

    public void setDataList(ArrayList<MyInvest.NonProfit> investList) {
        this.investList = investList;
        notifyDataSetChanged();

    }

    @Inject
    public MyInvestListAdapter() {
        investList = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_invest, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(investList.get(position));


    }


    @Override
    public int getItemCount() {

        return investList.size();


    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.staus_line)
        View stausLine;
        @BindView(R.id.staus)
        TextView staus;
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.product_layout)
        RelativeLayout productLayout;
        @BindView(R.id.invest_count)
        TextView investCount;
        @BindView(R.id.profit_time)
        TextView investTime;
        @BindView(R.id.title_layout)
        LinearLayout titleLayout;
        @BindView(R.id.item_bar)
        LinearLayout itemBar;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(MyInvest.NonProfit nonProfit) {


            if (nonProfit.getType() == 1) {
                itemBar.setVisibility(View.VISIBLE);
                stausLine.setBackgroundColor(icon.getContext().getResources().getColor(R.color.profit_color));
                staus.setText("计息中");
            } else if (nonProfit.getType() == 2) {
                itemBar.setVisibility(View.VISIBLE);
                stausLine.setBackgroundColor(icon.getContext().getResources().getColor(R.color.noprofit_color));
                staus.setText("待计息");
            } else if (nonProfit.getType() == 3) {
                itemBar.setVisibility(View.VISIBLE);
                stausLine.setBackgroundColor(icon.getContext().getResources().getColor(R.color.return_color));
                staus.setText("回款中");
            } else {
                itemBar.setVisibility(View.GONE);
            }

            productName.setText(nonProfit.getProductName());
            investCount.setText(StringUtil.repaceE(nonProfit.getInvestMoney()));
            investTime.setText(nonProfit.getInvestTerm() + "");

            RxView.clicks(titleLayout).subscribe(aVoid -> {
                Intent intent = new Intent(titleLayout.getContext(), InvestDeatilsActivity.class);
                intent.putExtra("id", nonProfit.getOrderId());
                intent.putExtra("productName", nonProfit.getProductName());
                titleLayout.getContext().startActivity(intent);
            });
            RxView.clicks(productLayout).subscribe(aVoid -> {
                Intent intent = new Intent(titleLayout.getContext(), ProductDeatilsActivity.class);
                intent.putExtra("id", nonProfit.getProductId());
                intent.putExtra("productName", nonProfit.getProductName());
                titleLayout.getContext().startActivity(intent);
            });


        }
    }


}
