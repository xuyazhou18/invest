package com.saferich.invest.vp.main.home;

import com.saferich.invest.vp.base.IBaseView;

/**
 * 契约类
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-08-24
 */

public interface HomeContract {

    interface View extends IBaseView {
        void showFragment(boolean isLogin);
    }

    interface Presenter {
        void showFragment();
    }
}
