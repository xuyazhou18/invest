package com.saferich.invest.vp.main.product.detatils.invest;

import android.app.Activity;
import android.content.Intent;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.InvestBody;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;
import com.saferich.invest.vp.main.product.detatils.invest.password.DealDialogActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class InvestPresenter extends BasePresenter<IInvestView> implements IBaseView {

    @Inject
    Activity activity;

    @Inject
    public InvestPresenter() {
        super();
    }

    public void choicePay(String count) {
        Intent intent = new Intent(activity, DealDialogActivity.class);

        intent.putExtra("count", count);
        intent.putExtra("id", activity.getIntent().getStringExtra("id"));

        activity.startActivity(intent);
    }


    public void addorder(String count) {


        InvestBody body = new InvestBody();
        body.setChannel("0");
        body.setPayWay("银行卡");
        body.setPayMethod("银行卡");
        body.setProductId(activity.getIntent().getStringExtra("id"));
      //  body.setProductId("QDFina000002");
        body.setInvestMoney(count);
        body.setCert_type("01");
        body.setCurrency("156");
        body.setTerminal_info("2222222");
        body.setTerminal_type("mobile");
        body.setUser_ip("192.168.0.122");


        apiWrapper.addorder(context, body)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((OrderInfo data) -> {
                    view.moveActivity(data);

                }));


    }


    public void init() {
        params.put("p_bankCode", userinfo.bankCode);
        params.put("p_channel", "RB");

        apiWrapper.getBankLimitByCodeAndChannel(context, params)
                .subscribe(newSubscriber((RechargeBean data) -> {
                    view.showData(data);
                }));


    }

    public void addSub() {
        subscription.add(rxBus.subscribe(event -> {
                    activity.finish();
                },
                InvestSuccessEvent.class));
    }

    public void Unsubscribe() {
        subscription.unsubscribe();
    }

}
