package com.saferich.invest.vp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.saferich.invest.R;
import com.saferich.invest.vp.main.MainPresenter;

import java.util.HashMap;


/**
 * 主界面的底部导航栏基类
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-25
 */
public abstract class BaseTabBottomActivity extends BaseActivity<MainPresenter>
        implements ITabView{

    protected HashMap<Integer, Fragment> mainFragmentList = new HashMap<>();
    int currentFragmentIndex = -1;

    Fragment currentFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResourceId() {//填充布局
        return R.layout.activity_main;
    }


    @Override
    public int getCurrentTabIndex() {//获取当前tab的索引值
        return currentFragmentIndex;
    }

    @Override
    public int getTabCount() {//获取tab数目
        return mainFragmentList.size();
    }

    @Override
    public void switchTab(int tabIndex) {//切换tab
        // assertThat(tabIndex >= 0 && tabIndex < totalFragmentCount);

        if (tabIndex == currentFragmentIndex)
            return;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        Fragment fragment = mainFragmentList.get(tabIndex);

        if (fragment == null) {
            fragment = createFragment(tabIndex);
            mainFragmentList.put(tabIndex, fragment);
            transaction.add(R.id.main_content_container, fragment)
                    .commitAllowingStateLoss();
        } else {
            transaction.show(fragment).commitAllowingStateLoss();
        }

        currentFragment = fragment;
        currentFragmentIndex = tabIndex;
    }


    protected abstract Fragment createFragment(int index);//创建底部的fragment
}
