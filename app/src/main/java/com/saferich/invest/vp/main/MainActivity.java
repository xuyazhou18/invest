package com.saferich.invest.vp.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;

import com.saferich.invest.R;
import com.saferich.invest.common.config.StateKey;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.base.BaseTabBottomActivity;
import com.saferich.invest.vp.main.discover.DiscoverFragment;
import com.saferich.invest.vp.main.home.HomeFragment;
import com.saferich.invest.vp.main.me.MeFragment;
import com.saferich.invest.vp.main.product.ProductFragment;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主界面
 */

public class MainActivity extends BaseTabBottomActivity implements IMainView, CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.main_content_container)
    FrameLayout mainContentContainer;
    @BindView(R.id.tab_home)
    RadioButton tabHome;
    @BindView(R.id.tab_discover)
    RadioButton tabDiscover;
    @BindView(R.id.tab_product)
    RadioButton tabProduct;
    @BindView(R.id.tab_me)
    RadioButton tabMe;

    private int curTab;
    private Bundle savedState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        tabHome.setOnCheckedChangeListener(this);
        tabDiscover.setOnCheckedChangeListener(this);
        tabProduct.setOnCheckedChangeListener(this);
        tabMe.setOnCheckedChangeListener(this);

        savedState = savedInstanceState;
        if (savedState != null) {
            int tab = savedState.getInt(StateKey.SAVE_STATE_KEY_HOME_TAB, 0);
            moveToOtherTab(tab);
        } else {
            switchTab(0);
        }

        presenter.init();
        presenter.initPresmision();

    }

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);

    }

    @Override
    protected Fragment createFragment(int index) {
        BaseFragment fragment;
        switch (index) {
            case IMainView.MAIN_TAB_HOME:
                fragment = new HomeFragment();
                break;
            case IMainView.MAIN_TAB_DISCOVER:
                fragment = new DiscoverFragment();
                break;
            case IMainView.MAIN_TAB_PRODUCT:
                fragment = new ProductFragment();
                break;
            case IMainView.MAIN_TAB_ME:
                fragment = new MeFragment();
                break;

            default:
                fragment = new HomeFragment();
                break;
        }

        fragment.setArguments(savedState);
        return fragment;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (!isChecked) {
            return;
        }

        switch (buttonView.getId()) {
            case R.id.tab_home:
                gotoHome();

                break;
            case R.id.tab_discover:
                gotoDiscover();

                break;
            case R.id.tab_product:
                gotoProduct();

                break;
            case R.id.tab_me:
                gotoMe();

                break;

            default:
                break;

        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(StateKey.SAVE_STATE_KEY_HOME_TAB, curTab);

        for (Map.Entry<Integer, Fragment> entry : mainFragmentList
                .entrySet()) {
            Fragment fragment = entry.getValue();
            fragment.onSaveInstanceState(outState);
        }
    }

    private void gotoMe() {
        switchTab(IMainView.MAIN_TAB_ME);
        curTab = IMainView.MAIN_TAB_ME;


    }

    private void gotoProduct() {
        switchTab(IMainView.MAIN_TAB_PRODUCT);
        curTab = IMainView.MAIN_TAB_PRODUCT;

    }

    private void gotoDiscover() {
        switchTab(IMainView.MAIN_TAB_DISCOVER);
        curTab = IMainView.MAIN_TAB_DISCOVER;


    }

    private void gotoHome() {
        switchTab(IMainView.MAIN_TAB_HOME);
        curTab = IMainView.MAIN_TAB_HOME;

    }


    public void moveToOtherTab(int index) {
        switch (index) {
            case IMainView.MAIN_TAB_HOME:
                tabHome.setChecked(true);
                break;
            case IMainView.MAIN_TAB_DISCOVER:
                tabDiscover.setChecked(true);
                break;
            case IMainView.MAIN_TAB_PRODUCT:
                tabProduct.setChecked(true);
                break;
            case IMainView.MAIN_TAB_ME:
                tabMe.setChecked(true);
                break;

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.Unsubscribe();
    }
}
