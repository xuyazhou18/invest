package com.saferich.invest.vp.main.me.bank;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.BankUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 绑定银行卡界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class BindBankActivity extends BaseActivity<BindBankPresenter> implements IbindBankView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.clear_name)
    ImageView clearName;
    @BindView(R.id.edit_id)
    EditText editId;
    @BindView(R.id.clear_id)
    ImageView clearId;
    @BindView(R.id.bank_name)
    TextView bankName;
    @BindView(R.id.bank_layout)
    RelativeLayout bankLayout;
    @BindView(R.id.bank_card)
    EditText bankCard;
    @BindView(R.id.clear_card)
    ImageView clearCard;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.clear_phone)
    ImageView clearPhone;
    @BindView(R.id.btn_bind)
    Button btnBind;
    @BindView(R.id.bank_tips)
    TextView bankTips;
    private String bankCode;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_binding_card;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("ischange", false)) {
            centerTitle.setText("修改银行卡");
            btnBind.setText("修改银行卡");
            editName.setText(getIntent().getStringExtra("name"));
            editId.setText(getIntent().getStringExtra("idCard"));
            clearName.setVisibility(View.INVISIBLE);
            clearId.setVisibility(View.INVISIBLE);
            editName.setEnabled(false);
            editId.setEnabled(false);
        } else {
            centerTitle.setText("绑定银行卡");
        }

        bindClick();
        presenter.addSub(bankName);
    }


    public void bindClick() {
        RxView.clicks(btnBind)
                .throttleFirst(500, TimeUnit.MILLISECONDS)


                .filter(aVoid -> StringUtil.checkIsNotEmpty(editName,
                        R.string.empty_name))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(editId,
                        R.string.empty_id))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(bankCard,
                        R.string.empty_bank_card))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(phone,
                        R.string.empty_phone))


                .subscribe(aVoid ->
                        presenter.code());
    }

    @OnClick(R.id.bank_layout)
    public void bankLayoutClick() {
        ActivityUtil.moveToActivity(this, BankListActivity.class);
    }


    @OnClick(R.id.clear_name)
    public void nameClearClick() {
        editName.setText("");
    }

    @OnClick(R.id.clear_id)
    public void idClearClick() {
        editId.setText("");
    }

    @OnClick(R.id.clear_card)
    public void cardClearClick() {
        bankCard.setText("");
    }

    @OnClick(R.id.clear_phone)
    public void phoneClearClick() {
        phone.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.Unsubscribe();
    }

    @Override
    public void successful(String data) {
        Bundle bundle = new Bundle();
        bundle.putString("number", phone.getText().toString().trim());
        Log.d("xyz", "input------->bankcode-------->" + BankUtils.bankCode.get(bankName.getText().toString().trim()));
        bundle.putString("bankCode", BankUtils.bankCode.get(bankName.getText().toString().trim()));
        bundle.putString("bankcard", bankCard.getText().toString().trim());
        bundle.putString("idcard", editId.getText().toString().trim());
        bundle.putString("realname", editName.getText().toString().trim());
        bundle.putString("data", data);
        if (getIntent().getBooleanExtra("ischange", false)) {
            bundle.putBoolean("ischange", true);
        }
        ActivityUtil.moveToActivity(this, BankInputPhoneNumberActivity.class, bundle);

    }

}
