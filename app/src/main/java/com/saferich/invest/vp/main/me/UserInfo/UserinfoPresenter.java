package com.saferich.invest.vp.main.me.userinfo;

import android.app.Activity;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.UserMessage;
import com.saferich.invest.model.event.BankSuccessEvent;
import com.saferich.invest.model.event.PayPWSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-13
 */

public class UserinfoPresenter extends BasePresenter<IUserInfoView> {

    @Inject
    Activity activity;

    @Inject
    public UserinfoPresenter() {
    }

    public void logout() {
        config.isLogin = false;
        config.update();
        userinfo = new Userinfo();
        userinfo.update();

        apiWrapper.logout(context)
                .doOnSubscribe(() -> showDialogView(R.string.doing, true))
                .doOnTerminate(this::doComplete)
                .subscribe(newSubscriber((Object object) -> {
                    if (object == null) {


                    }
                }));
    }

    private void doComplete() {
        ShowToast.Short(context, "退出登录成功");
        dismissDialog();
        activity.finish();
    }


    public void addSub(TextView bindText, TextView pay) {


        subscription.add(rxBus.subscribe(event -> {
                    bindText.setText("修改");
                    apiWrapper.getAccountInfo(context).subscribe(newSubscriber((UserMessage message) ->
                            view.showData(message)
                    ));
                },
                BankSuccessEvent.class));

        subscription.add(rxBus.subscribe(event -> {
                    pay.setText("修改");
                    apiWrapper.getAccountInfo(context).subscribe(newSubscriber((UserMessage message) ->
                            view.showData(message)
                    ));
                },
                PayPWSuccessEvent.class));

        apiWrapper.getAccountInfo(context).subscribe(newSubscriber((UserMessage message) ->
                view.showData(message)
        ));

    }

    public void Unsubscribe() {
        subscription.unsubscribe();
    }

    public void changeGesture(boolean avalible) {
        config.isGesture = avalible;
        config.update();
    }
}
