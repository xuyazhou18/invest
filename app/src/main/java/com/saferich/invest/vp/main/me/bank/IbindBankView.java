package com.saferich.invest.vp.main.me.bank;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public interface IbindBankView extends IBaseView {
    void successful(String data);

}
