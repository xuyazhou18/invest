package com.saferich.invest.vp.wellcome;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-30
 */

public class WellcomePresenter extends BasePresenter<IWellcomeView> {

    @Inject
    public WellcomePresenter() {
    }
}
