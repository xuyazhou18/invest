package com.saferich.invest.vp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.SystemUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 用户消息下拉菜单适配器
 */

public class ListDropDownAdapter extends BaseAdapter {

    private Context context;
    private List<String> list;
    private int checkItemPosition = 0;

    public void setCheckItem(int position) {
        checkItemPosition = position;
        notifyDataSetChanged();
    }

    public ListDropDownAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_default_drop_down, null);
            int weight = SystemUtil.getScreenWidth(context);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(weight / 2, ViewGroup.LayoutParams.WRAP_CONTENT);
            convertView.setLayoutParams(params);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        fillValue(position, viewHolder);
        return convertView;
    }

    private void fillValue(int position, ViewHolder viewHolder) {
        viewHolder.mText.setText(list.get(position));
        if (position == 0) {
            viewHolder.imageView.setVisibility(View.VISIBLE);
        }
        if (checkItemPosition != -1) {
            if (checkItemPosition == position) {
                viewHolder.mText.setTextColor(context.getResources().getColor(R.color.blue));
//                viewHolder.mText.setBackgroundResource(R.color.check_bg);
                viewHolder.imageView.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mText.setTextColor(context.getResources().getColor(R.color.drop_down_unselected));
//                viewHolder.mText.setBackgroundResource(R.color.white);
                viewHolder.imageView.setVisibility(View.INVISIBLE);
            }
        }

        if (position == 6) {
            viewHolder.line.setVisibility(View.VISIBLE);
            viewHolder.closeBtn.setVisibility(View.VISIBLE);
        } else {
            viewHolder.line.setVisibility(View.GONE);
            viewHolder.closeBtn.setVisibility(View.GONE);
        }
    }


    static class ViewHolder {
        @BindView(R.id.text)
        TextView mText;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.line)
        View line;
        @BindView(R.id.close_btn)
        ImageView closeBtn;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
