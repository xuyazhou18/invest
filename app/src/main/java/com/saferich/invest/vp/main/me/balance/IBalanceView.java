package com.saferich.invest.vp.main.me.balance;

import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.Balance;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public interface IBalanceView extends IBaseView {
    void showData(ArrayList<Balance> list);
    void showHeadData(Account data);

}
