package com.saferich.invest.vp.main.me.getCash;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public class GetCashPresenter extends BasePresenter<ICashView> {

    @Inject
    public GetCashPresenter() {
    }

    public void init() {
        apiWrapper.getExtractCashParameterList(context)
                .subscribe(newSubscriber((List<CashItem> list) -> {
                    view.showData(list);

                }));
    }
}
