package com.saferich.invest.vp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.NetworkUtil;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.widget.SlideShowView;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 产品推荐列表
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class RecommendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<RecommondItem> recomondList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private String[] imageList;
    private String[] linkList;

    @Inject
    public RecommendAdapter() {
        recomondList = new ArrayList<>();
    }


    public void SetListData(ArrayList<RecommondItem> productList) {

        this.recomondList = productList;

        notifyDataSetChanged();
    }

    public void SetHeaderData(String[] data, String[] link) {
        imageList = new String[data.length];
        linkList = new String[data.length];
        this.imageList = data;
        this.linkList = link;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recommend, parent, false);

            return new ItemViewHolder(view);
        }

        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommend_header, parent, false);
            return new HeaderViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.bindTo(recomondList.get(position - 1));


        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.setIsRecyclable(false);
            headerViewHolder.slideshowView.setImageUrls(imageList, linkList);

        }

    }

    @Override
    public int getItemCount() {
        return recomondList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_icon)
        ImageView productIcon;
        @BindView(R.id.yield_rate)
        TextView yieldRate;
        @BindView(R.id.yield_rate_number)
        TextView yieldRateNumber;
        @BindView(R.id.add_icon)
        TextView addIcon;
        @BindView(R.id.floatText)
        TextView floatText;
        @BindView(R.id.rete_layout)
        LinearLayout reteLayout;
        @BindView(R.id.product_tips)
        TextView productTips;
        @BindView(R.id.product_time)
        TextView productTime;
        @BindView(R.id.prsent)
        TextView prsent;
        @BindView(R.id.item_layout)
        LinearLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(RecommondItem recommondItem) {


            if (recommondItem.getBuyingGroupsCode().equals("BuyingGroupsCode_NEW")) {
                productIcon.setVisibility(View.VISIBLE);
            } else {
                productIcon.setVisibility(View.GONE);
            }

            if (recommondItem.getProductProfitTypeCode().equals("FixedIncome")) {
                yieldRateNumber.setText(StringUtil.repaceE(recommondItem.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                floatText.setVisibility(View.GONE);

            }

            if (recommondItem.getProductProfitTypeCode().equals("FloatIncome")) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                prsent.setVisibility(View.GONE);
                floatText.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                yieldRateNumber.setVisibility(View.GONE);
            }
            if (recommondItem.getProductProfitTypeCode().equals("Fixed&FloatIncome")) {
                floatText.setText(recommondItem.getFloatProfit() + "");
                yieldRateNumber.setText(StringUtil.repaceE(recommondItem.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.VISIBLE);
                floatText.setVisibility(View.VISIBLE);
            }

            productName.setText(recommondItem.getProductName());
            productTips.setText(recommondItem.getMinInvestMoney() + "元起投");
            productTime.setText(recommondItem.getInvestTerm() + "天投资期限");


            RxView.clicks(itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .filter(aVoid -> NetworkUtil.checkNet(itemLayout.getContext()))
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(productName.getContext(),
                                ProductDeatilsActivity.class);
                        intent.putExtra("id", recommondItem.getId());

                        productName.getContext().startActivity(intent);
                    });
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.slideshowView)
        SlideShowView slideshowView;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
