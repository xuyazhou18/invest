package com.saferich.invest.vp.main.me.password;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 修改交易密码界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class ChangePayPasswordActivity extends BaseActivity<PasswordPresenter> implements IPasswordView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.old_password)
    EditText oldPassword;
    @BindView(R.id.clear_code)
    ImageView delIcon;
    @BindView(R.id.passwordTips)
    TextView passwordTips;
    @BindView(R.id.icon_password)
    ImageView iconPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.del_icon2)
    ImageView delIcon2;
    @BindView(R.id.newpwTips)
    TextView newpwTips;
    @BindView(R.id.del_icon3)
    ImageView delIcon3;
    @BindView(R.id.ensure_password)
    EditText ensurePassword;
    @BindView(R.id.ensure_tips)
    TextView ensureTips;
    @BindView(R.id.btn_set)
    Button btnSet;
    @BindView(R.id.toast_layout)
    LinearLayout toastLayout;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_change_pay_password;
    }

    @Override
    public void succefull() {
        ShowToast.Short(this, "交易密码修改成功");
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("修改交易密码");

        passwordClick();
        addTextWatch();


    }

    @OnClick(R.id.forget_password)
    public void forgetPassClick() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("findPay", true);
        ActivityUtil.moveToActivity(this, SetPayPassWordActivity.class);
    }


    public void passwordClick() {
        RxView.clicks(btnSet)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> {
                    if (StringUtil.checkIsNotEmpty(oldPassword)) {
                        return true;
                    } else {

                        passwordTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .filter(aVoid -> StringUtil.checkLenght(oldPassword, 6,
                        R.string.string_length2))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(newPassword,
                        R.string.empty_new_password))
                .filter(aVoid -> StringUtil.checkLenght(newPassword, 6,
                        R.string.string_length2))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(ensurePassword,
                        R.string.empty_new_password))
                .filter(aVoid -> StringUtil.checkLenght(ensurePassword, 6,
                        R.string.string_length2))
                .filter(aVoid -> {
                    if (StringUtil.checkIsEqual(newPassword, ensurePassword)) {
                        return true;
                    } else {
                        ensureTips.setVisibility(View.VISIBLE);
                        return false;
                    }

                })
                .subscribe(aVoid ->
                        presenter.changePaypassword(oldPassword, newPassword));
    }

    @OnClick(R.id.clear_code)
    public void oldClearClick() {
        oldPassword.setText("");
    }

    @OnClick(R.id.del_icon2)
    public void newClearClick() {
        newPassword.setText("");
    }

    @OnClick(R.id.del_icon3)
    public void ensureClearClick() {
        ensurePassword.setText("");
    }

    private void addTextWatch() {
        oldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordTips.setVisibility(View.GONE);
            }
        });
        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                newpwTips.setVisibility(View.GONE);
                ensureTips.setVisibility(View.GONE);
            }
        });
        ensurePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ensureTips.setVisibility(View.GONE);
            }
        });
    }
}
