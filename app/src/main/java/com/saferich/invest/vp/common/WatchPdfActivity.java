package com.saferich.invest.vp.common;

import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;

import java.io.File;

import butterknife.BindView;


/**
 * 查看pdf
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-01
 */

public class WatchPdfActivity extends BaseActivity<WatchPdfPresenter> implements OnPageChangeListener {
    @BindView(R.id.pdfView)
    PDFView pdfView;
    Integer pageNumber = 1;
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pdf;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getStringExtra("url") == null) {
            pdfView.fromUri(Uri.fromFile(new File(getIntent().getStringExtra("netUrl"))))
                    .defaultPage(pageNumber)
                    .onPageChange(this)
                    .load();
        } else {
            pdfView.fromAsset(getIntent().getStringExtra("url"))
                    .defaultPage(pageNumber)
                    .onPageChange(this)
                    .load();
        }


    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        centerTitle.setText(String.format("%s %s / %s", getIntent().getStringExtra("title"), page, pageCount));
    }
}
