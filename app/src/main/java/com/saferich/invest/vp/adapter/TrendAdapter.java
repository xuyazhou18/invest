package com.saferich.invest.vp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.SystemUtil;
import com.saferich.invest.common.config.Constant;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.common.WebDeatilsActivity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 图片广告位适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class TrendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<AdCount> adcountList;
    private boolean isNomal;


    @Inject
    public TrendAdapter() {
        adcountList = new ArrayList<>();
    }


    public void SetListData(ArrayList<AdCount> adcountList, boolean isNomal) {

        this.adcountList = adcountList;
        this.isNomal = isNomal;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trend, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(adcountList.get(position), isNomal);


    }

    @Override
    public int getItemCount() {
        return adcountList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(AdCount adCount, boolean isNomal) {

            int realWith = SystemUtil.getScreenWidth(image.getContext());
            if (isNomal) {
                Glide.with(image.getContext()).load(Constant.imagUrl + adCount.getPic())
                        // .placeholder(R.mipmap.default_read_item)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(realWith, realWith * 300 / 750)
                        .into(image);


            } else {
                Glide.with(image.getContext()).load(Constant.imagUrl + adCount.getPic())
                        // .placeholder(R.mipmap.default_read_item)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(realWith, realWith * 200 / 750)
                        .into(image);
            }

            Log.d("ok", Constant.imagUrl + adCount.getPic());
            RxView.clicks(image).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {
                        Intent intent = new Intent(image.getContext(),
                                WebDeatilsActivity.class);
                        intent.putExtra("url", adCount.getForwardUrl());
                        image.getContext().startActivity(intent);
                    });
        }


    }


}
