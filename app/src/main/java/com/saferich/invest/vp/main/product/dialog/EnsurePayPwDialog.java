package com.saferich.invest.vp.main.product.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.product.detatils.invest.password.DealDialogActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 交易密码错误弹窗
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class EnsurePayPwDialog extends BaseActivity<BindCardPresenter> {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.reset)
    TextView reset;
    @BindView(R.id.retry)
    TextView retry;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_ensure_paypassword_dialog;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.reset)
    public void resetClick() {
        startActivity(new Intent(this, DealDialogActivity.class));
        this.finish();
    }

    @OnClick(R.id.retry)
    public void retryClick() {
        Intent intent = new Intent(this, DealDialogActivity.class);
        intent.putExtra("isRetry", true);
        intent.putExtra("password", getIntent().getStringExtra("firstPassword"));
        startActivity(intent);
        this.finish();

    }

}
