package com.saferich.invest.vp.main.discover.more.mediaList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.MediaListAdapter;
import com.saferich.invest.common.recyclerview.EndlessRecyclerOnScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * 媒体列表界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class MediaListActivity extends BaseActivity<MedialistPresenter> implements IMediaListView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Inject
    MediaListAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.common_refresh;
    }

    private void initUI() {


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(this, 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);

                presenter.loadMore();
            }
        });

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });

        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);

    }

    @Override
    public void showListData(ArrayList<Article> questionlist) {
        if (presenter.isloadMore()) {

            //  refresh.finishRefreshLoadMore();
        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            ptrFrame.refreshComplete();

        }

        adapter.setDataList(questionlist);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        centerTitle.setText("媒体新闻");
    }
}
