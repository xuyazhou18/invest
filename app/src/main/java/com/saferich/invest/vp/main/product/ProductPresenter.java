package com.saferich.invest.vp.main.product;

import com.saferich.invest.model.event.InvestSuccessEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class ProductPresenter extends BasePresenter<IProductView> implements IBaseView {

    @Inject
    public ProductPresenter() {
        super();
    }


    public void addSub() {
        subscription.add(rxBus.subscribe(event -> {
                    if (view != null) {
                        view.refreshData();
                    }
                },
                InvestSuccessEvent.class));
    }
}
