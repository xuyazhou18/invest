package com.saferich.invest.vp.main.me;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class MePresenter extends BasePresenter<IMeView> implements IBaseView {

    @Inject
    public MePresenter() {
        super();
    }

    public void showFragment() {

        view.showFragment(config.isLogin());
    }

    /**
     * Author: lampard_xu(xuyazhou18@gmail.com)
     * <p>
     * Date: 2016-06-06
     */

    public interface IVisterView extends IBaseView {
    }
}
