package com.saferich.invest.vp.common;

import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import javax.inject.Inject;


/**
 *
 * 本地网络浏览器的控制器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-18
 */
public class WebDeatilsPresenter extends BasePresenter<IBaseView> {

    @Inject
    public WebDeatilsPresenter() {
        super();
    }
}
