package com.saferich.invest.vp.main.product.detatils.invest;

import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class InvestSuceesPresenter extends BasePresenter<IInvestSuccessView> {

    @Inject
    public InvestSuceesPresenter() {
    }

    public void getdata() {
        apiWrapper.getAdContents(context, "5").subscribe(newSubscriber((List<AdCount> list) -> {

            view.ShowData((ArrayList<AdCount>) list);
        }));


        apiWrapper.getAccountMessage(context).subscribe(newSubscriber((Account data) -> {
            userinfo.isBindCard = data.isIfBindCard();
            userinfo.balance = data.getBalance();
            userinfo.totalMoney = data.getTotalMoney();
            userinfo.isSetPayWord = data.isIfSetPayPassword();
            userinfo.ifCardUsed = data.isIfCardUsed();
            userinfo.ifNewInvest = data.isIfNewInvest();

            userinfo.mobliePhone = data.getBindMobilePhone();
            userinfo.userLoginPhoneNumber = data.getMobile();
            if (data.isIfBindCard()) {
                userinfo.bankCard = data.getBankCard();
                userinfo.bankCode = data.getBankCode();
            }

            userinfo.update();

        }));
    }


}
