package com.saferich.invest.vp.main.home;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.InvestAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 *
 * 登陆后首页界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestFragment extends BaseFragment<InvestPresenter> implements IInvestView {
    @BindView(R.id.recyclerView_tile)
    RecyclerView recyclerViewTile;

    @BindView(R.id.refresh)
    MaterialRefreshLayout refresh;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;

    @Inject
    InvestAdapter titleAdapter;
    @Inject
    UserConfig userConfig;


    @Override
    protected void lazyLoad() {

        if (!isPrepared) {
            return;
        }

        if (!canLoadData(multiStateView, titleAdapter)) {
            return;
        }


    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_invest, container,
                false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
        presenter.addSub();
        presenter.getAdContent();
    }

    private void initUI() {

        LinearLayoutManager tiTlelinearLayoutManager = new LinearLayoutManager(getContext());
        tiTlelinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewTile.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(getContext(), 7)));
        recyclerViewTile.setLayoutManager(tiTlelinearLayoutManager);
        recyclerViewTile.setAdapter(titleAdapter);

        refresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                presenter.refresh();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {

                refresh.finishRefreshLoadMore();
            }
        });
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        refresh.autoRefresh();

        if (userConfig.isLogin) {
            presenter.getHeaderdata();
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.Unsubscribe();
    }

    @Override
    public void showData(ArrayList<RecommondItem> data) {

        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        refresh.finishRefresh();

        if (data != null) {
            titleAdapter.SetListData(data);
        } else {
            titleAdapter.SetListData(new ArrayList<RecommondItem>());
        }

    }

    @Override
    public void showHeaderData(ProfitBean data) {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        if (data != null) {
            titleAdapter.setHeaderData(data);
        }
        refresh.finishRefresh();
    }

    @Override
    public void showFootData(ArrayList<AdCount> data) {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        if (data != null) {
            titleAdapter.setFootData(data);
        }
        refresh.finishRefresh();
    }

    @Override
    public void error() {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        refresh.finishRefresh();
    }


}
