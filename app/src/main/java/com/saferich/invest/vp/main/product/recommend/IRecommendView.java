package com.saferich.invest.vp.main.product.recommend;

import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-03
 */
public interface IRecommendView extends IBaseView {
    void showData(ArrayList<RecommondItem> data);
    void showAdData(String[] data, String[] link);
    void error();
}
