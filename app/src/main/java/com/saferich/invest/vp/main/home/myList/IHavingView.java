package com.saferich.invest.vp.main.home.myList;

import com.saferich.invest.model.bean.MyInvest;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public interface IHavingView extends IBaseView {
    void showData(ArrayList<MyInvest.NonProfit> data);
}
