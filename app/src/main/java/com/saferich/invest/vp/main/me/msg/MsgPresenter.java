package com.saferich.invest.vp.main.me.msg;

import android.support.v4.app.Fragment;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.MsgBean;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class MsgPresenter extends BasePresenter<IMsgView> {


    @Inject
    Fragment fragment;

    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<MsgBean.list> noticeList;
    private int currentPage = 1;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Inject
    public MsgPresenter() {
        super();
        noticeList = new ArrayList<>();
    }

    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
    }

    public void getData() {
        params.clear();

        if (type != null) {
            params.put("p_type", type);
        }
        params.put("p_page", currentPage);
        params.put("p_maxRows", "10");


        apiWrapper.getSystemMessages(fragment, params).subscribe(newSubscriber((MsgBean bean) -> {
            if (isloadMore) {
                if (noticeList.size() > 0) {
                    noticeList.addAll(bean.getList());
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                noticeList.clear();

                noticeList.addAll(bean.getList());


            }
            view.showListData((ArrayList<MsgBean.list>) noticeList);

        }));

    }

    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }

    public void readItem(int id) {
        params.clear();
        params.put("p_id", id);

        apiWrapper.readMsg(fragment, params).subscribe(newSubscriber((MsgBean bean) -> {


        }));
    }

    public void readAll() {

        apiWrapper.readAllMsg(fragment).subscribe(newSubscriber((MsgBean bean) -> {


        }));
    }
}
