package com.saferich.invest.vp.main.me.phone;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.RxUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscriber;

/**
 *
 * 输入验证码界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class InputVerCodeActivity extends BaseActivity<InputVerCodePresenter> implements IInputVerCodeView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.send_code)
    Button sendVerifCode;
    @BindView(R.id.clear_code)
    ImageView delIcon;
    @BindView(R.id.btn_invest)
    Button btnInvest;
    @BindView(R.id.editText_code)
    EditText editText;
    @BindView(R.id.tips)
    TextView tips;
    @BindView(R.id.title)
    TextView title;
    private boolean isGetCash;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.phone_identification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        centerTitle.setText("验证手机号码");
        title.setText("本次操作需要信息确认,验证码已经发送到您的手机:" +
                StringUtil.replaceNumber(getIntent().getStringExtra("data")) + ",请注意查收");

        isGetCash = getIntent().getBooleanExtra("isGetCash", false);

        if (isGetCash) {
            presenter.cashcode();
        }
        doCountTime();


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tips.setVisibility(View.GONE);
            }
        });

        nextClick();
        codeClick();

    }

    private void nextClick() {
        RxView.clicks(btnInvest)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .filter(aVoid -> StringUtil.checkIsNotEmpty(editText,
                        R.string.verif_code_isnot_empty))
                .subscribe(aVoid -> {
                    if (isGetCash) {
                        presenter.getCash(editText, getIntent().getExtras());
                    } else {
                        presenter.getRechare(editText, getIntent().getExtras());
                    }

                });
    }

    private void codeClick() {
        RxView.clicks(sendVerifCode)
                .throttleFirst(500, TimeUnit.MILLISECONDS)

                .subscribe(aVoid -> {
                    if (isGetCash) {
                        presenter.cashcode();
                        doCountTime();
                    } else {
                        presenter.rechareCode(getIntent().getExtras());
                        doCountTime();
                    }
                });

    }


    public void doCountTime() {

        RxUtils.Countdown(60).
                subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                        sendVerifCode.setText("获取验证码");
                        sendVerifCode.setBackgroundColor(getResources().getColor(R.color.material_red));
                        sendVerifCode.setEnabled(true);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        sendVerifCode.setEnabled(false);
                        sendVerifCode.setBackgroundColor(getResources().getColor(R.color.code_background));
                        sendVerifCode.setText("重新获取(" + integer + ")秒");
                    }
                });
    }

    @OnClick(R.id.clear_code)
    public void clearClick() {
        editText.setText("");
    }


    @Override
    public void moveActivity() {

    }
}
