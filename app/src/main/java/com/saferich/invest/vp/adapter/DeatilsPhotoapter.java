package com.saferich.invest.vp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.SystemUtil;
import com.saferich.invest.model.bean.ProductInfo.ChannelImageCarousel;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 产品详细信息的产品项目实拍适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class DeatilsPhotoapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<ChannelImageCarousel> channelList;


    @Inject
    public DeatilsPhotoapter() {
        channelList = new ArrayList<>();
    }


    public void SetListData(ArrayList<ChannelImageCarousel> channelList) {

        this.channelList = channelList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_channel, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(channelList.get(position));
        // itemViewHolder.bindTo();


    }

    @Override
    public int getItemCount() {
        return channelList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(ChannelImageCarousel channelImageCarousel) {
            int realWith = SystemUtil.getScreenWidth(imageView.getContext());
            Glide.with(imageView.getContext()).load(channelImageCarousel.getImageAddress())
                    // .placeholder(R.mipmap.default_read_item)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(realWith, realWith * 240 / 750)
                    .into(imageView);

            RxView.clicks(imageView).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribe(aVoid -> {

                    });
        }
    }


}
