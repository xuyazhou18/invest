package com.saferich.invest.vp.main.discover.more.Help.deatils;

import android.app.Activity;

import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class QuestionDeatilsPresenter extends BasePresenter<IQuestionView> {

    @Inject
    Activity activity;

    @Inject
    public QuestionDeatilsPresenter() {
        super();
    }

    public void getdata() {


        apiWrapper.getQuestionById(activity, activity.getIntent().getIntExtra("id", 0))
                .subscribe(newSubscriber((Question data) -> {
            view.showData(data);
        }));

    }
}
