package com.saferich.invest.vp.main.me.result;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.TrendAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.getCash.GetCashActivity;
import com.saferich.invest.vp.main.me.rechare.RechargeActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 充值或者提现结果界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public class BalanceResultActivity extends BaseActivity<BalanceResultPresenter> implements IResultView {

    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.status_one)
    TextView statusOne;
    @BindView(R.id.status_two)
    TextView statusTwo;
    @BindView(R.id.invest_count)
    TextView tipsoneCount;
    @BindView(R.id.tipsThree)
    TextView tipsThree;
    @BindView(R.id.product_name)
    TextView accoutCount;
    @BindView(R.id.tipOne)
    TextView tipOne;
    @BindView(R.id.continue_layout)
    TextView continueLayout;
    @BindView(R.id.hot_activity)
    TextView hotActivity;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    Userinfo userinfo;
    @Inject
    TrendAdapter adapter;
    private boolean isCash;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_rechare_success;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isCash = getIntent().getBooleanExtra("isGetCash", false);

        if (isCash) {
            status.setText("提现申请成功");
            statusOne.setText("      尊敬的客户,您的的提现申请已提交成功,具体的投资结果信息,将以短信和系统的消息方式发送给您");
            statusTwo.setText("提现信息");
            tipOne.setText("提现金额");
            tipsThree.setText("账号金额");
            centerTitle.setText("提现申请成功");
            continueLayout.setText("继续提现>>");
        } else {
            centerTitle.setText("充值申请成功");
        }
        initUI();

        accoutCount.setText(getIntent().getStringExtra("blance") + "元");
        tipsoneCount.setText(getIntent().getStringExtra("rechargeMoney") + "元");

    }

    @OnClick(R.id.continue_layout)
    public void coutinueClick() {
        if (isCash) {
            startActivity(new Intent(this, GetCashActivity.class));
        } else {
            startActivity(new Intent(this, RechargeActivity.class));
        }
        this.finish();
    }

    private void initUI() {


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(this, 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        presenter.getData(isCash);

    }

    @Override
    public void showData(ArrayList<AdCount> list) {
        adapter.SetListData(list, false);
    }
}
