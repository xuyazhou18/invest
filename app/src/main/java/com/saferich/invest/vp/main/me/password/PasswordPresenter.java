package com.saferich.invest.vp.main.me.password;

import android.widget.EditText;

import com.saferich.invest.R;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class PasswordPresenter extends BasePresenter<IPasswordView> {

    @Inject
    public PasswordPresenter() {
    }

    public void changepassword(EditText oldPassword, EditText newPassword) {
        params.put("p_oldLoginPassword", oldPassword.getText().toString().trim());
        params.put("p_newLoginPassword", newPassword.getText().toString().trim());

        apiWrapper.updatePassWord(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Boolean issuccfull) -> {

            if (issuccfull) {
                view.succefull();
            }

        }));

    }

    public void changePaypassword(EditText oldPassword, EditText newPassword) {
        params.put("p_oldPayPassword", oldPassword.getText().toString().trim());
        params.put("p_newPayPassword", newPassword.getText().toString().trim());

        apiWrapper.updatePayPassword(context, params)
                .doOnSubscribe(() -> showDialogView(R.string.doing, false))
                .doOnTerminate(this::dismissDialog)
                .subscribe(newSubscriber((Boolean issuccfull) -> {

            if (issuccfull) {
                view.succefull();
            }

        }));
    }
}
