package com.saferich.invest.vp.main.home.deatils;

import android.app.Activity;

import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class InvestDeatilsPresenter extends BasePresenter<IInvestDeatilsView> {

    @Inject
    Activity activity;

    @Inject
    public InvestDeatilsPresenter() {
    }


    public void getdata() {

        apiWrapper.getOrderDetail(context, activity.getIntent().getStringExtra("id")).
                subscribe(newSubscriber((InvestInfo data) -> {
                    view.ShowData(data);
                }));
    }
}
