package com.saferich.invest.vp.main.discover.more.mediaList;

import android.app.Activity;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class MedialistPresenter extends BasePresenter<IMediaListView> {

    @Inject
    Activity activity;

    @Inject
    public MedialistPresenter() {
        articleList = new ArrayList<>();

    }


    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<Article> articleList;
    private int currentPage = 1;


    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
    }

    private void getData() {
        params.clear();


        params.put("p_page", currentPage);
        params.put("p_maxRowNum", "10");


        apiWrapper.getArticleList(activity, params).subscribe(newSubscriber((List<Article> list) -> {
            if (isloadMore) {
                if (articleList.size() > 0) {
                    articleList.addAll(list);
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                articleList.clear();

                articleList.addAll(list);


            }

            view.showListData((ArrayList<Article>) articleList);
        }));

    }

    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }
}
