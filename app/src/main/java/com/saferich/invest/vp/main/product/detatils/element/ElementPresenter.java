package com.saferich.invest.vp.main.product.detatils.element;

import com.saferich.invest.vp.base.BasePresenter;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-22
 */

public class ElementPresenter extends BasePresenter<IElementView> {

    @Inject
    public ElementPresenter() {
    }
}
