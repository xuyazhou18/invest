package com.saferich.invest.vp.main.home.myList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.Button;

import com.saferich.invest.R;
import com.saferich.invest.vp.adapter.MyFragmentAdapter;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的投资列表主界面
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public class MyInvestListActivity extends BaseActivity<MyInvestPresenter> implements IMyInvestListView,
        ViewPager.OnPageChangeListener {


    @BindView(R.id.product1)
    Button product1;
    @BindView(R.id.product5)
    Button product5;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    private HavingFragment havingFragment;
    private EndFragment endFragment;
    private List<Fragment> fragmentList;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    private MyFragmentAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_my_invest_list;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initFragment();
    }

    private void initFragment() {

        fragmentManager = getSupportFragmentManager();

        fragmentList = new ArrayList<>();

        havingFragment = new HavingFragment();
        endFragment = new EndFragment();

        fragmentList.add(havingFragment);
        fragmentList.add(endFragment);


        adapter = new MyFragmentAdapter(fragmentManager, fragmentList);
        viewpager.setAdapter(adapter);

        viewpager.setOffscreenPageLimit(1);
        viewpager.setCurrentItem(0);
        viewpager.addOnPageChangeListener(this);

    }


    @OnClick(R.id.product1)
    void productoneClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01_on);
        product1.setTextColor(getResources().getColor(R.color.colorPrimary));
        product5.setBackgroundResource(R.mipmap.top_bg04);
        product5.setTextColor(getResources().getColor(R.color.white));
        viewpager.setCurrentItem(0);

    }

    @OnClick(R.id.product5)
    void productFiveClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01);
        product1.setTextColor(getResources().getColor(R.color.white));

        product5.setBackgroundResource(R.mipmap.top_bg04_on);
        product5.setTextColor(getResources().getColor(R.color.colorPrimary));
        viewpager.setCurrentItem(1);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                productoneClick();
                break;
            case 1:
                productFiveClick();
                break;
            default:
                productoneClick();
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
