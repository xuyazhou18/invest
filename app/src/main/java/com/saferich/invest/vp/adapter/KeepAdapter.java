package com.saferich.invest.vp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.NetworkUtil;
import com.saferich.invest.common.Utils.ProductUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.model.bean.Product;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * 产品稳健列表
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class KeepAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<Product> productList;

    @Inject
    public KeepAdapter() {
        productList = new ArrayList<>();
    }


    public void SetListData(ArrayList<Product> productList) {

        this.productList = productList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_steady, parent, false);

        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.bindTo(productList.get(position));


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.yield_rate_number)
        TextView yieldRateNumber;
        @BindView(R.id.prsent)
        TextView prsent;
        @BindView(R.id.add_icon)
        TextView addIcon;
        @BindView(R.id.floatText)
        TextView floatText;
        @BindView(R.id.rete_layout)
        LinearLayout reteLayout;
        @BindView(R.id.days)
        TextView days;
        @BindView(R.id.day)
        TextView day;
        @BindView(R.id.start_price)
        TextView startPrice;
        @BindView(R.id.btn_invest)
        Button btnInvest;
        @BindView(R.id.product_icon)
        ImageView productIcon;
        @BindView(R.id.item_layout)
        LinearLayout itemLayout;


        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(Product product) {

            productName.setText(product.getProductName());
            price.setText(product.getMinInvestMoney() + "");

            days.setText(product.getInvestTerm() + "");


            if (product.getBuyingGroupsCode().equals("BuyingGroupsCode_NEW")) {
                productIcon.setVisibility(View.VISIBLE);
            } else {
                productIcon.setVisibility(View.GONE);
            }

            if (product.getProductProfitTypeCode().equals("FixedIncome")) {
                yieldRateNumber.setText(StringUtil.repaceE(product.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                floatText.setVisibility(View.GONE);


            }

            if (product.getProductProfitTypeCode().equals("FloatIncome")) {
                floatText.setText(product.getFloatProfit() + "");
                prsent.setVisibility(View.GONE);
                floatText.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.GONE);
                yieldRateNumber.setVisibility(View.GONE);
            }
            if (product.getProductProfitTypeCode().equals("Fixed&FloatIncome")) {
                floatText.setText(product.getFloatProfit() + "");
                yieldRateNumber.setText(StringUtil.repaceE(product.getFixedProfit()) + "");
                yieldRateNumber.setVisibility(View.VISIBLE);
                prsent.setVisibility(View.VISIBLE);
                addIcon.setVisibility(View.VISIBLE);
                floatText.setVisibility(View.VISIBLE);
            }


            if (product.getSaleState().equals("Finan_SellOut")) {
                btnInvest.setText("售罄");
                btnInvest.setBackgroundResource(R.drawable.base_button_sell);
            } else if
                    (product.getSaleState().equals("Finan_PreSale")) {
                btnInvest.setText("即将开放投资");
                btnInvest.setBackgroundResource(R.drawable.base_button_no);
            } else {
                btnInvest.setText("投资");
                btnInvest.setBackgroundResource(R.drawable.base_button);

            }


            RxView.clicks(btnInvest).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .filter(aVoid -> NetworkUtil.checkNet(btnInvest.getContext()))
                    .subscribe(aVoid -> {

                        if (product.getSaleState().equals("Finan_OnSale")) {
                            ProductUtils.checkLoginAndBind(product, btnInvest.getContext());
                        }

                    });

            RxView.clicks(itemLayout).throttleFirst(500, TimeUnit.MILLISECONDS)
                    .filter(aVoid -> NetworkUtil.checkNet(btnInvest.getContext()))
                    .subscribe(aVoid -> {

                        Intent intent = new Intent(productName.getContext(),
                                ProductDeatilsActivity.class);
                        intent.putExtra("id", product.getId());
                        intent.putExtra("productName", product.getProductName());

                        productName.getContext().startActivity(intent);

                    });

        }
    }


}
