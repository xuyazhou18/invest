package com.saferich.invest.vp.main.product.detatils;

import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-10
 */
public interface IDetealsView extends IBaseView {

    void backData(ProductInfo data);



}
