package com.saferich.invest.vp.main.product.detatils.element;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.vp.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 产品基本要素界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-21
 */

public class ElementFragment extends BaseFragment<ElementPresenter> implements IElementView {


    @BindView(R.id.product_title)
    TextView productTitle;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.line_layout)
    View lineLayout;
    @BindView(R.id.raise_count)
    TextView raiseCount;
    @BindView(R.id.rescursion)
    TextView rescursion;
    @BindView(R.id.rescursion_count)
    TextView rescursionCount;
    @BindView(R.id.year_profit)
    TextView yearProfit;
    @BindView(R.id.year_profit_count)
    TextView yearProfitCount;
    @BindView(R.id.rate_type)
    TextView rateType;
    @BindView(R.id.income_type)
    TextView incomeType;
    @BindView(R.id.income)
    TextView income;
    @BindView(R.id.comeback_type)
    TextView comebackType;
    @BindView(R.id.risk)
    TextView risk;
    @BindView(R.id.risk_type)
    TextView riskType;
    @BindView(R.id.rating_star)
    RatingBar ratingStar;
    @BindView(R.id.tobuy)
    TextView tobuy;
    @BindView(R.id.invest_count)
    TextView investCount;
    @BindView(R.id.manager)
    TextView manager;
    @BindView(R.id.manager_count)
    TextView managerCount;
    @BindView(R.id.line2)
    View line2;
    @BindView(R.id.buy_free)
    TextView buyFree;
    @BindView(R.id.button_del)
    ImageView buttonDel;
    @BindView(R.id.item_layout)
    CardView itemLayout;
    @BindView(R.id.buy_layout)
    LinearLayout buyLayout;
    @BindView(R.id.get_type)
    TextView getType;
    @BindView(R.id.profit_logic)
    TextView profitLogic;
    @BindView(R.id.back_type)
    TextView backType;
    private boolean isOpen;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_element, container,
                false);
    }

    public void setData(ProductInfo data) {

        buyFree.setText(data.getRemarkIllustration());

        productName.setText(data.getProductName());

        raiseCount.setText(StringUtil.repaceE(data.getProductScale()) + "元");
        rescursionCount.setText(StringUtil.repaceE(data.getPurchaseIncreaseMoney()) + "元");
        yearProfitCount.setText(TimeUtils.getFormatTime(data.getStartSaleDate(), "yyyy.MM.dd")
                + "-" + TimeUtils.getFormatTime(data.getEndSaleDate(), "yyyy.MM.dd"));
        getType.setText(data.getProductProfitTypeName());
        incomeType.setText(data.getProfitSettleMannerName());
        backType.setText(data.getCapitalRedeemRuleName());


        if (data.isSupportRedeemStatus()) {
            comebackType.setText("中途可赎回");
        } else {
            comebackType.setText("中途不可赎回");
        }

        riskType.setText(data.getRiskTypeName() + "型");
        investCount.setText(StringUtil.repaceE(data.getSubscriptionFee()) + "%");

        if (data.getRiskTypeName().equals("平衡")) {
            ratingStar.setRating(3);
        }
        if (data.getRiskTypeName().equals("稳健")) {
            ratingStar.setRating(4);
        }
        if (data.getRiskTypeName().equals("保守")) {
            ratingStar.setRating(5);
        }
        if (data.getRiskTypeName().equals("进取")) {
            ratingStar.setRating(2);
        }
        if (data.getManagementFee() == null) {
            managerCount.setText("待定");
        } else {
            managerCount.setText(StringUtil.repaceE(data.getManagementFee()) + "%");
        }


        if (data.getBeginProfitMannerCode().equals("BeginProfitManner_T&N")) {
            profitLogic.setText("T(" + data.getBeginProfitDateTypeName() + ")+" + data.getBeginProfitDay() + "(" + (data.getBeginBenefitDayTypeName()) + ")");
        } else {
            profitLogic.setText(TimeUtils.getFormatTime(data.getBeginProfitDate(), "yyyy/MM/dd"));
        }


    }

    @OnClick(R.id.button_del)
    public void cancleClick() {

        itemLayout.setVisibility(View.GONE);
        isOpen = false;


    }

    @OnClick(R.id.buy_layout)
    public void itemClick() {
        if (isOpen) {
            itemLayout.setVisibility(View.GONE);
            isOpen = false;
        } else {
            itemLayout.setVisibility(View.VISIBLE);
            isOpen = true;
        }

    }


}
