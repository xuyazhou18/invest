package com.saferich.invest.vp.main.home.deatils;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-27
 */

public interface IMessageView extends IBaseView{
    void showPdf(String url, String name);



}
