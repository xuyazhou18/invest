package com.saferich.invest.vp.main.me.balance;

import android.app.Activity;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.Balance;
import com.saferich.invest.vp.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-02
 */

public class BalancePresenter extends BasePresenter<IBalanceView> {

    @Inject
    Activity activity;

    @Inject
    public BalancePresenter() {
        balanceList = new ArrayList<>();

    }


    public boolean isloadMore() {
        return isloadMore;
    }

    private boolean isloadMore;
    private List<Balance> balanceList;
    private int currentPage = 1;


    public void refresh() {
        isloadMore = false;
        currentPage = 1;
        getData();
        getDataByToken();
    }

    private void getData() {
        params.clear();


        params.put("p_page", currentPage);
        params.put("p_maxRowNum", "10");


        apiWrapper.getBalanceFlow(activity, params).subscribe(newSubscriber((List<Balance> list) -> {
            if (isloadMore) {
                if (balanceList.size() > 0) {
                    balanceList.addAll(list);
                } else {
                    ShowToast.Short(context, "数据已经全部加载完毕");
                }
            } else {

                balanceList.clear();

                balanceList.addAll(list);


            }

            view.showData((ArrayList<Balance>) balanceList);
        }));

    }

    private void getDataByToken() {
        apiWrapper.getAccountMessage(context, userinfo.accessToken).subscribe(newSubscriber((Account data) -> {
            view.showHeadData(data);
            userinfo.isBindCard = data.isIfBindCard();
            userinfo.balance = data.getBalance();
            userinfo.totalMoney = data.getTotalMoney();
            userinfo.isSetPayWord = data.isIfSetPayPassword();
            userinfo.ifCardUsed = data.isIfCardUsed();
            userinfo.ifNewInvest = data.isIfNewInvest();

            userinfo.mobliePhone = data.getBindMobilePhone();
            userinfo.userLoginPhoneNumber = data.getMobile();
            if (data.isIfBindCard()) {
                userinfo.bankCard = data.getBankCard();
                userinfo.bankCode = data.getBankCode();
            }
            userinfo.update();
        }));
    }


    public void loadMore() {
        isloadMore = true;
        currentPage++;
        getData();
    }
}
