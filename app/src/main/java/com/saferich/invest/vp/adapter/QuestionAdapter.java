package com.saferich.invest.vp.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.main.discover.more.Help.deatils.QuestionDeatilsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 帮助中心列表适配器
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-16
 */
public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    private ArrayList<Question> questionlist;


    public void setListener(HeaderItemListener listener) {
        this.listener = listener;
    }

    private HeaderItemListener listener;

    public void setDataList(ArrayList<Question> questionlist) {

        this.questionlist = questionlist;
        notifyDataSetChanged();

    }

    @Inject
    public QuestionAdapter() {
        questionlist = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false);

            return new ItemViewHolder(view);
        }

        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_question, parent, false);
            return new HeaderViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.bindTo(questionlist.get(position - 1));


        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.bindTo(listener);
            headerViewHolder.setIsRecyclable(false);


        }
    }


    @Override
    public int getItemCount() {
        if (questionlist != null) {
            return questionlist.size() + 1;
        } else {
            return 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.item_layout)
        RelativeLayout itemLayout;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // 绑定数据
        @SuppressLint("SetTextI18n")
        public void bindTo(final Question item) {
            title.setText(item.getTitle());

            RxView.clicks(itemLayout).subscribe(aVoid -> {
                Intent intent = new Intent(itemLayout.getContext(), QuestionDeatilsActivity.class);
                intent.putExtra("id", item.getId());

                itemLayout.getContext().startActivity(intent);
            });


        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.member_layout)
        LinearLayout memberLayout;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.cash_layout)
        LinearLayout cashLayout;
        @BindView(R.id.invest_layout)
        LinearLayout investLayout;
        @BindView(R.id.payment_layout)
        LinearLayout paymentLayout;
        @BindView(R.id.discount_layout)
        LinearLayout discountLayout;
        @BindView(R.id.parsing_layout)
        LinearLayout parsingLayout;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindTo(final HeaderItemListener listener) {
            memberLayout.setOnClickListener(v -> listener.headItemClick(0));
            cashLayout.setOnClickListener(v -> listener.headItemClick(1));
            investLayout.setOnClickListener(v -> listener.headItemClick(2));
            paymentLayout.setOnClickListener(v -> listener.headItemClick(3));
            discountLayout.setOnClickListener(v -> listener.headItemClick(4));
            parsingLayout.setOnClickListener(v -> listener.headItemClick(5));
        }
    }

    public interface HeaderItemListener {
        void headItemClick(int type);
    }

}
