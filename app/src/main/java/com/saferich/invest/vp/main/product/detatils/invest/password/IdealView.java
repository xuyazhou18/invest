package com.saferich.invest.vp.main.product.detatils.invest.password;

import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public interface IdealView extends IBaseView {
    void authFinish(OrderInfo order);
}
