package com.saferich.invest.vp.main;

import android.Manifest;

import com.saferich.invest.common.Utils.FileUtil;
import com.saferich.invest.common.config.Constant;
import com.saferich.invest.model.event.ProductEvent;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;
import com.tbruyelle.rxpermissions.RxPermissions;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class MainPresenter extends BasePresenter<IMainView> implements IBaseView {

    @Inject
    public MainPresenter() {
        super();
    }

    public void init() {
        addSub();
    }

    private void addSub() {

        subscription.add(rxBus.subscribe(event -> {
                },
                ProductEvent.class));
    }


    public void Unsubscribe() {
        subscription.unsubscribe();
    }

    public void initPresmision() {
        RxPermissions.getInstance(context)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        FileUtil.CreateDir(Constant.DownLoadPath);
                    } else {
                        // Oups permission denied
                    }
                });


    }

}
