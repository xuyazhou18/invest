package com.saferich.invest.vp.main.discover.more.mediaList;

import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public interface IMediaListView extends IBaseView {
    void showListData(ArrayList<Article> questionlist);
}
