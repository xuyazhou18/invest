package com.saferich.invest.vp.main.discover.more.aboutApp;

import com.saferich.invest.vp.base.IBaseView;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public interface IAppView extends IBaseView {
    void showData();
}
