package com.saferich.invest.vp.main.product.detatils.invest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.common.Utils.TimeUtils;
import com.saferich.invest.vp.adapter.TrendAdapter;
import com.saferich.invest.common.widget.SpaceItemDecoration2;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.vp.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

/**
 * 产品投资界面
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class InvestSuccefulActivity extends BaseActivity<InvestSuceesPresenter> implements IInvestSuccessView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.buy_time)
    TextView buyTime;
    @BindView(R.id.create_time)
    TextView createTime;
    @BindView(R.id.invest_count)
    TextView investCount;
    @BindView(R.id.hot_activity)
    TextView hotActivity;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    TrendAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.invest_success_layout;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        centerTitle.setText("投资成功");
        productName.setText(getIntent().getStringExtra("productName"));
        createTime.setText(TimeUtils.getFormatTime(getIntent().getLongExtra("CreateTime", 0), "yyyy/MM/dd HH:mm:ss"));
        investCount.setText("￥" + getIntent().getStringExtra("investMoney") + "元");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration2(DensityUtil.dp2px(getContext(), 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        presenter.getdata();
    }

    @Override
    public void ShowData(ArrayList<AdCount> list) {
        adapter.SetListData(list, false);
    }
}
