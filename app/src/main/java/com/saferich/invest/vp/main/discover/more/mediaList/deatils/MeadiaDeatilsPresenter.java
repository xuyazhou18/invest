package com.saferich.invest.vp.main.discover.more.mediaList.deatils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.saferich.invest.model.bean.Article;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.common.WebDeatilsActivity;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class MeadiaDeatilsPresenter extends BasePresenter<IMediaDeailsView> {

    @Inject
    Activity activity;

    @Inject
    public MeadiaDeatilsPresenter() {
        super();
    }

    public void getdata() {


        apiWrapper.getArticleDetail(activity, activity.getIntent().getLongExtra("id", 0) + "")
                .subscribe(newSubscriber((Article data) -> {
                    view.showData(data);
                }));

    }

    public void setWebView(WebView webView, String content) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        webView.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(context,
                        WebDeatilsActivity.class);
                intent.putExtra("url", url);
                // intent.putExtra("title", deatails.getTitle());

                context.startActivity(intent);
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
                                                              String url) {
                WebResourceResponse response = null;
                response = super.shouldInterceptRequest(view, url);
                return response;
            }
        });
    }
}
