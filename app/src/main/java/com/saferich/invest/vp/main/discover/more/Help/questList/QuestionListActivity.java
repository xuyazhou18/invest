package com.saferich.invest.vp.main.discover.more.Help.questList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.DensityUtil;
import com.saferich.invest.vp.adapter.QuestionListAdapter;
import com.saferich.invest.common.recyclerview.EndlessRecyclerOnScrollListener;
import com.saferich.invest.common.widget.SpaceItemDecoration;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.discover.more.Help.IHelperView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 *
 * 问题列表界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class QuestionListActivity extends BaseActivity<QuestListPresenter> implements IHelperView {
    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.right_title)
    TextView rightTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ptr_frame)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.multiStateView)
    MultiStateView multiStateView;
    @Inject
    QuestionListAdapter adapter;

    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_question_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI() {
        switch (getIntent().getIntExtra("id", 0)) {
            case 0:
                centerTitle.setText("会员账户");
                break;
            case 1:
                centerTitle.setText("充值取现");
                break;
            case 2:
                centerTitle.setText("投资");
                break;
            case 3:
                centerTitle.setText("回款与收益");
                break;
            case 4:
                centerTitle.setText("我的优惠");
                break;
            case 5:
                centerTitle.setText("名词解释");
                break;
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new SpaceItemDecoration(DensityUtil.dp2px(this, 7)));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);

                presenter.loadMore();
            }
        });


        ptrFrame.setLoadingMinTime(1000);
        ptrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                                             View content, View header) {

                return PtrDefaultHandler.checkContentCanBePulledDown(frame,
                        recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {


                presenter.refresh();
            }
        });

        ptrFrame.postDelayed(() -> ptrFrame.autoRefresh(), 150);

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

    }

    @Override
    public void showListData(ArrayList<Question> questionlist) {
        if (presenter.isloadMore()) {

        } else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            ptrFrame.refreshComplete();

        }

        adapter.setDataList(questionlist);
    }
}
