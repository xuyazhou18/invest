package com.saferich.invest.vp.main.product.balance;

import android.support.v4.app.Fragment;

import com.saferich.invest.common.Utils.ShowToast;
import com.saferich.invest.model.bean.Product;
import com.saferich.invest.vp.base.BasePresenter;
import com.saferich.invest.vp.base.IBaseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-03
 */
public class BalancePresenter extends BasePresenter<IBalanceView> implements IBaseView {

    int page;
    @Inject
    Fragment fragment;

    private boolean isloadMore;
    ArrayList<Product> productList;

    @Inject
    public BalancePresenter() {
        super();
        productList = new ArrayList<>();
    }

    public void refresh() {
        page = 1;
        isloadMore = false;
        getdata();
    }

    private void getdata() {

        params.put("p_RiskTypeCode", "Balance");
        params.put("p_PageIndex", page);
        params.put("p_MaxRows", 10);

        apiWrapper.getProductList(fragment, params)
                .subscribe(newSubscriber((List<Product> list) -> {
                    if (isloadMore) {
                        if (list.size() > 0) {
                            productList.addAll(list);
                        } else {
                            ShowToast.Short(context, "数据已经全部加载完毕");
                        }
                    } else {
                        productList.clear();
                        if (list != null) {

                            productList.addAll(list);
                        }else {
                            List<Product> list1 = new ArrayList<>();

                            productList.addAll(list1);
                        }

                    }

                    view.showData(productList);
                }));
    }

    public void loadMore() {
        isloadMore = true;
        page++;
        getdata();
    }

    @Override
    protected void internetError() {
        super.internetError();
        view.error();
    }

    public boolean isloadMore() {
        return isloadMore;
    }
}
