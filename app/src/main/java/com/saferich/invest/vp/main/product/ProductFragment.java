package com.saferich.invest.vp.main.product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.saferich.invest.R;
import com.saferich.invest.vp.adapter.MyFragmentAdapter;
import com.saferich.invest.vp.base.BaseFragment;
import com.saferich.invest.vp.main.product.Progressive.ProgressiveFragment;
import com.saferich.invest.vp.main.product.balance.BalanceFragment;
import com.saferich.invest.vp.main.product.keeping.KeepFragment;
import com.saferich.invest.vp.main.product.recommend.RecommendFragment;
import com.saferich.invest.vp.main.product.steady.SteadyFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * 产品主界面
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class ProductFragment extends BaseFragment<ProductPresenter> implements IProductView,
        ViewPager.OnPageChangeListener {

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.product1)
    Button product1;
    @BindView(R.id.product2)
    Button product2;
    @BindView(R.id.product3)
    Button product3;
    @BindView(R.id.product4)
    Button product4;
    @BindView(R.id.product5)
    Button product5;
    private KeepFragment keepFragment;
    private SteadyFragment steadyFragment;
    private RecommendFragment recommendFragment;
    private BalanceFragment balanceFragment;
    private ProgressiveFragment progressiveFragment;
    private MyFragmentAdapter adapter;
    private List<Fragment> fragmentList;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void setupFragmentComponent() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_product, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initFragment();
        presenter.addSub();

    }

    private void initFragment() {

        fragmentManager = getChildFragmentManager();

        fragmentList = new ArrayList<>();

        recommendFragment = new RecommendFragment();
        keepFragment = new KeepFragment();
        steadyFragment = new SteadyFragment();
        balanceFragment = new BalanceFragment();
        progressiveFragment = new ProgressiveFragment();

        fragmentList.add(recommendFragment);
        fragmentList.add(keepFragment);
        fragmentList.add(steadyFragment);
        fragmentList.add(balanceFragment);
        fragmentList.add(progressiveFragment);


        adapter = new MyFragmentAdapter(fragmentManager, fragmentList);
        viewpager.setAdapter(adapter);

        viewpager.setOffscreenPageLimit(4);
        viewpager.setCurrentItem(0);
        viewpager.addOnPageChangeListener(this);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                productoneClick();
                break;
            case 1:
                producttwoClick();
                break;
            case 2:
                producthreeClick();
                break;
            case 3:
                productFourClick();
                break;
            case 4:
                productFiveClick();
                break;
            default:
                productoneClick();
                break;
        }

    }


    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @OnClick(R.id.product1)
    void productoneClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01_on);
        product1.setTextColor(getResources().getColor(R.color.colorPrimary));
        product2.setBackgroundResource(R.mipmap.top_bg02);
        product2.setTextColor(getResources().getColor(R.color.white));
        product3.setBackgroundResource(R.mipmap.top_bg02);
        product3.setTextColor(getResources().getColor(R.color.white));
        product4.setBackgroundResource(R.mipmap.top_bg02);
        product4.setTextColor(getResources().getColor(R.color.white));
        product5.setBackgroundResource(R.mipmap.top_bg04);
        product5.setTextColor(getResources().getColor(R.color.white));
        viewpager.setCurrentItem(0);

    }

    @OnClick(R.id.product2)
    void producttwoClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01);
        product1.setTextColor(getResources().getColor(R.color.white));
        product2.setBackgroundResource(R.mipmap.top_bg02_on);
        product2.setTextColor(getResources().getColor(R.color.colorPrimary));
        product3.setBackgroundResource(R.mipmap.top_bg02);
        product3.setTextColor(getResources().getColor(R.color.white));
        product4.setBackgroundResource(R.mipmap.top_bg02);
        product4.setTextColor(getResources().getColor(R.color.white));
        product5.setBackgroundResource(R.mipmap.top_bg04);
        product5.setTextColor(getResources().getColor(R.color.white));
        viewpager.setCurrentItem(1);

    }

    @OnClick(R.id.product3)
    void producthreeClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01);
        product1.setTextColor(getResources().getColor(R.color.white));
        product2.setBackgroundResource(R.mipmap.top_bg02);
        product2.setTextColor(getResources().getColor(R.color.white));
        product3.setBackgroundResource(R.mipmap.top_bg02_on);
        product3.setTextColor(getResources().getColor(R.color.colorPrimary));
        product4.setBackgroundResource(R.mipmap.top_bg02);
        product4.setTextColor(getResources().getColor(R.color.white));
        product5.setBackgroundResource(R.mipmap.top_bg04);
        product5.setTextColor(getResources().getColor(R.color.white));
        viewpager.setCurrentItem(2);

    }

    @OnClick(R.id.product4)
    void productFourClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01);
        product1.setTextColor(getResources().getColor(R.color.white));
        product2.setBackgroundResource(R.mipmap.top_bg02);
        product2.setTextColor(getResources().getColor(R.color.white));
        product3.setBackgroundResource(R.mipmap.top_bg02);
        product3.setTextColor(getResources().getColor(R.color.white));
        product5.setBackgroundResource(R.mipmap.top_bg04);
        product5.setTextColor(getResources().getColor(R.color.white));
        product4.setBackgroundResource(R.mipmap.top_bg02_on);
        product4.setTextColor(getResources().getColor(R.color.colorPrimary));
        viewpager.setCurrentItem(3);

    }

    @OnClick(R.id.product5)
    void productFiveClick() {
        product1.setBackgroundResource(R.mipmap.top_bg01);
        product1.setTextColor(getResources().getColor(R.color.white));
        product2.setBackgroundResource(R.mipmap.top_bg02);
        product2.setTextColor(getResources().getColor(R.color.white));
        product3.setBackgroundResource(R.mipmap.top_bg02);
        product3.setTextColor(getResources().getColor(R.color.white));
        product4.setBackgroundResource(R.mipmap.top_bg02);
        product4.setTextColor(getResources().getColor(R.color.white));
        product5.setBackgroundResource(R.mipmap.top_bg04_on);
        product5.setTextColor(getResources().getColor(R.color.colorPrimary));
        viewpager.setCurrentItem(4);

    }


    @Override
    public void refreshData() {
        if (recommendFragment != null) {
            recommendFragment.refreshData();
        }
        if (keepFragment != null) {
            keepFragment.refreshData();
        }
        if (steadyFragment != null) {
            steadyFragment.refreshData();
        }
        if (balanceFragment != null) {
            balanceFragment.refreshData();
        }


    }
}
