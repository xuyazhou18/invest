package com.saferich.invest.vp.main.me.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.saferich.invest.R;
import com.saferich.invest.common.Utils.ActivityUtil;
import com.saferich.invest.common.Utils.RxUtils;
import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.vp.base.BaseActivity;
import com.saferich.invest.vp.main.me.gesture.GestureActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * 用户注册
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-07
 */

public class RegisterActivity extends BaseActivity<RegsiterPresenter> implements IRegisterView {


    @BindView(R.id.center_title)
    TextView centerTitle;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.send_code)
    Button sendVerifCode;
    @BindView(R.id.clear_code)
    ImageView delIcon;
    @BindView(R.id.editText_code)
    EditText editTextCode;
    @BindView(R.id.cold_tips)
    TextView codeTips;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.del)
    ImageView del;
    @BindView(R.id.password_tips)
    TextView passwordTips;
    @BindView(R.id.ensure_password)
    EditText ensurePassword;
    @BindView(R.id.clear_name)
    ImageView clearName;
    @BindView(R.id.ensure_tips)
    TextView ensureTips;
    @BindView(R.id.btn_set)
    Button btnRegister;


    @Override
    protected void setupActivityComponent() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_set_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        centerTitle.setText("设置登录密码");
        doCountTime();
        verifCodeClick();
        RegisterClick();
        title.setText("本次操作需要信息确认,验证码已经发送到您的手机:" +
                StringUtil.replaceNumber(getIntent().getStringExtra("number")) + ",请注意查收");

        //  presenter.verifCode(getIntent().getStringExtra("number"));
        addTextWatch();
    }

    private void addTextWatch() {
        editTextCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                codeTips.setVisibility(View.GONE);
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordTips.setVisibility(View.GONE);
                ensureTips.setVisibility(View.GONE);
            }
        });
        ensurePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ensureTips.setVisibility(View.GONE);
            }
        });
    }


    public void verifCodeClick() {
        RxView.clicks(sendVerifCode)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .doOnCompleted(this::doCountTime)
                .subscribe(aVoid -> {
                            presenter.verifCode(getIntent().getStringExtra("number"));
                            doCountTime();
                        }
                );


    }

    public void RegisterClick() {
        RxView.clicks(btnRegister)
                .throttleFirst(500, TimeUnit.MILLISECONDS)

                .filter(aVoid -> {
                    if (!StringUtil.checkIsNotEmpty(editTextCode)) {
                        codeTips.setVisibility(View.VISIBLE);
                        return false;
                    } else {
                        return true;
                    }

                })

                .filter(aVoid -> {
                    if (!StringUtil.checkIsNotEmpty(password)) {
                        passwordTips.setVisibility(View.VISIBLE);
                        return false;
                    } else {
                        return true;
                    }

                })

//                .filter(aVoid -> {
//                    if (!StringUtil.passwordFormat2(password.getText().toString())) {
//                        passwordTips.setVisibility(View.VISIBLE);
//                        return false;
//                    } else {
//                        return true;
//                    }
//
//                })
                .filter(aVoid -> StringUtil.checkBigLenght(password, 6,
                        R.string.string_length)).filter(aVoid -> StringUtil.checkBigLenght(password, 6,
                R.string.string_length))
                .filter(aVoid -> StringUtil.checkIsNotEmpty(ensurePassword,
                        R.string.empty_new_password))
                .filter(aVoid -> StringUtil.checkBigLenght(ensurePassword, 6,
                        R.string.string_length))
                .filter(aVoid -> {
                    if (!StringUtil.checkIsEqual(password, ensurePassword)) {
                        ensureTips.setVisibility(View.VISIBLE);
                        return false;
                    } else {
                        return true;
                    }

                })
                .filter(aVoid -> StringUtil.isAlphabeticOrNumberic(password,
                        R.string.string_fakut))
                .subscribe(aVoid ->
                        presenter.register(editTextCode, password, getIntent().getStringExtra("number")));

    }

    @OnClick(R.id.clear_code)
    public void passwordDel() {
        editTextCode.setText("");
        codeTips.setVisibility(View.GONE);
    }

    @OnClick(R.id.del)
    public void codeDel() {
        password.setText("");
        passwordTips.setVisibility(View.GONE);
    }

    @OnClick(R.id.clear_name)
    public void ensureDel() {
        ensurePassword.setText("");
        ensureTips.setVisibility(View.GONE);
    }

    public void doCountTime() {

        RxUtils.Countdown(60).
                subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                        sendVerifCode.setText("获取验证码");
                        sendVerifCode.setBackgroundColor(getResources().getColor(R.color.material_red));
                        sendVerifCode.setEnabled(true);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        sendVerifCode.setEnabled(false);
                        sendVerifCode.setBackgroundColor(getResources().getColor(R.color.code_background));
                        sendVerifCode.setText("重新获取(" + integer + ")秒");
                    }
                });
    }

    @Override
    public void succefull() {
        this.finish();
        ActivityUtil.moveToActivity(this, GestureActivity.class);
    }
}
