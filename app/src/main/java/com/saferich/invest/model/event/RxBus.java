package com.saferich.invest.model.event;

import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * 利用Rxjava实现的一直事件总线控制器
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-15
 */

public class RxBus {


    // PublishSubject只会把在订阅发生的时间点之后来自原始Observable的数据发射给观察者
    private final Subject<Object, Object> rxBus = new SerializedSubject<>(PublishSubject.create());

    //发射订阅
    public void send(Object event) {
        rxBus.onNext(event);
    }


    //添加订阅
    public <T> Subscription subscribe(Action1<T> onNext, Class<T> type) {
        return rxBus.asObservable().ofType(type).subscribe(onNext);
    }

    //是否有当前这个观察者
    public boolean hasObservers() {
        return rxBus.hasObservers();
    }

}
