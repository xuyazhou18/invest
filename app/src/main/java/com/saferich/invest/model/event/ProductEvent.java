package com.saferich.invest.model.event;

/**
 * 产品的事件总线
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-03
 */
public class ProductEvent {
    public ProductEvent(int fragmentType) {
        this.fragmentType = fragmentType;
    }

    public int getFragmentType() {
        return fragmentType;
    }

    public void setFragmentType(int fragmentType) {
        this.fragmentType = fragmentType;
    }

    int fragmentType;
}
