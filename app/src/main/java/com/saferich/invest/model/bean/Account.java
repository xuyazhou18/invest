package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-06
 */

public class Account {


    /**
     * ifBindCard : true
     * balance : 0
     * ifSetPayPassword : true
     * ifCardUsed : false
     * totalMoney : 0
     * bankCard : 3063
     * mobile : 13554177577
     * bankCode : ICBC
     */

    private boolean ifBindCard;
    private Double balance;
    private boolean ifSetPayPassword;
    private boolean ifCardUsed;
    private boolean ifNewInvest;
    private Double totalMoney;
    private String bankCard;
    private String mobile;
    private String bankCode;
    private String bindMobilePhone;

    public boolean isIfNewInvest() {
        return ifNewInvest;
    }

    public void setIfNewInvest(boolean ifNewInvest) {
        this.ifNewInvest = ifNewInvest;
    }

    public String getBindMobilePhone() {
        return bindMobilePhone;
    }

    public void setBindMobilePhone(String bindMobilePhone) {
        this.bindMobilePhone = bindMobilePhone;
    }

    public boolean isIfBindCard() {
        return ifBindCard;
    }

    public void setIfBindCard(boolean ifBindCard) {
        this.ifBindCard = ifBindCard;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public boolean isIfSetPayPassword() {
        return ifSetPayPassword;
    }

    public void setIfSetPayPassword(boolean ifSetPayPassword) {
        this.ifSetPayPassword = ifSetPayPassword;
    }

    public boolean isIfCardUsed() {
        return ifCardUsed;
    }

    public void setIfCardUsed(boolean ifCardUsed) {
        this.ifCardUsed = ifCardUsed;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
