package com.saferich.invest.model.inject.components;


import com.saferich.invest.model.inject.models.ServiceModule;
import com.saferich.invest.model.inject.scope.ServiceScope;

import dagger.Component;


/**
 * service的注入声明
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-10
 */
@Component(
        dependencies = AppComponent.class,
        modules = ServiceModule.class
)
@ServiceScope
public interface ServiceComponent {
}
