package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public class GetCash {


    /**
     * balance : 98500
     * extractMoney : 1500
     * m_extractCashId” : TX0000000000000001
     */

    private double balance;
    private double extractMoney;
    private String m_extractCashId;

    public String getM_extractCashId() {
        return m_extractCashId;
    }

    public void setM_extractCashId(String m_extractCashId) {
        this.m_extractCashId = m_extractCashId;
    }



    public void setBalance(long balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getExtractMoney() {
        return extractMoney;
    }

    public void setExtractMoney(double extractMoney) {
        this.extractMoney = extractMoney;
    }

    public void setExtractMoney(long extractMoney) {
        this.extractMoney = extractMoney;
    }
}
