package com.saferich.invest.model.db;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;


/**
 * 用户本地配置表
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
@Table(database = AppDatabase.class)
public class UserConfig extends BaseModel {

    @PrimaryKey
    @Column
    public int configType;//配置类型

    @Column
    public boolean isLogin;//是否已经登录

    @Column
    public boolean isGuide;//是否已经引导

    @Column
    public boolean isRead;//消息是否已读
    @Column
    public boolean isGesture;//是否启动手势

    @Column
    public int anthType;//验证类型


    public boolean isGuide() {
        return isGuide;
    }

    public void setGuide(boolean guide) {
        isGuide = guide;
    }

    public int getConfigType() {
        return configType;
    }

    public void setConfigType(int configType) {
        this.configType = configType;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public int getAnthType() {
        return anthType;
    }

    public void setAnthType(int anthType) {
        this.anthType = anthType;
    }


}
