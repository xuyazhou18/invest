package com.saferich.invest.model.inject.models;


import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.saferich.invest.BuildConfig;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.api.ApiService;
import com.saferich.invest.model.api.ApiWrapper;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * api网络请求的一些注入模块
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-21
 */
@Module
public class ApiServiceModule {

    private static final String DEBUGENDPOINT = "http://192.168.0.36:8082/mobile-front/";
    private static final String RLEASEENDPOINT = "http://mobile.saferich.com:8082/mobile-front/";

    /**
     * retrofit 的创建
     *
     * @return retrofit的实例
     */
    @Provides
    @Singleton
    protected ApiService provideApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.DEBUG ? DEBUGENDPOINT : RLEASEENDPOINT)//根据编译类型选择测试或者正式服务器
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()) // 添加Rx适配器
                .addConverterFactory(GsonConverterFactory.create())//添加GSON自动解析
                .client(okHttpClient())
                .build();

        return retrofit.create(ApiService.class);
    }

    /**
     * okHttpClient 的创建
     *
     * @return
     */
    @Provides
    @Singleton
    protected OkHttpClient okHttpClient() {
        Userinfo userinfo = SQLite.select().from(Userinfo.class).querySingle();
        UserConfig config = SQLite.select().from(UserConfig.class).querySingle();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(BuildConfig.DEBUG ? Level.BODY : Level.BODY);//网络请求的打印

        Interceptor interceptor = chain -> {
            Request original = chain.request();

            Log.d("xyz", "getData------->token---->" + config.isLogin);
            Log.d("xyz", "getData------->token--->" + userinfo.accessToken);

            //自动在header上添加token,如果值为空,则header上不添加token
            if (config.isLogin && userinfo.accessToken != null) {
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("accessToken", userinfo.accessToken)
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            } else {
                Request.Builder requestBuilder = original.newBuilder()
                        .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }


        };

        return new OkHttpClient.Builder()
                .connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)//添加链接超时60S
                .readTimeout(60 * 1000, TimeUnit.MILLISECONDS)//添加读取超时60S
                .addInterceptor(logging)//添加上面的打印
                .addNetworkInterceptor(interceptor)//header上的一些截取处理
                .build();

    }

    //声明自己封装的一些网络api请求类
    @Provides
    protected ApiWrapper provideApiWrapper() {
        return new ApiWrapper(provideApiService());
    }
}
