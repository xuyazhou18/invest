package com.saferich.invest.model.bean;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public class InvestInfo {

    /**
     * productName : 最便宜3
     * investMoney : 100
     * totalProfit : 0
     * createTime : 1467375076000
     * beginprofitTime : 1468252800000
     * investTerm : 3
     * endTime : 1468512000000
     * totalInvestMoney : 100
     * perYearIncomeRate : null
     * profitSettleMannerName : 到期结转
     * capitalRedeemRuleName : 到期汇款
     * supportRedeemStatus : null
     * investTrackBeanList : [{"orderId":"DD0000000000000001","productId":"QDFina000031","memberId":"310000222","type":1,"typeName":"你好","createTime":1467378846000}]
     */

    private String productName;
    private Double investMoney;
    private Double totalProfit;
    private Double manageFee;
    private long createTime;
    private long beginprofitTime;
    private int investTerm;
    private long endTime;
    private double feeCount;
    private double realInvestMoney;
    private double realReturnInvest;
    private double totalInvestMoney;
    private double perYearIncomeRate;
    private String profitSettleMannerName;
    private String capitalRedeemRuleName;
    private Boolean supportRedeemStatus;

    public double getRealReturnInvest() {
        return realReturnInvest;
    }

    public void setRealReturnInvest(double realReturnInvest) {
        this.realReturnInvest = realReturnInvest;
    }

    public Double getManageFee() {
        return manageFee;
    }

    public void setManageFee(Double manageFee) {
        this.manageFee = manageFee;
    }

    public List<CustomerReportBeanList> getCustomerReportBeanList() {
        return customerReportBeanList;
    }


    private List<InvestTrackBeanList> investTrackBeanList;
    private List<CustomerReportBeanList> customerReportBeanList;
    private List<ProductInfo.ProtocalList> productProtocalBeanList;

    public List<ProductInfo.ProtocalList> getProductProtocalBeanList() {
        return productProtocalBeanList;
    }

    public void setProductProtocalBeanList(List<ProductInfo.ProtocalList> productProtocalBeanList) {
        this.productProtocalBeanList = productProtocalBeanList;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getBeginprofitTime() {
        return beginprofitTime;
    }

    public void setBeginprofitTime(long beginprofitTime) {
        this.beginprofitTime = beginprofitTime;
    }

    public int getInvestTerm() {
        return investTerm;
    }

    public void setInvestTerm(int investTerm) {
        this.investTerm = investTerm;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double getFeeCount() {
        return feeCount;
    }

    public void setFeeCount(double feeCount) {
        this.feeCount = feeCount;
    }

    public double getRealInvestMoney() {
        return realInvestMoney;
    }

    public void setRealInvestMoney(double realInvestMoney) {
        this.realInvestMoney = realInvestMoney;
    }

    public Double getInvestMoney() {
        return investMoney;
    }

    public void setInvestMoney(Double investMoney) {
        this.investMoney = investMoney;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public double getTotalInvestMoney() {
        return totalInvestMoney;
    }

    public void setTotalInvestMoney(double totalInvestMoney) {
        this.totalInvestMoney = totalInvestMoney;
    }

    public void setTotalInvestMoney(int totalInvestMoney) {
        this.totalInvestMoney = totalInvestMoney;
    }

    public double getPerYearIncomeRate() {
        return perYearIncomeRate;
    }

    public void setPerYearIncomeRate(double perYearIncomeRate) {
        this.perYearIncomeRate = perYearIncomeRate;
    }

    public String getProfitSettleMannerName() {
        return profitSettleMannerName;
    }

    public void setProfitSettleMannerName(String profitSettleMannerName) {
        this.profitSettleMannerName = profitSettleMannerName;
    }

    public String getCapitalRedeemRuleName() {
        return capitalRedeemRuleName;
    }

    public void setCapitalRedeemRuleName(String capitalRedeemRuleName) {
        this.capitalRedeemRuleName = capitalRedeemRuleName;
    }

    public Boolean getSupportRedeemStatus() {
        return supportRedeemStatus;
    }

    public void setSupportRedeemStatus(Boolean supportRedeemStatus) {
        this.supportRedeemStatus = supportRedeemStatus;
    }

    public void setCustomerReportBeanList(List<CustomerReportBeanList> customerReportBeanList) {
        this.customerReportBeanList = customerReportBeanList;
    }

    public List<InvestTrackBeanList> getInvestTrackBeanList() {
        return investTrackBeanList;
    }

    public void setInvestTrackBeanList(List<InvestTrackBeanList> investTrackBeanList) {
        this.investTrackBeanList = investTrackBeanList;
    }

    public static class InvestTrackBeanList {
        private String orderId;
        private String productId;
        private String memberId;
        private int type;
        private String typeName;
        private long createTime;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }
    }

    public class CustomerReportBeanList {
        private String id;
        private String fileName;
        private String filePath;
        private Long createTime;

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public String getId() {
            return id;
        }

        public String getFileName() {
            return fileName;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }

    private class ProductProtocalBeanList {

        /**
         * protocalNo : 0003
         * state : 1
         * name : 产品协议1
         * foregroundDisplayName : 产品协议11
         * protocalType : 1
         * createTime : 1467272632000
         * createUser : Admin
         * updateTime : 1467374915000
         * updateUser : Admin
         * productProtocal : Protocol/0003/110031_201401.pdf
         */

        private String protocalNo;
        private String state;
        private String name;
        private String foregroundDisplayName;
        private String protocalType;
        private long createTime;
        private String createUser;
        private long updateTime;
        private String updateUser;
        private String productProtocal;

        public String getProtocalNo() {
            return protocalNo;
        }

        public void setProtocalNo(String protocalNo) {
            this.protocalNo = protocalNo;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getForegroundDisplayName() {
            return foregroundDisplayName;
        }

        public void setForegroundDisplayName(String foregroundDisplayName) {
            this.foregroundDisplayName = foregroundDisplayName;
        }

        public String getProtocalType() {
            return protocalType;
        }

        public void setProtocalType(String protocalType) {
            this.protocalType = protocalType;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public String getProductProtocal() {
            return productProtocal;
        }

        public void setProductProtocal(String productProtocal) {
            this.productProtocal = productProtocal;
        }
    }
}
