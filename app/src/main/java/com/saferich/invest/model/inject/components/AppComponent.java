package com.saferich.invest.model.inject.components;


import com.saferich.invest.MyApplication;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.inject.models.ApiServiceModule;
import com.saferich.invest.model.inject.models.AppModule;
import com.saferich.invest.model.api.ApiService;
import com.saferich.invest.model.api.ApiWrapper;
import com.saferich.invest.model.cache.CacheManager;
import com.saferich.invest.model.event.RxBus;

import javax.inject.Singleton;

import dagger.Component;
import rx.subscriptions.CompositeSubscription;

/**
 * app全局的一些注入
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
@Component(
        modules = {
                //依赖的一些模块
                AppModule.class,
                ApiServiceModule.class
        }
)

@Singleton
public interface AppComponent {

    MyApplication inject(MyApplication app);

    ApiService apiService();

    MyApplication getApplication();

    RxBus getRxbus();

    CompositeSubscription getSubscription();

    ApiWrapper getApiWrapper();

    Userinfo getUserInfo();

    UserConfig getConfig();

    CacheManager getCacheManager();
}
