package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-24
 */

public class TabItem {

    /**
     * constKey : 79
     * typeCode : RiskType
     * cnName : 保守
     * value : null
     * cashcode : Keep
     * remark : 风险类型
     * state : Y
     * createUser : loginuser
     * createTime : 1465888405000
     * updateUser :
     * updateTime : null
     */

    private int constKey;
    private String typeCode;
    private String cnName;
    private String value;
    private String code;
    private String remark;
    private String state;
    private String createUser;
    private long createTime;
    private String updateUser;
    private String updateTime;

    public int getConstKey() {
        return constKey;
    }

    public void setConstKey(int constKey) {
        this.constKey = constKey;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
