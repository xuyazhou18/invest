package com.saferich.invest.model.cache;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.saferich.invest.model.db.JsonCache;
import com.saferich.invest.model.db.JsonCache_Table;

import java.util.List;
import java.util.Map;


/**
 * 缓存控制器
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-11
 */
public class CacheManager {


    public CacheManager() {
    }


    //从数据库获取数据
    public <T> T getCache(String jsonType, Class<T> type) {

        JsonCache jsonCache = new Select().from(JsonCache.class).where(JsonCache_Table.
                dataType.eq(jsonType)).querySingle();
        if (jsonCache != null) {
            return new Gson().fromJson(jsonCache.data, type);
        }
        return null;
    } //从数据库获取数据

    public Map<String, List<Object>> getBankCache(String jsonType) {

        JsonCache jsonCache = new Select().from(JsonCache.class).where(JsonCache_Table.
                dataType.eq(jsonType)).querySingle();

        if (jsonCache != null) {
            return new Gson().fromJson(jsonCache.data, Map.class);
        }
        return null;
    }


    // 插入缓存
    public void insertChache(String data, String jsonType) {

        JsonCache jsonCache = new Select().from(JsonCache.class).where(JsonCache_Table.
                dataType.eq(jsonType)).querySingle();

        if (jsonCache != null) {
            jsonCache.delete();
        }
        JsonCache NewjsonCache = new JsonCache();
        NewjsonCache.dataType = jsonType;
        NewjsonCache.data = data;
        NewjsonCache.createTime = System.currentTimeMillis() + "";
        NewjsonCache.save();

    }  // 插入缓存


}
