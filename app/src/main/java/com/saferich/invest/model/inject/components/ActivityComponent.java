package com.saferich.invest.model.inject.components;


import com.saferich.invest.model.inject.models.ActivityModule;
import com.saferich.invest.model.inject.scope.ActivityScope;
import com.saferich.invest.vp.common.WatchPdfActivity;
import com.saferich.invest.vp.common.WebDeatilsActivity;
import com.saferich.invest.vp.common.WebDeatilsTxtActivity;
import com.saferich.invest.vp.main.MainActivity;
import com.saferich.invest.vp.main.discover.more.Help.HelpeActivity;
import com.saferich.invest.vp.main.discover.more.Help.deatils.QuestionDeatilsActivity;
import com.saferich.invest.vp.main.discover.more.Help.questList.QuestionListActivity;
import com.saferich.invest.vp.main.discover.more.MoreActivity;
import com.saferich.invest.vp.main.discover.more.aboutApp.AboutAppActivity;
import com.saferich.invest.vp.main.discover.more.aboutCompany.AboutCompanyActivity;
import com.saferich.invest.vp.main.discover.more.feedback.FeedbackActivity;
import com.saferich.invest.vp.main.discover.more.mediaList.MediaListActivity;
import com.saferich.invest.vp.main.discover.more.mediaList.deatils.MeadiaDeatilsActivity;
import com.saferich.invest.vp.main.discover.more.update.UpdateDialogActivity;
import com.saferich.invest.vp.main.discover.more.wechat.WeixinActivity;
import com.saferich.invest.vp.main.discover.noticetrend.NoticeTrendsActivity;
import com.saferich.invest.vp.main.home.deatils.InvestDeatilsActivity;
import com.saferich.invest.vp.main.home.list.InvestListActivity;
import com.saferich.invest.vp.main.home.myList.MyInvestListActivity;
import com.saferich.invest.vp.main.me.balance.BalanceActivity;
import com.saferich.invest.vp.main.me.bank.BankInputPhoneNumberActivity;
import com.saferich.invest.vp.main.me.bank.BankListActivity;
import com.saferich.invest.vp.main.me.bank.BindBankActivity;
import com.saferich.invest.vp.main.me.bank.WatchBankActivity;
import com.saferich.invest.vp.main.me.gesture.GestureActivity;
import com.saferich.invest.vp.main.me.getCash.GetCashActivity;
import com.saferich.invest.vp.main.me.login.InputPhoneNumberActivity;
import com.saferich.invest.vp.main.me.login.LoginActivity;
import com.saferich.invest.vp.main.me.msg.MsgTrendsActivity;
import com.saferich.invest.vp.main.me.password.ChangePasswordActivity;
import com.saferich.invest.vp.main.me.password.ChangePayPasswordActivity;
import com.saferich.invest.vp.main.me.password.ForgetPassWordActivity;
import com.saferich.invest.vp.main.me.password.SetPayPassWordActivity;
import com.saferich.invest.vp.main.me.phone.InputVerCodeActivity;
import com.saferich.invest.vp.main.me.rechare.RechargeActivity;
import com.saferich.invest.vp.main.me.register.RegisterActivity;
import com.saferich.invest.vp.main.me.result.BalanceResultActivity;
import com.saferich.invest.vp.main.me.userinfo.UserinfoActivity;
import com.saferich.invest.vp.main.product.detatils.ProductDeatilsActivity;
import com.saferich.invest.vp.main.product.detatils.invest.BankPhonePayActivity;
import com.saferich.invest.vp.main.product.detatils.invest.InvestActivity;
import com.saferich.invest.vp.main.product.detatils.invest.InvestSuccefulActivity;
import com.saferich.invest.vp.main.product.detatils.invest.password.DealDialogActivity;
import com.saferich.invest.vp.main.product.detatils.phone.PhoneDialogActivity;
import com.saferich.invest.vp.main.product.dialog.BindCardDialog;
import com.saferich.invest.vp.main.product.dialog.EnsurePayPwDialog;
import com.saferich.invest.vp.main.product.dialog.RetryEnsurePayPwDialog;
import com.saferich.invest.vp.wellcome.WellcomeActivity;

import dagger.Component;


/**
 * 全部的activity 注入声明
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
@Component(
        //所依赖的一些模块
        dependencies = AppComponent.class,
        modules = ActivityModule.class
)

@ActivityScope
public interface ActivityComponent {

    void inject(MainActivity activity);


    void inject(ProductDeatilsActivity productDeatilsActivity);

    void inject(PhoneDialogActivity phoneDialogActivity);

    void inject(InvestActivity investActivity);

    void inject(DealDialogActivity dealDialogActivity);

    void inject(NoticeTrendsActivity noticeTrendsActivity);

    void inject(WebDeatilsActivity webDeatilsActivity);

    void inject(MoreActivity moreActivity);

    void inject(HelpeActivity helpeActivity);

    void inject(QuestionDeatilsActivity questionDeatilsActivity);

    void inject(QuestionListActivity questionListActivity);

    void inject(FeedbackActivity feedbackActivity);

    void inject(AboutAppActivity aboutAppActivity);

    void inject(WebDeatilsTxtActivity webDeatilsTxtActivity);

    void inject(AboutCompanyActivity aboutCompanyActivity);

    void inject(MediaListActivity mediaListActivity);

    void inject(WeixinActivity wechatActivity);

    void inject(UpdateDialogActivity updateDialogActivity);

    void inject(GestureActivity guestureActivity);

    void inject(BalanceActivity balanceActivity);

    void inject(GetCashActivity getCashActivity);

    void inject(RechargeActivity rechargeActivity);

    void inject(InputPhoneNumberActivity inputPhoneNumberActivity);

    void inject(LoginActivity loginActivity);

    void inject(RegisterActivity registerActivity);


    void inject(ForgetPassWordActivity forgetPassWordActivity);

    void inject(ChangePasswordActivity changePasswordActivity);

    void inject(SetPayPassWordActivity setPayPassWordActivity);

    void inject(ChangePayPasswordActivity changePayPasswordActivity);

    void inject(UserinfoActivity userinfoActivity);

    void inject(BindBankActivity bindBankActivity);

    void inject(BankListActivity bankListActivity);

    void inject(BankInputPhoneNumberActivity bankInputPhoneNumberActivity);

    void inject(MsgTrendsActivity msgTrendsActivity);

    void inject(WatchBankActivity watchBankActivity);

    void inject(MeadiaDeatilsActivity meadiaDeatilsActivity);

    void inject(BindCardDialog bindCardDialog);

    void inject(BankPhonePayActivity bankPhoneNumberActivity);

    void inject(RetryEnsurePayPwDialog retryEnsurePayPwDialog);

    void inject(InvestSuccefulActivity investSuccefulActivity);

    void inject(EnsurePayPwDialog ensurePayPwDialog);

    void inject(InvestListActivity investListActivity);

    void inject(MyInvestListActivity myInvestListActivity);

    void inject(InvestDeatilsActivity investDeatilsActivity);

    void inject(InputVerCodeActivity inputVerCodeActivity);

    void inject(BalanceResultActivity balanceResultActivity);

    void inject(WellcomeActivity wellcomeActivity);

    void inject(WatchPdfActivity watchPdfActivity);
}
