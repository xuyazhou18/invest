package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-30
 */

public class OrderInfo {


    /**
     * orderId : DD0000000000000070
     * productName : 0701保守1
     * createDate : 1467620143000
     * investMoney : 1.0
     */

    private String orderId;
    private String productName;
    private long createDate;
    private double investMoney;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public double getInvestMoney() {
        return investMoney;
    }

    public void setInvestMoney(double investMoney) {
        this.investMoney = investMoney;
    }
}
