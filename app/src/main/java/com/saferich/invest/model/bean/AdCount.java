package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public class AdCount {


    /**
     * id : 5152
     * pic : saferich-test.oss-cn-shanghai.aliyuncs.com/Content/images/Advert/圣诞节发来看就按了就卡机富利卡技术点就就分类空间答复啊水电费阿斯蒂芬asd发收发的发刷单.jpeg
     * altContent : 广告4
     * forwardUrl : www.baidu.com
     * sort : 56
     * createUserId : SR9018
     * createUserName : null
     * updateUserId : SR9018
     * updateUserName : null
     * positionId : 1
     */

    private int id;
    private String pic;
    private String altContent;
    private String forwardUrl;
    private int sort;
    private String createUserId;
    private Object createUserName;
    private String updateUserId;
    private Object updateUserName;
    private int positionId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAltContent() {
        return altContent;
    }

    public void setAltContent(String altContent) {
        this.altContent = altContent;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Object getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(Object createUserName) {
        this.createUserName = createUserName;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Object getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(Object updateUserName) {
        this.updateUserName = updateUserName;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }
}
