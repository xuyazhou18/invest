package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-31
 */

public class Question {


    /**
     * id : 46
     * categoryId : 9
     * categoryName : null
     * ifCommon : 0
     * title : 123131231231231
     * status : 0
     * updateUserId : SR9018
     * updateUserName : null
     * updateTime : 1464664757000
     * content : 2222222222
     */

    private int id;
    private int categoryId;
    private Object categoryName;
    private String ifCommon;
    private String title;
    private int status;
    private String updateUserId;
    private Object updateUserName;
    private long updateTime;
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Object getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(Object categoryName) {
        this.categoryName = categoryName;
    }

    public String getIfCommon() {
        return ifCommon;
    }

    public void setIfCommon(String ifCommon) {
        this.ifCommon = ifCommon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Object getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(Object updateUserName) {
        this.updateUserName = updateUserName;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
