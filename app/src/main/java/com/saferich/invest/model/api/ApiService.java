package com.saferich.invest.model.api;

import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.model.bean.Balance;
import com.saferich.invest.model.bean.Bank;
import com.saferich.invest.model.bean.BankItem;
import com.saferich.invest.model.bean.CardRecharge;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.model.bean.GetCash;
import com.saferich.invest.model.bean.Invest;
import com.saferich.invest.model.bean.InvestBody;
import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.model.bean.LoginBean;
import com.saferich.invest.model.bean.MsgBean;
import com.saferich.invest.model.bean.MyInvest;
import com.saferich.invest.model.bean.Notice;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.bean.Product;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.model.bean.RechargeBody;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.model.bean.Response;
import com.saferich.invest.model.bean.UserMessage;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * 所有app api的注入声明
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-02-23
 */
public interface ApiService {

    @GET("notice/getNotices")
    Observable<Response<List<Notice>>> getNotice(@QueryMap Map<String, Object> map);

    @GET("question/getCommonQuestionList")
    Observable<Response<List<Question>>> getQuestionList(@QueryMap Map<String, Object> map);

    @GET("question/getQuestionListByCategoryId")
    Observable<Response<List<Question>>> getQuestionListByid(@QueryMap Map<String, Object> map);

    @GET("article/getArticles")
    Observable<Response<List<Article>>> getArticleList(@QueryMap Map<String, Object> map);

    @GET("member/getBankNameList")
    Observable<Response<List<BankItem>>> getBankNameList();

    @GET("article/getArticleDetail/{p_articleId}")
    Observable<Response<Article>> getArticleDetail(@Path("p_articleId") String id);

    @GET("question/getQuestion/{p_id}")
    Observable<Response<Question>> getQuestionByid(@Path("p_id") long id);

    @GET("platformInfo/getUserGuarantee/getAgreement")
    Observable<Response<String>> getAgreement();

    @GET("platformInfo/getUserGuarantee/getSafeGuarantee")
    Observable<Response<String>> getsafe();

    @GET("platformInfo/getCompanyInfo/getDescrib")
    Observable<Response<String>> getDes();

    @GET("platformInfo/getCompanyInfo/getQualified")
    Observable<Response<String>> getQualified();

    @GET("platformInfo/getCompanyInfo/getJoinUs")
    Observable<Response<String>> getJoinUs();

    @GET("account/getAccountMessage")
    Observable<Response<Account>> getAccountMessage();

    @GET("account/getAccountMessage")
    Observable<Response<Account>> getAccountMessage2(@Header("accessToken") String token);


    @FormUrlEncoded
    @POST("feedBack/addFeedBack")
    Observable<Response<Object>> addFeedBack(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("mcenter/phone/regist")
    Observable<Response<Object>> getRegistCode(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("mcenter/regist")
    Observable<Response<Object>> register(@FieldMap Map<String, Object> map);

    @POST("mcenter/link")
    Observable<Response<LoginBean>> getNonce();

    @FormUrlEncoded
    @POST("mcenter/login")
    Observable<Response<Userinfo>> login(@Header("randomToken") String token, @FieldMap Map<String, Object> map);

    @POST("mcenter/logout")
    Observable<Response<Object>> logout();

    @FormUrlEncoded
    @POST("account/bindBankCard")
    Observable<Response<Object>> bindBankCard(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("account/updateBindBankCard")
    Observable<Response<Object>> updateBindBankCard(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("member/updateMemberLoginPassword")
    Observable<Response<Boolean>> updatePassWord(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("member/setPayPassword")
    Observable<Response<Boolean>> setPayPassword(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("member/updatePayPassword")
    Observable<Response<Boolean>> updatePayPassword(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("mcenter/changemimaByVCode")
    Observable<Response<Boolean>> setPassword(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("member/phoneVerificationCode")
    Observable<Response<Object>> phoneVerificationCode(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("mcenter/existPhone")
    Observable<Response<Boolean>> existPhone(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("mcenter/phoneVCode")
    Observable<Response<Object>> phoneVCode(@FieldMap Map<String, Object> map);

    @POST("account/sendVCodeForBindCard")
    Observable<Response<String>> sendVCodeForBindCard();

    @GET("account/ifBindBankCard")
    Observable<Response<Boolean>> ifBindBankCard();

    @POST("account/getAccountInfo")
    Observable<Response<UserMessage>> getAccountInfo();

    @GET("account/getBindCardDetail")
    Observable<Response<Bank>> getBindCardDetail();

    @FormUrlEncoded
    @POST("account/getBankLimitByCodeAndChannel")
    Observable<Response<RechargeBean>> getBankLimitByCodeAndChannel(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("extractCash/addExtractCash")
    Observable<Response<GetCash>> addExtractCash(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("recharge/confirmPay")
    Observable<Response<String>> confirmPay(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("order/confirmPay")
    Observable<Response<OrderInfo>> ordConfirmPay(@FieldMap Map<String, Object> map);

    @POST("extractCash/phoneVCode")
    Observable<Response<Object>> addExtractCashphoneVCode();

    @POST("recharge/addRecharge")
    Observable<Response<CardRecharge>> addRecharge(@Body RechargeBody body);

    @FormUrlEncoded
    @POST("recharge/sendVerificationCode")
    Observable<Response<String>> rechargesendVerificationCode(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @POST("order/sendVerificationCode")
    Observable<Response<String>> OrdsendVerificationCode(@FieldMap Map<String, Object> map);


    @GET("account/getBankLimitListByChannel")
    Observable<Response<Account>> getBankLimitListByChannel(@QueryMap Map<String, Object> map);

    @GET("product/get/appList")
    Observable<Response<List<Product>>> getProductList(@QueryMap Map<String, Object> map);

    @GET("account/getBalanceFlow")
    Observable<Response<List<Balance>>> getBalanceFlow(@QueryMap Map<String, Object> map);

    @GET("product/get/appInfo/{p_ChannelProductNo}")
    Observable<Response<ProductInfo>> getProductInfo(@Path("p_ChannelProductNo") String id);

    @GET("notice/getStatus")
    Observable<Response<String>> getUpdate();

    @GET("account/getExtractCashParameterList")
    Observable<Response<List<CashItem>>> getExtractCashParameterList();

    @GET("product/get/RecommendList")
    Observable<Response<List<RecommondItem>>> getRecommondList(@QueryMap Map<String, Object> map);

    @GET("order/getFundFlow")
    Observable<Response<List<Invest>>> getFundFlow(@QueryMap Map<String, Object> map);

    @GET("order/getUncompleteOrder")
    Observable<Response<MyInvest>> getUncompleteOrder();

    @GET("order/getCompleteOrder")
    Observable<Response<List<Complete>>> getCompleteOrder();

    @GET("order/getOrderDetail/{p_orderId}")
    Observable<Response<InvestInfo>> getOrderDetail(@Path("p_orderId") String id);

    @GET("Adcontent/getAdContents/{p_sceneId}")
    Observable<Response<List<AdCount>>> getAdContents(@Path("p_sceneId") String id);

    @GET("order/getOrderInfo")
    Observable<Response<ProfitBean>> getOrderInfo();

    @GET("message/getSystemMessages")
    Observable<Response<MsgBean>> getSystemMessages(@QueryMap Map<String, Object> map);

    @GET("message/readMessage")
    Observable<Response<Object>> readMessage(@QueryMap Map<String, Object> map);

    @POST("message/allReadMessage")
    Observable<Response<Object>> allReadMessage();

    @GET("order/getOrderInfo")
    Observable<Response<ProfitBean>> getOrderInfo2(@Header("accessToken") String token);

    @POST("order/save")
    Observable<Response<OrderInfo>> addOrder(@Body InvestBody body);


}
