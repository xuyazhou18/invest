package com.saferich.invest.model.api;


import com.saferich.invest.model.bean.Response;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.trello.rxlifecycle.components.support.RxFragment;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 网络获得数据后的再一次切割封装
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-28
 */
public class RetrofitUtil {

    /**
     * 对网络接口返回的Response进行分割操作
     *
     * @param response
     * @param <T>
     * @return
     */
    public <T> Observable<T> flatResponse(final Response<T> response) {
        return Observable.create(subscriber -> {


//            if (response.isTokenExpired() || response.isTokenExpired2()) {
//                subscriber.onError(new TokenException());
//            }

            if (response.success) {

                subscriber.onNext(response.data);

            } else if (response.isTokenExpired() || response.isTokenExpired2()) {
                subscriber.onError(new TokenException(response.errCode, response.message));

            } else if (response.isTokenExpired3()) {
                subscriber.onError(new TokenNullException(response.errCode, response.message));

            } else if (response.isBindCardFail()) {
                subscriber.onError(new BindCardFail());

            } else if (response.isPayWordFail()) {
                subscriber.onError(new PayWordFail());

            } else {
                subscriber.onError(new APIException(response.errCode, response.message));
            }

            subscriber.onCompleted();


        });


    }

    /**
     * 自定义异常
     */
    public static class APIException extends Exception {
        public String getCode() {
            return code;
        }

        public String code;
        public String message;

        public APIException(String status, String message) {
            this.code = status;
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    //token过期情况
    public static class TokenException extends Exception {

        public TokenException(String status, String message) {
            this.code = status;
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }

        public String code;
        public String message;

    }

    public static class TokenNullException extends Exception {

        public TokenNullException(String status, String message) {
            this.code = status;
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }

        public String code;
        public String message;

    } //用户token异常情况


    public static class BindCardFail extends Exception {

        public BindCardFail() {

        }

    }

    public static class PayWordFail extends Exception {

        public PayWordFail() {

        }

    }

    /**
     * http://www.jianshu.com/p/e9e03194199e
     * <p>
     * Transformer实际上就是一个Func1<Observable<T>, Observable<R>>，
     * 换言之就是：可以通过它将一种类型的Observable转换成另一种类型的Observable，
     * 和调用一系列的内联操作符是一模一样的。
     * 利用RxLifeCyle自动管理observe生命周期
     *
     * @param <T>
     * @return
     */
    protected <T> Observable.Transformer<T, T> applySchedulersForActivity(RxAppCompatActivity context) {

        return observable -> (observable).subscribeOn(Schedulers.newThread())//网络请求创建新线程
                .observeOn(AndroidSchedulers.mainThread())//获得回调后在主线程处理
                .compose(context.bindToLifecycle())//利用RxLifeCyle自动管理observe生命周期
                .flatMap((Func1) response ->
                        flatResponse((Response<Object>) response));
    }

    protected <T> Observable.Transformer<T, T> applySchedulersForFragment(RxFragment context) {

        return observable -> (observable).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(context.bindToLifecycle())
                .flatMap((Func1) response ->
                        flatResponse((Response<Object>) response));
    }


}
