package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-05
 */
public class Product {


    /**
     * id : QDFina000093
     * productName : wxt测试！勿动002
     * minInvestMoney : 2
     * expectedProfitRateRange : null
     * fixedProfit : 2
     * DoubleProfit : null
     * investTerm : 2
     * saleState : Finan_OnSale
     */

    private String id;
    private String productName;
    private Long minInvestMoney;
    private String expectedProfitRateRange;
    private Double fixedProfit;
    private Double remainMoney;
    private Double subscriptionFee;
    private Double purchaseIncreaseMoney;
    private String buyingGroupsCode;
    private String productProfitTypeCode;

    public String getProductProfitTypeCode() {
        return productProfitTypeCode;
    }

    public void setProductProfitTypeCode(String productProfitTypeCode) {
        this.productProfitTypeCode = productProfitTypeCode;
    }

    public Double getPurchaseIncreaseMoney() {
        return purchaseIncreaseMoney;
    }

    public void setPurchaseIncreaseMoney(Double purchaseIncreaseMoney) {
        this.purchaseIncreaseMoney = purchaseIncreaseMoney;
    }

    public String getBuyingGroupsCode() {
        return buyingGroupsCode;
    }

    public void setBuyingGroupsCode(String buyingGroupsCode) {
        this.buyingGroupsCode = buyingGroupsCode;
    }

    public Double getRemainMoney() {
        return remainMoney;
    }

    public void setRemainMoney(Double remainMoney) {
        this.remainMoney = remainMoney;
    }

    public Double getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setSubscriptionFee(Double subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }



    public String getFloatProfit() {
        return floatProfit;
    }

    public void setFloatProfit(String floatProfit) {
        this.floatProfit = floatProfit;
    }

    private String floatProfit;
    private Long investTerm;
    private String saleState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getMinInvestMoney() {
        return minInvestMoney;
    }

    public void setMinInvestMoney(Long minInvestMoney) {
        this.minInvestMoney = minInvestMoney;
    }

    public String getExpectedProfitRateRange() {
        return expectedProfitRateRange;
    }

    public void setExpectedProfitRateRange(String expectedProfitRateRange) {
        this.expectedProfitRateRange = expectedProfitRateRange;
    }

    public Double getFixedProfit() {
        return fixedProfit;
    }

    public void setFixedProfit(Double fixedProfit) {
        this.fixedProfit = fixedProfit;
    }


    public Long getInvestTerm() {
        return investTerm;
    }

    public void setInvestTerm(Long investTerm) {
        this.investTerm = investTerm;
    }

    public String getSaleState() {
        return saleState;
    }

    public void setSaleState(String saleState) {
        this.saleState = saleState;
    }
}
