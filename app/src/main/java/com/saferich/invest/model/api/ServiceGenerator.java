package com.saferich.invest.model.api;


import com.saferich.invest.BuildConfig;
import com.saferich.invest.model.downLoad.FileConverterFactory;
import com.saferich.invest.model.downLoad.body.HttpClientHelper;
import com.saferich.invest.model.downLoad.body.ProgressRequestListener;
import com.saferich.invest.model.downLoad.body.ProgressResponseListener;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * 根据编译环境的不同切换url的前缀地址
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-02
 */

public class ServiceGenerator {
    private static final String HOST = "http://saferich.oss-cn-shanghai.aliyuncs.com/";
    private static final String DebugHOST = "http://saferich-test.oss-cn-shanghai.aliyuncs.com/";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BuildConfig.DEBUG ? DebugHOST : HOST)
            .addConverterFactory(FileConverterFactory.create());


    public static <T> T createService(Class<T> tClass) {
        return builder.build().create(tClass);
    }


    /**
     * 创建带响应进度(下载进度)回调的service
     */
    public static <T> T createResponseService(Class<T> tClass, ProgressResponseListener listener) {
        OkHttpClient client = HttpClientHelper.addProgressResponseListener(new OkHttpClient.Builder(), listener).build();
        return builder
                .client(client)
                .build()
                .create(tClass);
    }


    /**
     * 创建带请求体进度(上传进度)回调的service
     */
    public static <T> T createReqeustService(Class<T> tClass, ProgressRequestListener listener) {
        OkHttpClient client = HttpClientHelper.addProgressRequestListener(new OkHttpClient.Builder(), listener).build();
        return builder
                .client(client)
                .build()
                .create(tClass);
    }
}
