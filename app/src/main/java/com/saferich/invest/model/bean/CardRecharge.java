package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public class CardRecharge {


    /**
     * balance : 51069
     * rechargeMoney : 1500
     * rechargeId : CZ0000000000000016
     */

    private Result result;
    /**
     * sys_message : 签约成功
     * sys_status : 00000
     */

    private Detail detail;
    /**
     * result : {"balance":51069,"rechargeMoney":1500,"rechargeId":"CZ0000000000000016"}
     * detail : {"sys_message":"签约成功","sys_status":"00000"}
     * mobile : 135****2585
     */

    private String mobile;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public static class Result {
        private Double balance;
        private Double rechargeMoney;
        private String rechargeId;

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public Double getRechargeMoney() {
            return rechargeMoney;
        }

        public void setRechargeMoney(Double rechargeMoney) {
            this.rechargeMoney = rechargeMoney;
        }

        public String getRechargeId() {
            return rechargeId;
        }

        public void setRechargeId(String rechargeId) {
            this.rechargeId = rechargeId;
        }
    }

    public static class Detail {
        private String sys_message;
        private String sys_status;

        public String getSys_message() {
            return sys_message;
        }

        public void setSys_message(String sys_message) {
            this.sys_message = sys_message;
        }

        public String getSys_status() {
            return sys_status;
        }

        public void setSys_status(String sys_status) {
            this.sys_status = sys_status;
        }
    }
}
