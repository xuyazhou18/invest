package com.saferich.invest.model.inject.models;

import android.content.Context;
import android.content.res.Resources;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.saferich.invest.MyApplication;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.cache.CacheManager;
import com.saferich.invest.model.event.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * 全局声明的一些依赖变量声明
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-01-28
 */
@Module
public class AppModule {

    private MyApplication myApplication;

    public AppModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @Provides
    @Singleton
    MyApplication providesApplication() {
        return myApplication;
    }


    @Provides
    @Singleton
    Context providesContext() {
        return myApplication;
    }

    @Provides
    @Singleton
    protected Resources provideResources() {
        return myApplication.getResources();
    }


    @Provides
    @Singleton
    protected UserConfig provideConfig() {//用户配置
        return SQLite.select().from(UserConfig.class).querySingle();
    }


    @Provides
    @Singleton
    protected Userinfo provideUserBean() {//用户信息
        return SQLite.select().from(Userinfo.class).querySingle();
    }

    @Provides
    @Singleton
    protected CacheManager provideCacheManager() {//缓存帮助类
        return new CacheManager();
    }

    @Provides
    @Singleton
    protected RxBus provideRxbus() {//事件总线
        return new RxBus();
    }


    @Provides
    protected CompositeSubscription provideSubsccribe() {
        return new CompositeSubscription();
    }
}


