package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public class RechargeBody {


    /**
     * cert_type : 01
     * currency : 156
     * operateChannel : 0
     * terminal_info :
     * terminal_type : mobile
     * rechargeMoney : 1
     * user_ip :
     */

    private String cert_type;
    private String currency;
    private String operateChannel;
    private String terminal_info;
    private String terminal_type;
    private String rechargeMoney;
    private String user_ip;

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOperateChannel() {
        return operateChannel;
    }

    public void setOperateChannel(String operateChannel) {
        this.operateChannel = operateChannel;
    }

    public String getTerminal_info() {
        return terminal_info;
    }

    public void setTerminal_info(String terminal_info) {
        this.terminal_info = terminal_info;
    }

    public String getTerminal_type() {
        return terminal_type;
    }

    public void setTerminal_type(String terminal_type) {
        this.terminal_type = terminal_type;
    }

    public String getRechargeMoney() {
        return rechargeMoney;
    }

    public void setRechargeMoney(String rechargeMoney) {
        this.rechargeMoney = rechargeMoney;
    }

    public String getUser_ip() {
        return user_ip;
    }

    public void setUser_ip(String user_ip) {
        this.user_ip = user_ip;
    }
}
