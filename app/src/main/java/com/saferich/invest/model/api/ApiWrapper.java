package com.saferich.invest.model.api;


import android.content.Context;
import android.support.v4.app.Fragment;

import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.Account;
import com.saferich.invest.model.bean.AdCount;
import com.saferich.invest.model.bean.Article;
import com.saferich.invest.model.bean.Balance;
import com.saferich.invest.model.bean.Bank;
import com.saferich.invest.model.bean.BankItem;
import com.saferich.invest.model.bean.CardRecharge;
import com.saferich.invest.model.bean.CashItem;
import com.saferich.invest.model.bean.Complete;
import com.saferich.invest.model.bean.GetCash;
import com.saferich.invest.model.bean.Invest;
import com.saferich.invest.model.bean.InvestBody;
import com.saferich.invest.model.bean.InvestInfo;
import com.saferich.invest.model.bean.LoginBean;
import com.saferich.invest.model.bean.MsgBean;
import com.saferich.invest.model.bean.MyInvest;
import com.saferich.invest.model.bean.Notice;
import com.saferich.invest.model.bean.OrderInfo;
import com.saferich.invest.model.bean.Product;
import com.saferich.invest.model.bean.ProductInfo;
import com.saferich.invest.model.bean.ProfitBean;
import com.saferich.invest.model.bean.Question;
import com.saferich.invest.model.bean.RechargeBean;
import com.saferich.invest.model.bean.RechargeBody;
import com.saferich.invest.model.bean.RecommondItem;
import com.saferich.invest.model.bean.Response;
import com.saferich.invest.model.bean.UserMessage;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.trello.rxlifecycle.components.support.RxFragment;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 *
 * 网络访问封装类
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-30
 */
public class ApiWrapper extends RetrofitUtil {


    private ApiService apiService;


    public ApiWrapper(ApiService apiService) {
        this.apiService = apiService;
    }

    public Observable<Response<List<Notice>>> getNotice(Fragment context, Map<String, Object> map) {
        map.put("p_maxRowNum", 10);
        return apiService.getNotice(map)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<ProfitBean>> getOrderInfo(Fragment context) {
        return apiService.getOrderInfo()
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<ProfitBean>> getOrderInfo2(Fragment context, String token) {
        return apiService.getOrderInfo2(token)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<MsgBean>> getSystemMessages(Fragment context, Map<String, Object> map) {
        return apiService.getSystemMessages(map)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<Object>> readMsg(Fragment context, Map<String, Object> map) {
        return apiService.readMessage(map)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<Object>> readAllMsg(Fragment context) {
        return apiService.allReadMessage()
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<List<Product>>> getProductList(Fragment context, Map<String, Object> map) {
        return apiService.getProductList(map)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<List<Question>>> getQuestionList(Context context, Map<String, Object> map) {
        map.put("p_maxRowNum", 10);
        return apiService.getQuestionList(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getUpdate(Context context) {
        return apiService.getUpdate()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<Question>>> getQuestionListById(Context context, Map<String, Object> map) {
        return apiService.getQuestionListByid(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<Article>>> getArticleList(Context context, Map<String, Object> map) {
        return apiService.getArticleList(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<Balance>>> getBalanceFlow(Context context, Map<String, Object> map) {
        return apiService.getBalanceFlow(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<Invest>>> getFundFlow(Context context, Map<String, Object> map) {
        return apiService.getFundFlow(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getAgrement(Context context) {
        return apiService.getAgreement()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getsafe(Context context) {
        return apiService.getsafe()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<GetCash>> addExtractCash(Context context, Map<String, Object> map) {
        return apiService.addExtractCash(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<CardRecharge>> addRecharge(Context context, RechargeBody body) {
        return apiService.addRecharge(body)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> confirmPay(Context context, Map<String, Object> map) {
        return apiService.confirmPay(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<OrderInfo>> ordConfirmPay(Context context, Map<String, Object> map) {
        return apiService.ordConfirmPay(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getdes(Context context) {
        return apiService.getDes()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<OrderInfo>> addorder(Context context, InvestBody body) {
        return apiService.addOrder(body)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getQualified(Context context) {
        return apiService.getQualified()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> getJoinUs(Context context) {
        return apiService.getJoinUs()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Question>> getQuestionById(Context context, long id) {

        return apiService.getQuestionByid(id)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> addFeedBack(Context context, Map<String, Object> map) {

        return apiService.addFeedBack(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> getRegistCode(Context context, Map<String, Object> map) {

        return apiService.getRegistCode(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Userinfo>> login(Context context, Map<String, Object> map, String token) {

        return apiService.login(token, map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> register(Context context, Map<String, Object> map) {

        return apiService.register(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> rechargesendVerificationCode(Context context, Map<String, Object> map) {

        return apiService.rechargesendVerificationCode(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> OrdsendVerificationCode(Context context, Map<String, Object> map) {

        return apiService.OrdsendVerificationCode(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> addExtractCashphoneVCode(Context context) {

        return apiService.addExtractCashphoneVCode()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> logout(Context context) {

        return apiService.logout()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> existPhone(Context context, Map<String, Object> map) {

        return apiService.existPhone(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> phoneVCode(Context context, Map<String, Object> map) {

        return apiService.phoneVCode(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<RechargeBean>> getBankLimitByCodeAndChannel(Context context, Map<String, Object> map) {

        return apiService.getBankLimitByCodeAndChannel(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> ifBindBankCard(Context context) {

        return apiService.ifBindBankCard()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<UserMessage>> getAccountInfo(Context context) {

        return apiService.getAccountInfo()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<String>> sendVCodeForBindCard(Context context) {

        return apiService.sendVCodeForBindCard()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> setPassword(Context context, Map<String, Object> map) {

        return apiService.setPassword(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> updatePayPassword(Context context, Map<String, Object> map) {

        return apiService.updatePayPassword(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> setPayPassword(Context context, Map<String, Object> map) {

        return apiService.setPayPassword(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Boolean>> updatePassWord(Context context, Map<String, Object> map) {

        return apiService.updatePassWord(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> bindCard(Context context, Map<String, Object> map) {

        return apiService.bindBankCard(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Object>> changebindCard(Context context, Map<String, Object> map) {

        return apiService.updateBindBankCard(map)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<LoginBean>> getNonce(Context context) {

        return apiService.getNonce();
    }


    public Observable<Response<Account>> getAccountMessage(Context context, String Token) {

        return apiService.getAccountMessage2(Token)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Account>> getAccountMessage(Fragment context, String Token) {

        return apiService.getAccountMessage2(Token)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<Account>> getAccountMessage(Context context) {

        return apiService.getAccountMessage()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<BankItem>>> getBankNameList(Context context) {

        return apiService.getBankNameList()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<CashItem>>> getExtractCashParameterList(Context context) {

        return apiService.getExtractCashParameterList()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<RecommondItem>>> getRecommondList(Fragment context, Map<String, Object> map) {

        return apiService.getRecommondList(map)
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<MyInvest>> getUncompleteOrder(Fragment context) {

        return apiService.getUncompleteOrder()
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<List<Complete>>> getCompleteOrder(Fragment context) {

        return apiService.getCompleteOrder()
                .compose(applySchedulersForFragment((RxFragment) context));
    }

    public Observable<Response<Bank>> getBindCardDetail(Context context) {

        return apiService.getBindCardDetail()
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<InvestInfo>> getOrderDetail(Context context, String id) {

        return apiService.getOrderDetail(id)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<List<AdCount>>> getAdContents(Context context, String id) {

        return apiService.getAdContents(id)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<Article>> getArticleDetail(Context context, String id) {

        return apiService.getArticleDetail(id)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

    public Observable<Response<ProductInfo>> getProductInfo(Context context, String id) {

        return apiService.getProductInfo(id)
                .compose(applySchedulersForActivity((RxAppCompatActivity) context));
    }

}
