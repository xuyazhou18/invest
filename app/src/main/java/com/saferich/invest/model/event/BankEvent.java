package com.saferich.invest.model.event;

/**
 *
 * 选定银行后的事件总线
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class BankEvent {

    public BankEvent(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //银行名称
    String name;
    //银行code
    String code;
}
