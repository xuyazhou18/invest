package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-28
 */

public class Balance  {


    /**
     * id : 23
     * type : 回款
     * memberId : 810000148
     * money : 2200
     * channel : web
     * serialNumber : 2
     * fundsFrom : 理财产品1_金2222
     * fundsTo : 账户余额
     * createDate : 1466498140000
     */

    private String id;
    private String type;
    private String memberId;
    private Double money;
    private String channel;
    private String serialNumber;
    private String fundsFrom;
    private String fundsTo;
    private String statusName;
    private long createDate;

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFundsFrom() {
        return fundsFrom;
    }

    public void setFundsFrom(String fundsFrom) {
        this.fundsFrom = fundsFrom;
    }

    public String getFundsTo() {
        return fundsTo;
    }

    public void setFundsTo(String fundsTo) {
        this.fundsTo = fundsTo;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }
}
