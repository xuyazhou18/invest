package com.saferich.invest.model.bean;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-05
 */

public class MsgBean {
    private int totalCount;
    /**
     * id : 44
     * messageType : 4
     * messageTypeName : 提现
     * isRead : true
     * receiveId : 410000272
     * receiveName : 王前程
     * createTime : 1467702598000
     * updateTime : 1467715081000
     * content : 您好，您于【2016-07-05 14:56:38】提现的【10,000,000】元，由于【代付失败】，未能提取成功。
     */

    private List<list> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<list> getList() {
        return list;
    }

    public void setList(List<list> list) {
        this.list = list;
    }

    public static class list {
        private int id;
        private int messageType;
        private String messageTypeName;
        private boolean isRead;
        private String receiveId;
        private String receiveName;
        private long createTime;
        private long updateTime;
        private String content;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMessageType() {
            return messageType;
        }

        public void setMessageType(int messageType) {
            this.messageType = messageType;
        }

        public String getMessageTypeName() {
            return messageTypeName;
        }

        public void setMessageTypeName(String messageTypeName) {
            this.messageTypeName = messageTypeName;
        }

        public boolean isIsRead() {
            return isRead;
        }

        public void setIsRead(boolean isRead) {
            this.isRead = isRead;
        }

        public String getReceiveId() {
            return receiveId;
        }

        public void setReceiveId(String receiveId) {
            this.receiveId = receiveId;
        }

        public String getReceiveName() {
            return receiveName;
        }

        public void setReceiveName(String receiveName) {
            this.receiveName = receiveName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}
