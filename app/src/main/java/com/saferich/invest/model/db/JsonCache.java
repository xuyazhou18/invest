package com.saferich.invest.model.db;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * 本地存储json表
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-11
 */
@Table(database = AppDatabase.class)
public class JsonCache extends BaseModel {


    @PrimaryKey
    @Column
    public String dataType;//数据类型

    @Column
    public String data;//数据

    @Column
    public String createTime;//数据创建时间
}
