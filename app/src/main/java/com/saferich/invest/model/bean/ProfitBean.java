package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public class ProfitBean {


    /**
     * profit : 0
     * currentInvestMoney : 0
     */

    private String profit;
    private String currentInvestMoney;
    private String totalInvestMoney;

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getCurrentInvestMoney() {
        return currentInvestMoney;
    }

    public void setCurrentInvestMoney(String currentInvestMoney) {
        this.currentInvestMoney = currentInvestMoney;
    }

    public String getTotalInvestMoney() {
        return totalInvestMoney;
    }

    public void setTotalInvestMoney(String totalInvestMoney) {
        this.totalInvestMoney = totalInvestMoney;
    }
}
