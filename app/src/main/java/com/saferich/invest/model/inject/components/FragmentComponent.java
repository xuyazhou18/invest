package com.saferich.invest.model.inject.components;


import com.saferich.invest.model.inject.models.FragmentModule;
import com.saferich.invest.model.inject.scope.FragmentScope;
import com.saferich.invest.vp.main.discover.DiscoverFragment;
import com.saferich.invest.vp.main.discover.noticetrend.notice.NoticeFragment;
import com.saferich.invest.vp.main.discover.noticetrend.trend.TrendFragment;
import com.saferich.invest.vp.main.home.HomeFragment;
import com.saferich.invest.vp.main.home.InvestFragment;
import com.saferich.invest.vp.main.home.InvestVistorFragment;
import com.saferich.invest.vp.main.home.deatils.InvestMessageFragment;
import com.saferich.invest.vp.main.home.deatils.InvestTraceFragment;
import com.saferich.invest.vp.main.home.myList.EndFragment;
import com.saferich.invest.vp.main.home.myList.HavingFragment;
import com.saferich.invest.vp.main.me.MeFragment;
import com.saferich.invest.vp.main.me.UserFragment;
import com.saferich.invest.vp.main.me.VisterFragment;
import com.saferich.invest.vp.main.me.msg.MTrendFragment;
import com.saferich.invest.vp.main.me.msg.MsgFragment;
import com.saferich.invest.vp.main.product.ProductFragment;
import com.saferich.invest.vp.main.product.Progressive.ProgressiveFragment;
import com.saferich.invest.vp.main.product.balance.BalanceFragment;
import com.saferich.invest.vp.main.product.detatils.element.ElementFragment;
import com.saferich.invest.vp.main.product.detatils.moreDeatils.MoreDeatilsFragment;
import com.saferich.invest.vp.main.product.keeping.KeepFragment;
import com.saferich.invest.vp.main.product.steady.SteadyFragment;
import com.saferich.invest.vp.main.product.recommend.RecommendFragment;

import dagger.Component;


/**
 *
 * 全部的 fragment 注入声明
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-03-03
 */
@Component(
        dependencies = AppComponent.class,
        modules = FragmentModule.class
)

@FragmentScope
public interface FragmentComponent {

    void inject(HomeFragment fragment);

    void inject(UserFragment userFragment);

    void inject(ProductFragment productFragment);

    void inject(DiscoverFragment discoverFragment);

    void inject(KeepFragment currentFragment);

    void inject(SteadyFragment fundFragment);

    void inject(RecommendFragment recommendFragment);

    void inject(BalanceFragment timeFragment);

    void inject(NoticeFragment noticeFragment);

    void inject(TrendFragment trendFragment);

    void inject(MeFragment meFragment);

    void inject(VisterFragment visterFragment);

    void inject(MsgFragment msgFragment);

    void inject(MTrendFragment trendFragment);

    void inject(ProgressiveFragment progressiveFragment);

    void inject(ElementFragment elementFragment);

    void inject(MoreDeatilsFragment moreDeatilsFragment);

    void inject(InvestFragment investFragment);

    void inject(InvestVistorFragment investVistorFragment);

    void inject(HavingFragment havingFragment);

    void inject(EndFragment endFragment);

    void inject(InvestMessageFragment investMessageFragment);

    void inject(InvestTraceFragment investMsgFragment);
}
