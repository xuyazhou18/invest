package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-20
 */

public class RechargeBean {


    /**
     * id : 1
     * bankCode : ABC
     * minMoney : 1
     * maxMoney : 50000
     * maxMoneyDay : 200000
     * channel : RB
     */

    private long id;
    private String bankCode;
    private long minMoney;
    private long maxMoney;
    private long maxMoneyDay;
    private String channel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public long getMinMoney() {
        return minMoney;
    }

    public void setMinMoney(long minMoney) {
        this.minMoney = minMoney;
    }

    public long getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(long maxMoney) {
        this.maxMoney = maxMoney;
    }

    public long getMaxMoneyDay() {
        return maxMoneyDay;
    }

    public void setMaxMoneyDay(long maxMoneyDay) {
        this.maxMoneyDay = maxMoneyDay;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
