package com.saferich.invest.model.event;

/**
 * 支付成功的事件总线
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-16
 */
public class PayPWSuccessEvent {

    public PayPWSuccessEvent() {
    }
}
