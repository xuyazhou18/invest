package com.saferich.invest.model.event;

/**
 *
 * 登陆成功的事件总线
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-16
 */
public class loginEvent {
    String Token;

    public loginEvent(String token) {
        Token = token;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
