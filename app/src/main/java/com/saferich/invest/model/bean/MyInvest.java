package com.saferich.invest.model.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public class MyInvest {

    private List<NonProfit> profit;
    @SerializedName("returnInvest")
    private List<NonProfit> returnX;
    /**
     * productName : 理财产品1副本
     * investMoney : 200
     * investTerm : 30
     */

    private List<NonProfit> nonProfit;

    public List<NonProfit> getProfit() {
        return profit;
    }

    public void setProfit(List<NonProfit> profit) {
        this.profit = profit;
    }

    public List<NonProfit> getReturnX() {
        return returnX;
    }

    public void setReturnX(List<NonProfit> returnX) {
        this.returnX = returnX;
    }

    public List<NonProfit> getNonProfit() {
        return nonProfit;
    }

    public void setNonProfit(List<NonProfit> nonProfit) {
        this.nonProfit = nonProfit;
    }

    public static class NonProfit {
        private String productName;
        private Double investMoney;
        private int investTerm;
        private int type;
        private String orderId;
        private String productId;

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Double getInvestMoney() {
            return investMoney;
        }

        public void setInvestMoney(Double investMoney) {
            this.investMoney = investMoney;
        }

        public int getInvestTerm() {
            return investTerm;
        }

        public void setInvestTerm(int investTerm) {
            this.investTerm = investTerm;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }
    }
}
