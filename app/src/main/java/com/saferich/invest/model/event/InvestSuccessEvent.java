package com.saferich.invest.model.event;

/**
 *
 * 投资成功的事件总线
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class InvestSuccessEvent {
    String Token;

    public InvestSuccessEvent(String token) {
        Token = token;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
