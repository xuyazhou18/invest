package com.saferich.invest.model.db;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;


/**
 * 用户信息配置表
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
@ModelContainer
@Table(database = AppDatabase.class)
public class Userinfo extends BaseModel {

    @PrimaryKey
    @Column
    public int userType;//用户类型

    @Column
    public long memberId;//用户id
    @Column
    public Double balance;//用户余额

    @Column
    public Double totalMoney;//用户总资产

    @Column
    public boolean isBindCard;//是否绑定银行卡
    @Column
    public boolean ifNewInvest;//是否是第一次投资
    @Column
    public boolean ifCardUsed;//银行卡是否使用过

    @Column
    public boolean isSetPayWord;//是否设置过交易密码

    @Column
    public double profit;//收益

    @Column
    public double currentInvestMoney;//当前投资金额

    @Column
    public String bankCode;//银行code
    @Column
    public String bankCard;//银行名称

    @Column
    public String payPassword;//交易密码
    @Column
    public String gesturePassword;//手势密码

    @Column
    public String accessToken;//token
    @Column
    public String idCard;//身份证号码
    @Column
    public String mobliePhone;//银行绑定号码

    @Column
    public String userLoginPhoneNumber;//用户登录密码

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public boolean isBindCard() {
        return isBindCard;
    }

    public void setBindCard(boolean bindCard) {
        isBindCard = bindCard;
    }

    public boolean isIfCardUsed() {
        return ifCardUsed;
    }

    public void setIfCardUsed(boolean ifCardUsed) {
        this.ifCardUsed = ifCardUsed;
    }

    public boolean isSetPayWord() {
        return isSetPayWord;
    }

    public void setSetPayWord(boolean setPayWord) {
        isSetPayWord = setPayWord;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getCurrentInvestMoney() {
        return currentInvestMoney;
    }

    public void setCurrentInvestMoney(double currentInvestMoney) {
        this.currentInvestMoney = currentInvestMoney;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public String getGesturePassword() {
        return gesturePassword;
    }

    public void setGesturePassword(String gesturePassword) {
        this.gesturePassword = gesturePassword;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMobliePhone() {
        return mobliePhone;
    }

    public void setMobliePhone(String mobliePhone) {
        this.mobliePhone = mobliePhone;
    }

    public String getUserLoginPhoneNumber() {
        return userLoginPhoneNumber;
    }

    public void setUserLoginPhoneNumber(String userLoginPhoneNumber) {
        this.userLoginPhoneNumber = userLoginPhoneNumber;
    }
}
