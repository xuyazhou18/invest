package com.saferich.invest.model.bean;

import com.saferich.invest.common.Utils.StringUtil;
import com.saferich.invest.common.config.ErrorCode;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-30
 */
public class Response<T> {
    public boolean success;
    public String errCode;
    public String message;
    public T data;

    public boolean isTokenExpired() {
        return StringUtil.equals(errCode, ErrorCode.TOKENEXPIRED) ;
    }

    public boolean isTokenExpired2() {
        return StringUtil.equals(errCode, ErrorCode.TOKENEXPIRED2);
    }

    public boolean isTokenExpired3() {
        return StringUtil.equals(errCode, ErrorCode.TOKENEXPIRED3);
    }

    public boolean isBindCardFail() {
        return StringUtil.equals(errCode, ErrorCode.BindCardFail);
    }

    public boolean isPayWordFail() {
        return StringUtil.equals(errCode, ErrorCode.PayWordFail);
    }


}
