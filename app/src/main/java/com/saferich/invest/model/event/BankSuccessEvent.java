package com.saferich.invest.model.event;

/**
 *
 * 绑定银行卡后的事件总线
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class BankSuccessEvent {
    String Token;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public BankSuccessEvent(String token) {
        Token = token;
    }
}
