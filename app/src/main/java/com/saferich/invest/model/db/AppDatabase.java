package com.saferich.invest.model.db;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p/>
 * Date: 2016-03-07
 */

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {


    //数据库名称
    public static final String NAME = "InvestDb";
    //版本号
    public static final int VERSION = 1;
}
