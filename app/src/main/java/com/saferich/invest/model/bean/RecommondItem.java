package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-24
 */

public class RecommondItem {


    /**
     * id : QDFina000009
     * productName : 理财产品测试123
     * minInvestMoney : 4636737291354636288
     * expectedProfitRateRange : null
     * fixedProfit : 1.2
     * floatProfit : null
     * investTerm : 30
     * saleState : Finan_OnSale
     */

    private String id;
    private String productName;
    private long minInvestMoney;
    private String expectedProfitRateRange;
    private double fixedProfit;
    private String floatProfit;
    private int investTerm;
    private String saleState;
    private String buyingGroupsCode;
    private String productProfitTypeCode;



    public String getProductProfitTypeCode() {
        return productProfitTypeCode;
    }

    public void setProductProfitTypeCode(String productProfitTypeCode) {
        this.productProfitTypeCode = productProfitTypeCode;
    }

    public String getBuyingGroupsCode() {
        return buyingGroupsCode;
    }

    public void setBuyingGroupsCode(String buyingGroupsCode) {
        this.buyingGroupsCode = buyingGroupsCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getMinInvestMoney() {
        return minInvestMoney;
    }

    public void setMinInvestMoney(long minInvestMoney) {
        this.minInvestMoney = minInvestMoney;
    }

    public Object getExpectedProfitRateRange() {
        return expectedProfitRateRange;
    }

    public void setExpectedProfitRateRange(String expectedProfitRateRange) {
        this.expectedProfitRateRange = expectedProfitRateRange;
    }

    public double getFixedProfit() {
        return fixedProfit;
    }

    public void setFixedProfit(double fixedProfit) {
        this.fixedProfit = fixedProfit;
    }

    public String getFloatProfit() {
        return floatProfit;
    }

    public void setFloatProfit(String floatProfit) {
        this.floatProfit = floatProfit;
    }

    public int getInvestTerm() {
        return investTerm;
    }

    public void setInvestTerm(int investTerm) {
        this.investTerm = investTerm;
    }

    public String getSaleState() {
        return saleState;
    }

    public void setSaleState(String saleState) {
        this.saleState = saleState;
    }
}
