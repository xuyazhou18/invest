package com.saferich.invest.model.bean;

import java.util.List;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-22
 */

public class ProductInfo {


    /**
     * id : QDFina000018
     * productName : 0701平衡
     * minInvestMoney : 1
     * expectedProfitRateRange : null
     * fixedProfit : null
     * floatProfit : 1
     * appIsShowCarousel : true
     * investTerm : 30
     * productScale : 30
     * haveRaisedMoney : 0
     * investmentProgress : 0
     * purchaseIncreaseMoney : 1
     * startSaleDate : 1466870400000
     * endSaleDate : 1467302400000
     * productProfitTypeCode : FloatIncome
     * productProfitTypeName : 浮动收益
     * profitSettleMannerCode : ProfitSettleManner_CarryOver
     * profitSettleMannerName : 到期结转
     * beginProfitDateTypeCode : null
     * beginProfitDateTypeName : null
     * beginBenefitDayTypeCode : BeginBenfitDayType_Days
     * beginBenefitDayTypeName : 工作日
     * beginProfitDay : null
     * capitalRedeemRuleCode : CapitalRedeemRule_Maturity
     * capitalRedeemRuleName : 到期汇款
     * supportRedeemStatus : null
     * riskTypeCode : Balance
     * riskTypeName : 平衡
     * subscriptionFee : 1
     * managementFee : 1
     * remainMoney : 30
     * remarkIllustration : 1
     * buyingGroupsCode : BuyingGroupsCode_NEW
     * buyingGroupsName : 新用户
     * textList : [{"id":null,"channelProductId":"QDFina000018","teminalType":null,"rich1":"<p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;<\/p><p>呵呵呵呵！@#￥%\u2026\u2026&amp;*<\/p><p>123456797643456<\/p>","rich2":"<p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;<\/p><p>呵呵呵呵！@#￥%\u2026\u2026&amp;*<\/p><p><\/p><p>123456797643456<\/p><p><br><\/p>","rich3":"<p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;<\/p><p>呵呵呵呵！@#￥%\u2026\u2026&amp;*<\/p><p>123456797643456<\/p><p><br><\/p>","rich4":"<p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;<\/p><p>呵呵呵呵！@#￥%\u2026\u2026&amp;*<\/p><p>123456797643456<\/p><p><br><\/p>","rich5":"<p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;<\/p><p>呵呵呵呵！@#￥%\u2026\u2026&amp;*<\/p><p>123456797643456<\/p><p><br><\/p>"}]
     * channelImageCarousel : [{"id":19,"productId":"QDFina000018","imageAddress":"Carouselpic/QDFina000018/t016c362fac77848723.jpg","uploadTime":1467352539000,"uploadUser":"Admin"}]
     * protocalList : [{"protocalNo":"0001","state":"1","name":"协议","foregroundDisplayName":"协议","protocalType":"1","createTime":1467271603000,"createUser":"Admin","updateTime":1467271875000,"updateUser":"Admin","productProtocal":"20140515124294819481.jpeg"},{"protocalNo":"0002","state":"1","name":"服务协议1","foregroundDisplayName":"服务协议1","protocalType":"0","createTime":1467272598000,"createUser":"Admin","updateTime":1467272598000,"updateUser":"Admin","productProtocal":"scbc.PNG"}]
     */

    private String id;
    private String productName;
    private Double minInvestMoney;
    private String expectedProfitRateRange;
    private Double fixedProfit;
    private String floatProfit;
    private boolean appIsShowCarousel;
    private long investTerm;
    private Double productScale;
    private double haveRaisedMoney;
    private long investmentProgress;
    private long beginProfitDate;
    private Double purchaseIncreaseMoney;
    private long startSaleDate;
    private long endSaleDate;
    private long beginProfitDay;
    private String productProfitTypeCode;
    private String productProfitTypeName;
    private String profitSettleMannerCode;
    private String profitSettleMannerName;
    private String beginProfitDateTypeCode;
    private String beginProfitDateTypeName;
    private String beginBenefitDayTypeCode;
    private String beginBenefitDayTypeName;
    private String capitalRedeemRuleCode;
    private String capitalRedeemRuleName;
    private boolean supportRedeemStatus;
    private String riskTypeCode;
    private String riskTypeName;
    private Double subscriptionFee;
    private Double managementFee;
    private Double remainMoney;
    private String remarkIllustration;
    private String buyingGroupsCode;
    private String buyingGroupsName;
    private String saleState;
    private String saleStateName;
    private String beginProfitMannerCode;


    public long getBeginProfitDay() {
        return beginProfitDay;
    }

    public void setBeginProfitDay(long beginProfitDay) {
        this.beginProfitDay = beginProfitDay;
    }

    public long getBeginProfitDate() {
        return beginProfitDate;
    }

    public void setBeginProfitDate(long beginProfitDate) {
        this.beginProfitDate = beginProfitDate;
    }

    public String getBeginProfitMannerCode() {
        return beginProfitMannerCode;
    }

    public void setBeginProfitMannerCode(String beginProfitMannerCode) {
        this.beginProfitMannerCode = beginProfitMannerCode;
    }

    public void setMinInvestMoney(Double minInvestMoney) {
        this.minInvestMoney = minInvestMoney;
    }

    public void setPurchaseIncreaseMoney(Double purchaseIncreaseMoney) {
        this.purchaseIncreaseMoney = purchaseIncreaseMoney;
    }

    public String getSaleState() {
        return saleState;
    }

    public void setSaleState(String saleState) {
        this.saleState = saleState;
    }

    public String getSaleStateName() {
        return saleStateName;
    }

    public void setSaleStateName(String saleStateName) {
        this.saleStateName = saleStateName;
    }

    public List<ProductInfo.accessoriesList> getAccessoriesList() {
        return accessoriesList;
    }

    public void setAccessoriesList(List<ProductInfo.accessoriesList> accessoriesList) {
        this.accessoriesList = accessoriesList;
    }

    /**
     * id : null
     * channelProductId : QDFina000018
     * teminalType : null
     * rich1 : <p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;</p><p>呵呵呵呵！@#￥%……&amp;*</p><p>123456797643456</p>
     * rich2 : <p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;</p><p>呵呵呵呵！@#￥%……&amp;*</p><p></p><p>123456797643456</p><p><br></p>
     * rich3 : <p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;</p><p>呵呵呵呵！@#￥%……&amp;*</p><p>123456797643456</p><p><br></p>
     * rich4 : <p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;</p><p>呵呵呵呵！@#￥%……&amp;*</p><p>123456797643456</p><p><br></p>
     * rich5 : <p>肥嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟嘟 &nbsp; &nbsp;</p><p>呵呵呵呵！@#￥%……&amp;*</p><p>123456797643456</p><p><br></p>
     */

    private List<TextList> textList;
    /**
     * id : 19
     * productId : QDFina000018
     * imageAddress : Carouselpic/QDFina000018/t016c362fac77848723.jpg
     * uploadTime : 1467352539000
     * uploadUser : Admin
     */

    private List<ChannelImageCarousel> channelImageCarousel;
    private List<accessoriesList> accessoriesList;
    /**
     * protocalNo : 0001
     * state : 1
     * name : 协议
     * foregroundDisplayName : 协议
     * protocalType : 1
     * createTime : 1467271603000
     * createUser : Admin
     * updateTime : 1467271875000
     * updateUser : Admin
     * productProtocal : 20140515124294819481.jpeg
     */

    private List<ProtocalList> protocalList;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getMinInvestMoney() {
        return minInvestMoney;
    }

    public void setMinInvestMoney(double minInvestMoney) {
        this.minInvestMoney = minInvestMoney;
    }

    public String getExpectedProfitRateRange() {
        return expectedProfitRateRange;
    }

    public void setExpectedProfitRateRange(String expectedProfitRateRange) {
        this.expectedProfitRateRange = expectedProfitRateRange;
    }

    public Double getFixedProfit() {
        return fixedProfit;
    }

    public void setFixedProfit(Double fixedProfit) {
        this.fixedProfit = fixedProfit;
    }

    public String getFloatProfit() {
        return floatProfit;
    }

    public void setFloatProfit(String floatProfit) {
        this.floatProfit = floatProfit;
    }

    public boolean isAppIsShowCarousel() {
        return appIsShowCarousel;
    }

    public void setAppIsShowCarousel(boolean appIsShowCarousel) {
        this.appIsShowCarousel = appIsShowCarousel;
    }

    public long getInvestTerm() {
        return investTerm;
    }

    public void setInvestTerm(long investTerm) {
        this.investTerm = investTerm;
    }

    public Double getProductScale() {
        return productScale;
    }

    public void setProductScale(Double productScale) {
        this.productScale = productScale;
    }

    public double getHaveRaisedMoney() {
        return haveRaisedMoney;
    }

    public void setHaveRaisedMoney(double haveRaisedMoney) {
        this.haveRaisedMoney = haveRaisedMoney;
    }

    public long getInvestmentProgress() {
        return investmentProgress;
    }

    public void setInvestmentProgress(long investmentProgress) {
        this.investmentProgress = investmentProgress;
    }

    public Double getPurchaseIncreaseMoney() {
        return purchaseIncreaseMoney;
    }

    public void setPurchaseIncreaseMoney(double purchaseIncreaseMoney) {
        this.purchaseIncreaseMoney = purchaseIncreaseMoney;
    }

    public long getStartSaleDate() {
        return startSaleDate;
    }

    public void setStartSaleDate(long startSaleDate) {
        this.startSaleDate = startSaleDate;
    }

    public long getEndSaleDate() {
        return endSaleDate;
    }

    public void setEndSaleDate(long endSaleDate) {
        this.endSaleDate = endSaleDate;
    }

    public String getProductProfitTypeCode() {
        return productProfitTypeCode;
    }

    public void setProductProfitTypeCode(String productProfitTypeCode) {
        this.productProfitTypeCode = productProfitTypeCode;
    }

    public String getProductProfitTypeName() {
        return productProfitTypeName;
    }

    public void setProductProfitTypeName(String productProfitTypeName) {
        this.productProfitTypeName = productProfitTypeName;
    }

    public String getProfitSettleMannerCode() {
        return profitSettleMannerCode;
    }

    public void setProfitSettleMannerCode(String profitSettleMannerCode) {
        this.profitSettleMannerCode = profitSettleMannerCode;
    }

    public String getProfitSettleMannerName() {
        return profitSettleMannerName;
    }

    public void setProfitSettleMannerName(String profitSettleMannerName) {
        this.profitSettleMannerName = profitSettleMannerName;
    }

    public String getBeginProfitDateTypeCode() {
        return beginProfitDateTypeCode;
    }

    public void setBeginProfitDateTypeCode(String beginProfitDateTypeCode) {
        this.beginProfitDateTypeCode = beginProfitDateTypeCode;
    }

    public String getBeginProfitDateTypeName() {
        return beginProfitDateTypeName;
    }

    public void setBeginProfitDateTypeName(String beginProfitDateTypeName) {
        this.beginProfitDateTypeName = beginProfitDateTypeName;
    }

    public String getBeginBenefitDayTypeCode() {
        return beginBenefitDayTypeCode;
    }

    public void setBeginBenefitDayTypeCode(String beginBenefitDayTypeCode) {
        this.beginBenefitDayTypeCode = beginBenefitDayTypeCode;
    }

    public String getBeginBenefitDayTypeName() {
        return beginBenefitDayTypeName;
    }

    public void setBeginBenefitDayTypeName(String beginBenefitDayTypeName) {
        this.beginBenefitDayTypeName = beginBenefitDayTypeName;
    }

    public String getCapitalRedeemRuleCode() {
        return capitalRedeemRuleCode;
    }

    public void setCapitalRedeemRuleCode(String capitalRedeemRuleCode) {
        this.capitalRedeemRuleCode = capitalRedeemRuleCode;
    }

    public String getCapitalRedeemRuleName() {
        return capitalRedeemRuleName;
    }

    public void setCapitalRedeemRuleName(String capitalRedeemRuleName) {
        this.capitalRedeemRuleName = capitalRedeemRuleName;
    }

    public boolean isSupportRedeemStatus() {
        return supportRedeemStatus;
    }

    public void setSupportRedeemStatus(boolean supportRedeemStatus) {
        this.supportRedeemStatus = supportRedeemStatus;
    }

    public String getRiskTypeCode() {
        return riskTypeCode;
    }

    public void setRiskTypeCode(String riskTypeCode) {
        this.riskTypeCode = riskTypeCode;
    }

    public String getRiskTypeName() {
        return riskTypeName;
    }

    public void setRiskTypeName(String riskTypeName) {
        this.riskTypeName = riskTypeName;
    }

    public Double getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setSubscriptionFee(Double subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }

    public Double getManagementFee() {
        return managementFee;
    }

    public void setManagementFee(Double managementFee) {
        this.managementFee = managementFee;
    }

    public Double getRemainMoney() {
        return remainMoney;
    }

    public void setRemainMoney(Double remainMoney) {
        this.remainMoney = remainMoney;
    }

    public String getRemarkIllustration() {
        return remarkIllustration;
    }

    public void setRemarkIllustration(String remarkIllustration) {
        this.remarkIllustration = remarkIllustration;
    }

    public String getBuyingGroupsCode() {
        return buyingGroupsCode;
    }

    public void setBuyingGroupsCode(String buyingGroupsCode) {
        this.buyingGroupsCode = buyingGroupsCode;
    }

    public String getBuyingGroupsName() {
        return buyingGroupsName;
    }

    public void setBuyingGroupsName(String buyingGroupsName) {
        this.buyingGroupsName = buyingGroupsName;
    }

    public List<TextList> getTextList() {
        return textList;
    }

    public void setTextList(List<TextList> textList) {
        this.textList = textList;
    }

    public List<ChannelImageCarousel> getChannelImageCarousel() {
        return channelImageCarousel;
    }

    public void setChannelImageCarousel(List<ChannelImageCarousel> channelImageCarousel) {
        this.channelImageCarousel = channelImageCarousel;
    }

    public List<ProtocalList> getProtocalList() {
        return protocalList;
    }

    public void setProtocalList(List<ProtocalList> protocalList) {
        this.protocalList = protocalList;
    }

    public static class TextList {
        private Object id;
        private String channelProductId;
        private Object teminalType;
        private String rich1;
        private String rich2;
        private String rich3;
        private String rich4;
        private String rich5;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public Object getTeminalType() {
            return teminalType;
        }

        public void setTeminalType(Object teminalType) {
            this.teminalType = teminalType;
        }

        public String getRich1() {
            return rich1;
        }

        public void setRich1(String rich1) {
            this.rich1 = rich1;
        }

        public String getRich2() {
            return rich2;
        }

        public void setRich2(String rich2) {
            this.rich2 = rich2;
        }

        public String getRich3() {
            return rich3;
        }

        public void setRich3(String rich3) {
            this.rich3 = rich3;
        }

        public String getRich4() {
            return rich4;
        }

        public void setRich4(String rich4) {
            this.rich4 = rich4;
        }

        public String getRich5() {
            return rich5;
        }

        public void setRich5(String rich5) {
            this.rich5 = rich5;
        }
    }

    public static class ChannelImageCarousel {
        private long id;
        private String productId;
        private String imageAddress;
        private long uploadTime;
        private String uploadUser;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getImageAddress() {
            return imageAddress;
        }

        public void setImageAddress(String imageAddress) {
            this.imageAddress = imageAddress;
        }

        public long getUploadTime() {
            return uploadTime;
        }

        public void setUploadTime(long uploadTime) {
            this.uploadTime = uploadTime;
        }

        public String getUploadUser() {
            return uploadUser;
        }

        public void setUploadUser(String uploadUser) {
            this.uploadUser = uploadUser;
        }
    }

    public static class accessoriesList {

        /**
         * id : 147
         * fileName : QDFina000001/110031_201502.pdf
         * version : 1.0.0
         * associateNo : QDFina000239
         * createTime : 1467362683000
         * createUser : loginuser
         * updateUser : loginuser
         * updateTime : 1467362683000
         * state : Y
         * accessoriseType : Report
         */

        private int id;
        private String fileName;
        private String version;
        private String associateNo;
        private long createTime;
        private String createUser;
        private String updateUser;
        private long updateTime;
        private String state;
        private String accessoriseType;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getAssociateNo() {
            return associateNo;
        }

        public void setAssociateNo(String associateNo) {
            this.associateNo = associateNo;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getAccessoriseType() {
            return accessoriseType;
        }

        public void setAccessoriseType(String accessoriseType) {
            this.accessoriseType = accessoriseType;
        }
    }

    public static class ProtocalList {
        private String protocalNo;
        private String state;
        private String name;
        private String foregroundDisplayName;
        private String protocalType;
        private long createTime;
        private String createUser;
        private long updateTime;
        private String updateUser;
        private String productProtocal;

        public String getProtocalNo() {
            return protocalNo;
        }

        public void setProtocalNo(String protocalNo) {
            this.protocalNo = protocalNo;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getForegroundDisplayName() {
            return foregroundDisplayName;
        }

        public void setForegroundDisplayName(String foregroundDisplayName) {
            this.foregroundDisplayName = foregroundDisplayName;
        }

        public String getProtocalType() {
            return protocalType;
        }

        public void setProtocalType(String protocalType) {
            this.protocalType = protocalType;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public String getProductProtocal() {
            return productProtocal;
        }

        public void setProductProtocal(String productProtocal) {
            this.productProtocal = productProtocal;
        }
    }
}
