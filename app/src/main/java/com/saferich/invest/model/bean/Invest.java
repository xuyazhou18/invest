package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-29
 */

public class Invest  {


    /**
     * id : 36
     * type : 投资
     * memberId : 810000313
     * money : 200.2
     * channel : android
     * serialNumber : DD1000041
     * fundsFrom : 余额
     * fundsTo : 理财产品1副本
     * createDate : 1467168307000
     */

    private String id;
    private String type;
    private String memberId;
    private double money;
    private String channel;
    private String serialNumber;
    private String fundsFrom;
    private String fundsTo;
    private long createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFundsFrom() {
        return fundsFrom;
    }

    public void setFundsFrom(String fundsFrom) {
        this.fundsFrom = fundsFrom;
    }

    public String getFundsTo() {
        return fundsTo;
    }

    public void setFundsTo(String fundsTo) {
        this.fundsTo = fundsTo;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }
}
