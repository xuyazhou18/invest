package com.saferich.invest.model.bean;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-14
 */

public class UserMessage {


    /**
     * ifBindBankCard : false
     * m_phone : 135****2585
     * ifSetPayPassword : false
     */

    private boolean ifBindBankCard;
    private String m_phone;
    private boolean ifSetPayPassword;
    private boolean ifCardUsed;

    public boolean isIfCardUsed() {
        return ifCardUsed;
    }

    public void setIfCardUsed(boolean ifCardUsed) {
        this.ifCardUsed = ifCardUsed;
    }

    public boolean isIfBindBankCard() {
        return ifBindBankCard;
    }

    public void setIfBindBankCard(boolean ifBindBankCard) {
        this.ifBindBankCard = ifBindBankCard;
    }

    public String getM_phone() {
        return m_phone;
    }

    public void setM_phone(String m_phone) {
        this.m_phone = m_phone;
    }

    public boolean isIfSetPayPassword() {
        return ifSetPayPassword;
    }

    public void setIfSetPayPassword(boolean ifSetPayPassword) {
        this.ifSetPayPassword = ifSetPayPassword;
    }
}
