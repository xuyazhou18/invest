package com.saferich.invest;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.saferich.invest.model.db.AppDatabase;
import com.saferich.invest.model.db.SQLCipherHelperImpl;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.inject.components.AppComponent;
import com.saferich.invest.model.inject.components.DaggerAppComponent;
import com.saferich.invest.model.inject.models.ApiServiceModule;
import com.saferich.invest.model.inject.models.AppModule;

import java.util.LinkedList;
import java.util.List;

/**
 * app的全局配置
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-04-26
 */
public class MyApplication extends Application {
    AppComponent appComponent = null;
    private List<Activity> activityList = new LinkedList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        //数据库配置
        initDB();
        //Dagger依赖注入配置
        initInject();


    }

    private void initDB() {
        //数据库的创建和SQLCipherHelper加密
        FlowManager.init(new FlowConfig.Builder(this)
                .addDatabaseConfig(
                        new DatabaseConfig.Builder(AppDatabase.class)
                                .openHelper(SQLCipherHelperImpl::new)
                                .build())
                .build());

        if (SQLite.select().from(UserConfig.class).querySingle() == null) {
            UserConfig config = new UserConfig();
            config.save();
        }


        if (SQLite.select().from(Userinfo.class).querySingle() == null) {
            Userinfo userinfo = new Userinfo();
            userinfo.save();
        }

    }


    private void initInject() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiServiceModule(new ApiServiceModule())
                .build();


    }


    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    // 添加Activity到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    // 遍历所有Activity并finish
    public void exit() {

        for (Activity activity : activityList) {
            activity.finish();
        }
        System.exit(0);
    }
}
