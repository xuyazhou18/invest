package com.saferich.invest.common.Utils;

import android.content.Context;
import android.util.SparseIntArray;
import android.widget.ImageView;

import com.saferich.invest.R;

import java.util.ArrayList;



/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 *
 * Date: 2016-03-23
 */
public class LocalResourceHandler {

	private static final SparseIntArray GuideItems = new SparseIntArray(5);
	static {
		GuideItems.put(0, R.mipmap.mom_no1);
		GuideItems.put(1, R.mipmap.mom_no1);
		GuideItems.put(2, R.mipmap.mom_no1);

	}

	public static ArrayList<ImageView> getGuideItemList(Context context) {
		ArrayList<ImageView> imageViews = new ArrayList<>();
		for (int i = 0; i < GuideItems.size(); i++) {
			ImageView imageView = new ImageView(context);
			imageView.setImageResource(GuideItems.get(i));

			imageViews.add(imageView);
		}

		return imageViews;
	}
}
