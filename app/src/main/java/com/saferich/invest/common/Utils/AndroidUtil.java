package com.saferich.invest.common.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.List;


/**
 * android系统相关常用操作
 * 
 * @author Aizhimin 说明：提供一些android系统常用操作：如系统版本，图片操作等
 */
public class AndroidUtil {

	/**
	 * 获取sdk版本
	 * 
	 * @return
	 */
	public static int getAndroidSDKVersion() {
		return android.os.Build.VERSION.SDK_INT;
	}

	/**
	 * 返回当前程序版本号
	 */
	public static int getAppVersionCode(Context context) {
		int versionCode = 0;
		try {
			// ---get the package info---
			PackageManager pm = context.getPackageManager();
			// 这里的context.getPackageName()可以换成你要查看的程序的包名
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionCode = pi.versionCode;
		} catch (Exception e) {
			Log.e("VersionInfo", "Exception", e);
		}
		return versionCode;
	}


    /**
     * 获取版本号
     */
    public static String getVersion(Context context) {
        String version = "NONE";
        try {
            PackageManager packageManager = context.getPackageManager();
            // getPackageName()是你当前类的包名，0代表是获取版本信息
            PackageInfo packInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            version = packInfo.versionName;
        } catch (Exception e) {

        }
        return version;
    }

    /**
     * 获取内部版本号
     */
    public static String getInnerVersionCode(Context context) {
		String versionCode = "NONE";
        try {
            PackageManager packageManager = context.getPackageManager();
            // getPackageName()是你当前类的包名，0代表是获取版本信息
            PackageInfo packInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            versionCode = String.valueOf(packInfo.versionCode);
        } catch (Exception e) {

        }
        return versionCode;
    }
    

	/**
	 * 返回当前程序版本名
	 */
	public static String getAppVersionName(Context context) {
		String versionName = "0.0.0";
		String versionNameNew = "0.0.0";

		try {
			// ---get the package info---
			PackageManager pm = context.getPackageManager();
			// 这里的context.getPackageName()可以换成你要查看的程序的包名
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionName = pi.versionName;
//			String[] sp = versionName.split(".");
//			versionNameNew = sp[0] + "." + sp[1] + "." + sp[2];
//			StringTokenizer token = new StringTokenizer(versionName, ".");
			if (versionName == null || versionName.length() <= 0) {
				return "";
			}
		} catch (Exception e) {
			Log.e("VersionInfo", "Exception", e);
		}
		return versionName;
	}

	/**
	 * 获得设备识别认证码
	 * 
	 * @return
	 */
	public static String getIMEI(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm == null) {
			return null;
		}
		return tm.getDeviceId();
	}

//	/**
//	 * 退出应用
//	 *
//	 * @param context
//	 */
//	public static void exitApp(Context context) {
//		List<Activity> list = ((my) context
//				.getApplicationContext()).getActivities();
//		for (Activity ac : list) {
//			ac.finish();
//		}
//		list.clear();
//		System.gc();
//		// 关闭service
//		// Intent it = new Intent(context,PullService.class);
//		// context.stopService(it);
//	}

	/**
	 * 关闭软键盘
	 * 
	 * @param context
	 */
	public static void closeKeyBox(Context context) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		final View v = ((Activity) context).getWindow().peekDecorView();
		imm.hideSoftInputFromWindow(v.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * 判断sd卡是否安装
	 * 
	 * @return
	 */
	public static boolean existSdcard() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	/**
	 * 获得屏幕密度
	 * 
	 * @param
	 * @return
	 */
	public static float getDensity(Context context) {
		DisplayMetrics dm = context.getApplicationContext().getResources()
				.getDisplayMetrics();
		return dm.density;
	}

	/**
	 * 获得屏幕尺寸
	 * 
	 * @param context
	 * @return
	 */
	public static final Display getScreenSize(Context context) {
		return ((Activity) context).getWindowManager().getDefaultDisplay();
	}

	/**
	 * 得到屏幕宽度
	 * 
	 * @return
	 */
	public static int getDeviceWidth(Context context) {
		return context.getResources().getDisplayMetrics().widthPixels;
	}

	/**
	 * 得到栈顶activity名
	 * 
	 * @param context
	 * @return
	 */
	public static String getTaskStackTopActivityStr(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		List<RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);

		if (runningTaskInfos == null) {
			return null;
		}

		if (runningTaskInfos.size() != 0) {
			String topStr = (runningTaskInfos.get(0).topActivity).toString();
			return topStr;
		} else {
			return null;
		}
	}
}
