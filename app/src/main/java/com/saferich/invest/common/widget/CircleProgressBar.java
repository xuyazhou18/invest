package com.saferich.invest.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * 环形进度条实现
 * 
 */
public class CircleProgressBar extends View {
	private int maxProgress = 100;
	private Float progress = 30f;
	private int progressStrokeWidth;
	// 画圆所在的距形区域
	RectF oval;
	Paint paint;

	private Context context;

	public CircleProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO 自动生成的构造函数存根
		oval = new RectF();
		paint = new Paint();
		this.context = context;

        //progressStrokeWidth = DisplayUtil.dip2px(context,10);

        progressStrokeWidth = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, context.getResources().getDisplayMetrics());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO 自动生成的方法存根
		super.onDraw(canvas);
		int width = this.getWidth();
		int height = this.getHeight();

		if (width != height) {
			int min = Math.min(width, height);
			width = min;
			height = min;
		}

		paint.setAntiAlias(true); // 设置画笔为抗锯齿

		paint.setColor(Color.parseColor("#10ffffff"));

		canvas.drawColor(Color.TRANSPARENT); // 白色背景
		paint.setStrokeWidth( progressStrokeWidth); // 线宽
		paint.setStyle(Style.STROKE);

		oval.left = progressStrokeWidth / 2; // 左上角x
		oval.top = progressStrokeWidth / 2; // 左上角y
		oval.right = width - progressStrokeWidth / 2; // 左下角x
		oval.bottom = height - progressStrokeWidth / 2; // 右下角y

		canvas.drawArc(oval, 0, 360, false, paint); // 绘制白色圆圈，即进度条背景

		// Shader mShader = new SweepGradient(width / 2, height / 2, new int[] {
		// Color.parseColor("#ff2271"), Color.parseColor("#44dfff") },
		// null);
		// paint.setShader(mShader);
		paint.setColor(Color.parseColor("#ffffff"));
		canvas.drawArc(oval, 0, (progress / maxProgress) * 360, false,
				paint); // 绘制进度圆弧，这里是蓝色

		paint.setStrokeWidth(1);
		String text = progress + "%";
		int textHeight = height / 4;

		paint.setTextSize((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics()));

		int textWidth = (int) paint.measureText(text, 0, text.length());
		paint.setStyle(Style.FILL);

		// Shader mShader1 = new LinearGradient(width / 2 - 30, height / 2
		// + textHeight / 2 - 74, width / 2 - 30, height / 2 + textHeight
		// / 2 - 14, new int[] { Color.parseColor("#ffffff"),
		// Color.parseColor("#696969") }, null, Shader.TileMode.REPEAT);
		// paint.setShader(mShader1);
		// paint.setColor(Color.rgb(0xA4, 0xA4, 0xA4));
		// paint.setTypeface(Typeface.DEFAULT_BOLD);
//
//		canvas.drawText(text, width / 2 - textWidth / 2, height / 2
//				+ textHeight / 2, paint);
	}

	public int getMaxProgress() {
		return maxProgress;
	}

	public void setMaxProgress(int maxProgress) {
		this.maxProgress = maxProgress;
	}

	public void setProgress(Float progress) {
		this.progress = progress;
		this.invalidate();
	}

//	/**
//	 * 非ＵＩ线程调用
//	 */
//	public void setProgressNotInUiThread(int progress) {
//		this.progress = progress;
//		this.postInvalidate();
//	}
}
