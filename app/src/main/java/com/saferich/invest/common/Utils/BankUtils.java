package com.saferich.invest.common.Utils;

import com.saferich.invest.R;
import com.saferich.invest.model.bean.BankItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 银行文件资源的相关工具类
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-15
 */

public class BankUtils {

    public static Map<String, List<Object>> SavaBank(List<BankItem> banklist) {

        Map<String, List<Object>> newBanklist = new HashMap<>();

        for (int i = 0; i < banklist.size(); i++) {
            List<Object> list = new ArrayList<>();
            list.add(banklist.get(i).getName());
            list.add(bankLogo.get(banklist.get(i).getBankCode()));
            list.add(bankColor.get(banklist.get(i).getBankCode()));
            newBanklist.put(banklist.get(i).getBankCode(), list);
        }

        return newBanklist;
    }

    //根据code获取icon
    public static final Map<String, Integer> bankLogo = new HashMap<>();

    static {
        bankLogo.put("ICBC", R.mipmap.icbc);
        bankLogo.put("ABC", R.mipmap.abc);
        bankLogo.put("BOC", R.mipmap.boc);
        bankLogo.put("CCB", R.mipmap.ccb);
        bankLogo.put("PSBC", R.mipmap.psbc);
        bankLogo.put("CITIC", R.mipmap.citic);
        bankLogo.put("CEB", R.mipmap.ceb);
        bankLogo.put("CMBC", R.mipmap.cmbc);
        bankLogo.put("PAYH", R.mipmap.payh);
        bankLogo.put("CIB", R.mipmap.cib);
        bankLogo.put("CGB", R.mipmap.cgb);
        bankLogo.put("SPDB", R.mipmap.spdb);
        bankLogo.put("BOCM", R.mipmap.bocm);

    }

    //根据code获取银行名称
    public static final Map<String, String> bankName = new HashMap<>();

    static {
        bankName.put("ICBC", "工商银行");
        bankName.put("ABC", "农业银行");
        bankName.put("BOC", "中国银行");
        bankName.put("CCB", "建设银行");
        bankName.put("PSBC", "邮政储蓄银行");
        bankName.put("CITIC", "中信银行");
        bankName.put("CEB", "光大银行");
        bankName.put("CMBC", "民生银行");
        bankName.put("PAYH", "平安银行");
        bankName.put("CIB", "兴业银行");
        bankName.put("CGB", "广发银行");
        bankName.put("SPDB", "浦发银行");
        bankName.put("BOCM", "交通银行");

    }

    //根据银行名称获取code
    public static final Map<String, String> bankCode = new HashMap<>();

    static {
        bankCode.put("工商银行", "ICBC");
        bankCode.put("农业银行", "ABC");
        bankCode.put("中国银行", "BOC");
        bankCode.put("建设银行", "CCB");
        bankCode.put("邮政储蓄银行", "PSBC");
        bankCode.put("中信银行", "CITIC");
        bankCode.put("光大银行", "CEB");
        bankCode.put("民生银行", "CMBC");
        bankCode.put("平安银行", "PAYH");
        bankCode.put("兴业银行", "CIB");
        bankCode.put("广发银行", "CGB");
        bankCode.put("浦发银行", "SPDB");
        bankCode.put("交通银行", "BOCM");

    }

    //根据银行code获取背景颜色
    public static final Map<String, Integer> bankColor = new HashMap<>();

    static {
        bankColor.put("ICBC", R.color.icbc);
        bankColor.put("ABC", R.color.abc);
        bankColor.put("BOC", R.color.boc);
        bankColor.put("CCB", R.color.ccb);
        bankColor.put("PSBC", R.color.psbc);
        bankColor.put("CITIC", R.color.citic);
        bankColor.put("CEB", R.color.ceb);
        bankColor.put("CMBC", R.color.cmbc);
        bankColor.put("PAYH", R.color.payh);
        bankColor.put("CIB", R.color.cib);
        bankColor.put("CGB", R.color.cgb);
        bankColor.put("SPDB", R.color.spdb);
        bankColor.put("BOCM", R.color.bocm);

    }


}
