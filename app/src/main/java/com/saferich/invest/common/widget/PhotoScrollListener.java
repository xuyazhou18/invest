package com.saferich.invest.common.widget;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 *
 * 产品详情项目实拍滑动控件
 *
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-07-04
 */

public abstract class PhotoScrollListener extends RecyclerView.OnScrollListener {

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);


        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

        if (newState == RecyclerView.SCROLL_STATE_IDLE) {

            View child = recyclerView.getChildAt(0);

            if (child == null) {
                return;
            }
            int lastVisiblePosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();


            changeTitle(lastVisiblePosition + 1);
        }
    }

    public abstract void changeTitle(int position);
}
