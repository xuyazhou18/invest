package com.saferich.invest.common.Utils;

import android.content.Context;
import android.content.Intent;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.saferich.invest.model.db.UserConfig;
import com.saferich.invest.model.db.Userinfo;
import com.saferich.invest.model.bean.Product;
import com.saferich.invest.vp.main.me.login.InputPhoneNumberActivity;
import com.saferich.invest.vp.main.product.detatils.invest.InvestActivity;
import com.saferich.invest.vp.main.product.dialog.BindCardDialog;

/**
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-06-23
 */

public class ProductUtils {

    //检查是否登录或者绑定银行卡
    public static void checkLoginAndBind(String id, String name, Context context) {
        Userinfo userinfo = SQLite.select().from(Userinfo.class).querySingle();
        UserConfig config = SQLite.select().from(UserConfig.class).querySingle();

        if (config.isLogin) {

            if (userinfo.isBindCard) {
                Intent intent = new Intent(context,
                        InvestActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("productName", name);

                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(context, BindCardDialog.class));
            }

        } else {
            context.startActivity(new Intent(context, InputPhoneNumberActivity.class));
        }
    }

    public static void checkLoginAndBind(String id, String name, Double fee, Double remainMoney,
                                         Double mininvest, Double raise, Context context) {
        Userinfo userinfo = SQLite.select().from(Userinfo.class).querySingle();
        UserConfig config = SQLite.select().from(UserConfig.class).querySingle();

        if (config.isLogin) {

            if (userinfo.isBindCard) {
                Intent intent = new Intent(context,
                        InvestActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("productName", name);
                intent.putExtra("remainMoney", StringUtil.repaceE(remainMoney));
                intent.putExtra("subscriptionFee", fee);
                intent.putExtra("purchaseIncreaseMoney", StringUtil.repaceE(raise));
                intent.putExtra("minInvestMoney", StringUtil.repaceE(mininvest));

                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(context, BindCardDialog.class));
            }

        } else {
            context.startActivity(new Intent(context, InputPhoneNumberActivity.class));
        }
    }
    //检查是否登录或者绑定银行卡
    public static void checkLoginAndBind(Product product, Context context) {
        Userinfo userinfo = SQLite.select().from(Userinfo.class).querySingle();
        UserConfig config = SQLite.select().from(UserConfig.class).querySingle();

        if (config.isLogin) {

            if (userinfo.isBindCard) {
                Intent intent = new Intent(context,
                        InvestActivity.class);
                intent.putExtra("id", product.getId());
                intent.putExtra("productName", product.getProductName());
                intent.putExtra("remainMoney", StringUtil.repaceE(product.getRemainMoney()));
                intent.putExtra("subscriptionFee", product.getSubscriptionFee());
                intent.putExtra("purchaseIncreaseMoney", StringUtil.repaceE(product.getPurchaseIncreaseMoney()));
                intent.putExtra("minInvestMoney", product.getMinInvestMoney() + "");
                intent.putExtra("productProfitTypeCode", product.getProductProfitTypeCode());

                context.startActivity(intent);
            } else {
                context.startActivity(new Intent(context, BindCardDialog.class));
            }

        } else {
            context.startActivity(new Intent(context, InputPhoneNumberActivity.class));
        }
    }
}
