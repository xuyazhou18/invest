package com.saferich.invest.common.interFace;

/**
 * 产品列表点击监听器接口
 * <p>
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public interface ProductItemClickListener {
    void itemClick(int posistion);
}
