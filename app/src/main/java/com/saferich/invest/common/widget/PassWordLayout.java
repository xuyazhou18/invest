package com.saferich.invest.common.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.saferich.invest.R;
import com.saferich.invest.common.Utils.StringUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 密码输入布局
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class PassWordLayout extends FrameLayout {


    private Context context;
    private EditText[] texts;
    private ViewHolder viewHolder;
    private intputFinishListener listener;
    private boolean isFinish;
    private boolean isAuth;
    private boolean isDel;

    public void setRealPassword(String realPassword) {
        this.isAuth = true;
        this.realPassword = realPassword;
    }

    private String realPassword;

    public PassWordLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.password_layout, this);

        viewHolder = new ViewHolder(view);

        texts = new EditText[6];
        texts[0] = viewHolder.editOne;
        texts[1] = viewHolder.editTwo;
        texts[2] = viewHolder.editThree;
        texts[3] = viewHolder.editFour;
        texts[4] = viewHolder.editFive;
        texts[5] = viewHolder.editSix;


        texts[0].setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    texts[0].requestFocus();
                    texts[0].getText().delete(0, 1);
                    texts[0].callOnClick();
                    Log.d("xyz", "del------>fouse====true ");
                } else {
                    Log.d("xyz", "del------>fouse====false ");
                }
            }
        });

        for (int i = 0; i < texts.length; i++) {
            final int curIndex = i;


            texts[i].setOnKeyListener((v, keyCode, event) -> {

                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (curIndex != 0 && curIndex != 6) {
                        isDel = true;

                        texts[curIndex - 1].requestFocus();
                        texts[curIndex - 1].setText("");


                    } else {
                        texts[curIndex].requestFocus();
                    }
                }

                return false;

            });


            texts[i].addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                    // TODO Auto-generated method stubi

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                    int nextIndex = curIndex + 1;

                    if (isDel || s.toString().equals("")) {
                        isDel = false;
                        return;
                    }


                    //当输入到最后一个EditText时

                    if (nextIndex >= texts.length) {
                        if (!isFinish) {
                            if (isAuth) {

                                authPassword();

                            } else {
                                returnPassword();
                            }
                        }
                        return;
                    }
                    texts[nextIndex].requestFocus();
                }


            });
        }

    }

    private void returnPassword() {

        String currentPassword = StringUtil.concat(
                viewHolder.editOne.getText().toString().trim(),
                viewHolder.editTwo.getText().toString().trim(),
                viewHolder.editThree.getText().toString().trim(),
                viewHolder.editFour.getText().toString().trim(),
                viewHolder.editFive.getText().toString().trim(),
                viewHolder.editSix.getText().toString().trim());
        listener.returnBack(currentPassword);
        isFinish = true;
        clearText();
    }


    public boolean authPassword() {
        if (realPassword == null) {
            return false;
        }

        String currentPassword = StringUtil.concat(
                viewHolder.editOne.getText().toString().trim(),
                viewHolder.editTwo.getText().toString().trim(),
                viewHolder.editThree.getText().toString().trim(),
                viewHolder.editFour.getText().toString().trim(),
                viewHolder.editFive.getText().toString().trim(),
                viewHolder.editSix.getText().toString().trim());
        if (StringUtil.equals(currentPassword, realPassword)) {
            listener.authFinish(true);
            return true;
        } else {
            isFinish = true;
            clearText();
            listener.authFinish(false);
            return false;
        }


    }

    private void clearText() {
//
        texts[0].setText("");
        texts[1].setText("");
        texts[2].setText("");
        texts[3].setText("");
        texts[4].setText("");
        texts[5].setText("");
//
        texts[0].requestFocus();
        isFinish = false;
    }

    public void setListener(intputFinishListener listener) {
        this.listener = listener;
    }


    static class ViewHolder {
        @BindView(R.id.edit_one)
        EditText editOne;
        @BindView(R.id.edit_two)
        EditText editTwo;
        @BindView(R.id.edit_three)
        EditText editThree;
        @BindView(R.id.edit_four)
        EditText editFour;
        @BindView(R.id.edit_five)
        EditText editFive;
        @BindView(R.id.edit_six)
        EditText editSix;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface intputFinishListener {
        void authFinish(boolean isSuccess);

        void returnBack(String password);
    }
}
