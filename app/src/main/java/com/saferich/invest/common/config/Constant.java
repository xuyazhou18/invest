package com.saferich.invest.common.config;

import android.os.Environment;

import com.saferich.invest.BuildConfig;

/**
 * 常用本地常量
 * Author: lampard_xu(xuyazhou18@gmail.com)
 * <p>
 * Date: 2016-05-16
 */
public class Constant {

    //图片地址
    public static String imagUrl = BuildConfig.DEBUG ? "" : "";


    //本地缓存下载存放地址
    public static final String DownLoadPath = Environment
            .getExternalStorageDirectory().getPath()
            + "/Android/data/com.saferich.invest/download/";

}
